package accountserver

import (
	"context"
	"fmt"
	"testing"
)

type validationTestData struct {
	value string
	ok    bool
}

func runValidationTest(t testing.TB, v ValidatorFunc, testData []validationTestData) {
	for _, td := range testData {
		err := v(&RequestContext{Context: context.Background()}, td.value)
		if (err == nil && !td.ok) || (err != nil && td.ok) {
			t.Errorf("test for '%s' failed: expected %v, got error %v", td.value, td.ok, err)
		}
	}
}

func TestValidator_NotInSet(t *testing.T) {
	td := []validationTestData{
		{"apple", true},
		{"pear", false},
		{"", true},
	}
	runValidationTest(t, notInSet(newStringSetFromList([]string{"pear", "banana"})), td)
}

func TestValidator_MinMaxLength(t *testing.T) {
	td := []validationTestData{
		{"a", false},
		{"aaa", true},
		{"aaaaa", true},
		{"aaaaaaa", false},
		{"aaaaaaaaa", false},
	}
	runValidationTest(t, allOf(minLength(2), maxLength(5)), td)
}

func TestValidator_MatchUsername(t *testing.T) {
	td := []validationTestData{
		{"a", false},
		{"aa", true},
		{"abc", true},
		{"123", true},
		{"a-b", true},
		{"a_b", true},
		{"a-b-c-d", true},
		{"-ab", false},
		{"ab-", false},
		{"a--b", false},
		{"a__b", false},
		{"...", false},
		{"a.b", true},
		{"a.-b", false},
		{"a-.-b", false},
	}
	runValidationTest(t, matchUsernameRx(), td)
}

type fakeCheckBackend struct {
	// We need this just to satisfy the Backend interface.
	*fakeBackend

	m map[string]struct{}
}

type fakeCheckTX struct {
	TX
	m map[string]struct{}
}

func newFakeCheckBackend(seeds []FindResourceRequest) *fakeCheckBackend {
	f := &fakeCheckBackend{
		fakeBackend: createFakeBackend(),
		m:           make(map[string]struct{}),
	}
	for _, s := range seeds {
		f.m[fmt.Sprintf("%s/%s", s.Type, s.Name)] = struct{}{}
	}
	return f
}

func (f *fakeCheckBackend) NewTransaction() (TX, error) {
	tx, _ := f.fakeBackend.NewTransaction()
	return &fakeCheckTX{TX: tx, m: f.m}, nil
}

func (f *fakeCheckTX) HasAnyResource(_ context.Context, reqs []FindResourceRequest) (bool, error) {
	for _, req := range reqs {
		if _, ok := f.m[fmt.Sprintf("%s/%s", req.Type, req.Name)]; ok {
			return true, nil
		}
	}
	return false, nil
}

func newTestValidationContext(forbiddenUsernames ...string) *validationContext {
	config := &ValidationConfig{
		ForbiddenUsernames: forbiddenUsernames,
	}
	config.compile() // nolint: errcheck
	return &validationContext{config: config}
}

func newFakeDomainBackend(domains ...string) domainBackend {
	set := newStringSetFromList(domains)
	return &staticDomainBackend{
		sets: map[string]*stringSet{
			ResourceTypeEmail:       set,
			ResourceTypeMailingList: set,
			ResourceTypeWebsite:     set,
		},
	}
}

func TestValidator_HostedEmail(t *testing.T) {
	td := []validationTestData{
		{"user1@example.com", true},
		{"user2@example.com", true},
		{".@example.com", false},
		{"user@other-domain.com", false},
		{"forbidden@example.com", false},
		{"existing@example.com", false},
	}
	vc := newTestValidationContext("forbidden")
	vc.backend = newFakeCheckBackend([]FindResourceRequest{
		{Type: ResourceTypeEmail, Name: "existing@example.com"},
	})
	vc.domains = newFakeDomainBackend("example.com")
	vf := allOf(vc.validHostedEmail(), vc.isAvailableEmailAddr())
	runValidationTest(t, vf, td)
}

func TestValidator_HostedMailingList(t *testing.T) {
	td := []validationTestData{
		{"list1@domain1.com", true},
		{"list2@domain2.com", true},
		{".@domain1.com", false},
		{"list@other-domain.com", false},
		{"forbidden@domain1.com", false},
		{"existing@domain1.com", false},
		{"existing@domain2.com", false},
	}
	vc := newTestValidationContext("forbidden")
	vc.backend = newFakeCheckBackend([]FindResourceRequest{
		{Type: ResourceTypeMailingList, Name: "existing@domain2.com"},
	})
	vc.domains = newFakeDomainBackend("domain1.com", "domain2.com")
	vf := allOf(vc.validHostedMailingList(), vc.isAvailableEmailAddr())
	runValidationTest(t, vf, td)
}
