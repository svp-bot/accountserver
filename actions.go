package accountserver

import (
	"context"
	"crypto/rand"
	"encoding/base64"
	"errors"
	"fmt"

	"github.com/pquerna/otp/totp"
	"github.com/sethvargo/go-password/password"
)

// Request is the generic interface for request types. Each request
// type defines its own handler, built of composable objects that
// define its behavior with respect to validation, authentication and
// execution.
type Request interface {
	PopulateContext(*RequestContext) error
	Validate(*RequestContext) error
	Authorize(*RequestContext) error
	Serve(*RequestContext) (interface{}, error)
}

// Auth parameters of an incoming request (validated).
type Auth struct {
	Username string
	IsAdmin  bool
}

// The RequestContext holds a large number of request-scoped variables
// populated at different stages of the action workflow. This is
// simpler than managing lots of custom Context vars and the
// associated boilerplate, but it's still a bit of an antipattern due
// to the loss of generality.
type RequestContext struct {
	// Link to the infra services.
	*AccountService

	// Request-scoped read-only values.
	Context context.Context
	//HTTPRequest *http.Request
	TX TX

	// Request-scoped read-write parameters.
	Auth     Auth
	User     *RawUser
	Resource *Resource
	Comment  string
}

// Value that we put in place of private fields when sanitizing.
const sanitizedValue = "XXXXXX"

// RequestBase contains parameters shared by all authenticated request types.
type RequestBase struct {
	SSO string `json:"sso"`

	// Optional comment, will end up in audit logs.
	Comment string `json:"comment,omitempty"`
}

// Sanitize the request.
func (r *RequestBase) Sanitize() {
	if r.SSO != "" {
		r.SSO = sanitizedValue
	}
}

// Validate the request.
func (r *RequestBase) Validate(rctx *RequestContext) error {
	return nil
}

// PopulateContext extracts information from the request and stores it
// into the RequestContext.
func (r *RequestBase) PopulateContext(rctx *RequestContext) error {
	rctx.Comment = r.Comment

	if r.SSO == "" {
		return newAuthError(errors.New("no SSO credentials provided"))
	}
	var err error
	rctx.Auth, err = rctx.authFromSSO(r.SSO)
	if err != nil {
		return newAuthError(err)
	}
	return nil
}

// commented out so subtypes *must* implement authorization, checked at compile time.
//func (r *RequestBase) Authorize(rctx *RequestContext) error {
//	return nil
//}

// AdminRequestBase is a generic admin request.
type AdminRequestBase struct {
	RequestBase
}

// Authorize the request.
func (r *AdminRequestBase) Authorize(rctx *RequestContext) error {
	if !rctx.Auth.IsAdmin {
		return fmt.Errorf("user %s is not an admin", rctx.Auth.Username)
	}
	return nil
}

// UserRequestBase is a generic request about a specific user.
type UserRequestBase struct {
	RequestBase
	Username string `json:"username"`
}

// Validate the request.
func (r *UserRequestBase) Validate(rctx *RequestContext) error {
	return r.RequestBase.Validate(rctx)
}

// PopulateContext extracts information from the request and stores it
// into the RequestContext.
func (r *UserRequestBase) PopulateContext(rctx *RequestContext) error {
	user, err := getUserOrDie(rctx.Context, rctx.TX, r.Username)
	if err != nil {
		return err
	}
	rctx.User = user
	return r.RequestBase.PopulateContext(rctx)
}

// Authorize the request.
func (r *UserRequestBase) Authorize(rctx *RequestContext) error {
	if !rctx.Auth.IsAdmin && rctx.Auth.Username != r.Username {
		return fmt.Errorf("user %s can't access resource %s", rctx.Auth.Username, r.Username)
	}
	return nil
}

// AdminUserRequestBase is an admin-only version of UserRequestBase.
type AdminUserRequestBase struct {
	UserRequestBase
}

// Authorize the request.
func (r *AdminUserRequestBase) Authorize(rctx *RequestContext) error {
	if !rctx.Auth.IsAdmin {
		return fmt.Errorf("user %s is not an admin", rctx.Auth.Username)
	}
	return nil
}

// PrivilegedRequestBase extends RequestBase with the user password,
// for privileged endpoints.
type PrivilegedRequestBase struct {
	UserRequestBase
	CurPassword string `json:"cur_password"`
	RemoteAddr  string `json:"remote_addr"`
	// TODO: Add full DeviceInfo?
}

// Sanitize the request.
func (r *PrivilegedRequestBase) Sanitize() {
	r.UserRequestBase.Sanitize()
	if r.CurPassword != "" {
		r.CurPassword = sanitizedValue
	}
}

// Authorize the request.
func (r *PrivilegedRequestBase) Authorize(rctx *RequestContext) error {
	if err := r.UserRequestBase.Authorize(rctx); err != nil {
		return err
	}
	if err := rctx.authorizeUser(rctx.Context, rctx.User.Name, r.CurPassword, r.RemoteAddr); err != nil {
		return err
	}
	return nil
}

// ResourceRequestBase is the base type for resource-level requests.
type ResourceRequestBase struct {
	RequestBase
	ResourceID ResourceID `json:"resource_id"`
}

// PopulateContext extracts information from the request and stores it
// into the RequestContext.
func (r *ResourceRequestBase) PopulateContext(rctx *RequestContext) error {
	rsrc, err := getResourceOrDie(rctx.Context, rctx.TX, r.ResourceID)
	if err != nil {
		return err
	}
	rctx.Resource = &rsrc.Resource

	// If the resource has an owner, populate the User context field.
	if rsrc.Owner != "" {
		user, err := getUserOrDie(rctx.Context, rctx.TX, rsrc.Owner)
		if err != nil {
			return err
		}
		rctx.User = user
	}

	return r.RequestBase.PopulateContext(rctx)
}

// Authorize the request.
func (r *ResourceRequestBase) Authorize(rctx *RequestContext) error {
	if !rctx.Auth.IsAdmin && !rctx.TX.CanAccessResource(rctx.Context, rctx.Auth.Username, rctx.Resource) {
		return fmt.Errorf("user %s can't access resource %s", rctx.Auth.Username, rctx.Resource.ID)
	}
	return nil
}

// AdminResourceRequestBase is an admin-only version of ResourceRequestBase.
type AdminResourceRequestBase struct {
	ResourceRequestBase
}

// Authorize the request.
func (r *AdminResourceRequestBase) Authorize(rctx *RequestContext) error {
	if !rctx.Auth.IsAdmin {
		return fmt.Errorf("user %s is not an admin", rctx.Auth.Username)
	}
	return nil
}

func randomBase64(n int) string {
	b := make([]byte, n*3/4)
	_, err := rand.Read(b[:]) // #nosec
	if err != nil {
		panic(err)
	}
	return base64.StdEncoding.EncodeToString(b[:])[:n]
}

func randomPassword() string {
	// Create a 16-character password with 4 digits and 2 symbols.
	return password.MustGenerate(16, 4, 2, false, false)
}

func randomAppSpecificPassword() string {
	// Create a 32-character password with 5 digits and 5 symbols.
	return password.MustGenerate(32, 5, 5, false, false)
}

const appSpecificPasswordIDLen = 4

func randomAppSpecificPasswordID() string {
	return randomBase64(appSpecificPasswordIDLen)
}

func generateTOTPSecret() (string, error) {
	key, err := totp.Generate(totp.GenerateOpts{})
	if err != nil {
		return "", err
	}
	return key.Secret(), nil
}
