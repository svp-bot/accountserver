package main

import (
	"errors"
	"flag"
	"fmt"
	"io/ioutil"
	"log"
	"strings"

	"git.autistici.org/ai3/accountserver"
	"git.autistici.org/ai3/go-common/clientutil"
	"git.autistici.org/ai3/go-common/pwhash"
	"git.autistici.org/ai3/go-common/serverutil"
	"gopkg.in/yaml.v2"

	auxdbbackend "git.autistici.org/ai3/accountserver/backend/auxdb"
	cachebackend "git.autistici.org/ai3/accountserver/backend/cache"
	insbackend "git.autistici.org/ai3/accountserver/backend/instrumented"
	ldapbackend "git.autistici.org/ai3/accountserver/backend/ldap"
	"git.autistici.org/ai3/accountserver/server"
)

var (
	addr       = flag.String("addr", ":4040", "tcp `address` to listen on")
	configFile = flag.String("config", "/etc/accountserver/config.yml", "configuration `file`")
)

type config struct {
	LDAP struct {
		URI        string `yaml:"uri"`
		BindDN     string `yaml:"bind_dn"`
		BindPw     string `yaml:"bind_pw"`
		BindPwFile string `yaml:"bind_pw_file"`
		BaseDN     string `yaml:"base_dn"`
	} `yaml:"ldap"`
	Cache struct {
		Enabled bool `yaml:"enabled"`
	} `yaml:"cache"`
	AccountServerConfig accountserver.Config     `yaml:",inline"`
	ServerConfig        *serverutil.ServerConfig `yaml:"http_server"`
	PwHash              struct {
		Algo   string         `yaml:"algo"`
		Params map[string]int `yaml:"params"`
	} `yaml:"pwhash"`
	AuxDB *clientutil.BackendConfig `yaml:"aux_db"`

	// Replication config.
	Replication struct {
		LeaderURL string                      `yaml:"leader_url"`
		Peers     []string                    `yaml:"peers"`
		TLS       *clientutil.TLSClientConfig `yaml:"tls"`
	} `yaml:"replication"`
}

func (c *config) validate() error {
	if c.LDAP.URI == "" {
		return errors.New("empty ldap.uri")
	}
	if c.LDAP.BindDN == "" {
		return errors.New("empty ldap.bind_dn")
	}
	if (c.LDAP.BindPwFile == "" && c.LDAP.BindPw == "") || (c.LDAP.BindPwFile != "" && c.LDAP.BindPw != "") {
		return errors.New("only one of ldap.bind_pw_file or ldap.bind_pw must be set")
	}
	if c.AccountServerConfig.SSO.PublicKeyFile == "" {
		return errors.New("empty sso.public_key")
	}
	if c.AccountServerConfig.SSO.Domain == "" {
		return errors.New("empty sso.domain")
	}
	if c.AccountServerConfig.SSO.Service == "" {
		return errors.New("empty sso.service")
	}
	return nil
}

func (c *config) getPasswordHash() (h pwhash.PasswordHash, err error) {
	switch c.PwHash.Algo {
	case "":
		// Just use the defaults.
		h = pwhash.DefaultEncryptAlgorithm
	case "argon2":
		pTime := 1
		if i, ok := c.PwHash.Params["time"]; ok {
			pTime = i
		}
		pMem := 4
		if i, ok := c.PwHash.Params["mem"]; ok {
			pMem = i
		}
		pThreads := 4
		if i, ok := c.PwHash.Params["threads"]; ok {
			pThreads = i
		}
		h = pwhash.NewArgon2WithParams(uint32(pTime), uint32(pMem), uint8(pThreads))
	case "scrypt":
		pN := 16384
		if i, ok := c.PwHash.Params["n"]; ok {
			pN = i
		}
		pR := 8
		if i, ok := c.PwHash.Params["r"]; ok {
			pR = i
		}
		pP := 1
		if i, ok := c.PwHash.Params["p"]; ok {
			pP = i
		}
		h = pwhash.NewScryptWithParams(pN, pR, pP)
	default:
		err = fmt.Errorf("unknown pwhash algo '%s'", c.PwHash.Algo)
	}
	return
}

func loadConfig(path string) (*config, error) {
	// Read YAML config.
	data, err := ioutil.ReadFile(path)
	if err != nil {
		return nil, err
	}
	var c config
	if err := yaml.Unmarshal(data, &c); err != nil {
		return nil, err
	}
	if err := c.validate(); err != nil {
		return nil, err
	}
	return &c, nil
}

func getBindPw(c *config) (string, error) {
	// Read the bind password.
	bindPw := c.LDAP.BindPw
	if c.LDAP.BindPwFile != "" {
		pwData, err := ioutil.ReadFile(c.LDAP.BindPwFile)
		if err != nil {
			return "", err
		}
		bindPw = strings.TrimSpace(string(pwData))
	}

	return bindPw, nil
}

func main() {
	log.SetFlags(0)
	flag.Parse()

	config, err := loadConfig(*configFile)
	if err != nil {
		log.Fatal(err)
	}

	h, err := config.getPasswordHash()
	if err != nil {
		log.Fatal(err)
	}
	accountserver.DefaultPasswordHash = h

	bindPw, err := getBindPw(config)
	if err != nil {
		log.Fatal(err)
	}

	ldapBE, err := ldapbackend.NewLDAPBackend(
		config.LDAP.URI,
		config.LDAP.BindDN,
		bindPw,
		config.LDAP.BaseDN,
	)
	if err != nil {
		log.Fatal(err)
	}
	be := insbackend.Wrap(ldapBE, "ldap")

	// If the cache is enabled, create the cache backend wrapper. It
	// also acts as a http.Handler for the replicated cache
	// invalidation RPC, so we're going to have to route its endpoint
	// on the main Server later.
	var cacheBE *cachebackend.CacheBackend
	if config.Cache.Enabled {
		cacheBE, err = cachebackend.Wrap(
			be,
			config.Replication.Peers,
			config.Replication.TLS,
		)
		if err != nil {
			log.Fatal(err)
		}
		be = insbackend.Wrap(cacheBE, "cache")
	}

	// Enable lookups to the aux-db service. Errors are not fatal,
	// the service is optional.
	if config.AuxDB != nil {
		xdb, err := auxdbbackend.Wrap(be, config.AuxDB)
		if err != nil {
			log.Printf("warning: could not initialize webappdb backend: %v", err)
		} else {
			be = xdb
		}
	}

	service, err := accountserver.NewAccountService(be, &config.AccountServerConfig)
	if err != nil {
		log.Fatal(err)
	}

	as, err := server.New(service, be, config.Replication.LeaderURL, config.Replication.TLS)
	if err != nil {
		log.Fatal(err)
	}
	if cacheBE != nil {
		as.Handle(cachebackend.InvalidateURLPath, cacheBE)
	}

	// Start the HTTP server.
	if err := serverutil.Serve(as, config.ServerConfig, *addr); err != nil {
		log.Fatal(err)
	}
}
