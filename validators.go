package accountserver

import (
	"bufio"
	"context"
	"errors"
	"fmt"
	"math/rand"
	"os"
	"path/filepath"
	"regexp"
	"strings"
	"time"

	"golang.org/x/net/publicsuffix"
)

// A domainBackend manages the list of domains users are allowed to request services on.
type domainBackend interface {
	GetAllowedDomains(context.Context, string) []string
	IsAllowedDomain(*RequestContext, string, string) bool
}

// A shardBackend can return information about available / allowed service shards.
type shardBackend interface {
	GetAllowedShards(context.Context, string) []string
	GetAvailableShards(context.Context, string) []string
	IsAllowedShard(context.Context, string, string) bool
}

// ValidationConfig specifies a large number of validation-related
// configurable parameters.
type ValidationConfig struct {
	ForbiddenUsernames     []string            `yaml:"forbidden_usernames"`
	ForbiddenUsernamesFile string              `yaml:"forbidden_usernames_file"`
	ForbiddenPasswords     []string            `yaml:"forbidden_passwords"`
	ForbiddenPasswordsFile string              `yaml:"forbidden_passwords_file"`
	ForbiddenDomains       []string            `yaml:"forbidden_domains"`
	ForbiddenDomainsFile   string              `yaml:"forbidden_domains_file"`
	AvailableDomains       map[string][]string `yaml:"available_domains"`
	WebsiteRootDir         string              `yaml:"website_root_dir"`
	MinPasswordLen         int                 `yaml:"min_password_len"`
	MaxPasswordLen         int                 `yaml:"max_password_len"`
	MinUsernameLen         int                 `yaml:"min_username_len"`
	MaxUsernameLen         int                 `yaml:"max_username_len"`
	MinUID                 int                 `yaml:"min_backend_uid"`
	MaxUID                 int                 `yaml:"max_backend_uid"`

	forbiddenUsernames *stringSet
	forbiddenPasswords *stringSet
	forbiddenDomains   *regexpSet
}

const (
	defaultMinPasswordLen = 8
	defaultMaxPasswordLen = 128
	defaultMinUsernameLen = 3
	defaultMaxUsernameLen = 64
	defaultMinUID         = 1000
	defaultMaxUID         = 0
)

func (c *ValidationConfig) setDefaults() {
	if c.MinPasswordLen == 0 {
		c.MinPasswordLen = defaultMinPasswordLen
	}
	if c.MaxPasswordLen == 0 {
		c.MaxPasswordLen = defaultMaxPasswordLen
	}
	if c.MinUsernameLen == 0 {
		c.MinUsernameLen = defaultMinUsernameLen
	}
	if c.MaxUsernameLen == 0 {
		c.MaxUsernameLen = defaultMaxUsernameLen
	}
	if c.MinUID == 0 {
		c.MinUID = defaultMinUID
	}
	if c.MaxUID == 0 {
		c.MaxUID = defaultMaxUID
	}
}

func (c *ValidationConfig) compile() (err error) {
	c.setDefaults()

	c.forbiddenUsernames, err = newStringSetFromFileOrList(c.ForbiddenUsernames, c.ForbiddenUsernamesFile)
	if err != nil {
		return
	}
	c.forbiddenPasswords, err = newStringSetFromFileOrList(c.ForbiddenPasswords, c.ForbiddenPasswordsFile)
	if err != nil {
		return
	}
	c.forbiddenDomains, err = newRegexpSetFromFileOrList(c.ForbiddenDomains, c.ForbiddenDomainsFile)
	return
}

// The validationContext contains all configuration and backends that
// the various validation functions will need. Most methods on this
// object return functions themselves (ValidatorFunc or variations
// thereof) that can later be called multiple times at will and
// combined with operators like 'allOf'.
type validationContext struct {
	config  *ValidationConfig
	domains domainBackend
	shards  shardBackend
	backend Backend
}

// A stringSet is just a list of strings with a quick membership test.
type stringSet struct {
	set  map[string]struct{}
	list []string
}

func newStringSetFromList(list []string) *stringSet {
	set := make(map[string]struct{})
	for _, s := range list {
		set[s] = struct{}{}
	}
	return &stringSet{set: set, list: list}
}

func newStringSetFromFileOrList(list []string, path string) (*stringSet, error) {
	if path != "" {
		var err error
		list, err = loadStringsFromFile(path)
		if err != nil {
			return nil, err
		}
	}
	return newStringSetFromList(list), nil
}

func (s *stringSet) Contains(needle string) bool {
	if s == nil {
		return false
	}
	_, ok := s.set[needle]
	return ok
}

func (s *stringSet) List() []string {
	if s == nil {
		return nil
	}
	return s.list
}

// A set of regular expressions.
type regexpSet struct {
	exprs []*regexp.Regexp
}

func newRegexpSetFromList(list []string) (*regexpSet, error) {
	var set regexpSet
	for _, pattern := range list {
		rx, err := regexp.Compile(pattern)
		if err != nil {
			return nil, err
		}
		set.exprs = append(set.exprs, rx)
	}
	return &set, nil
}

func newRegexpSetFromFileOrList(list []string, path string) (*regexpSet, error) {
	if path != "" {
		var err error
		list, err = loadStringsFromFile(path)
		if err != nil {
			return nil, err
		}
	}
	return newRegexpSetFromList(list)
}

func (s *regexpSet) Contains(needle string) bool {
	for _, rx := range s.exprs {
		if rx.MatchString(needle) {
			return true
		}
	}
	return false
}

// A domainBackend that works with a static list of type-specific allowed domains.
type staticDomainBackend struct {
	sets map[string]*stringSet
}

func (d *staticDomainBackend) GetAllowedDomains(_ context.Context, kind string) []string {
	return d.sets[kind].List()
}

func (d *staticDomainBackend) IsAllowedDomain(_ *RequestContext, kind, domain string) bool {
	return d.sets[kind].Contains(domain)
}

type staticShardBackend struct {
	available map[string]*stringSet
	allowed   map[string]*stringSet
}

func (d *staticShardBackend) GetAllowedShards(_ context.Context, kind string) []string {
	return d.allowed[kind].List()
}

func (d *staticShardBackend) GetAvailableShards(_ context.Context, kind string) []string {
	return d.available[kind].List()
}

func (d *staticShardBackend) IsAllowedShard(_ context.Context, kind, shard string) bool {
	return d.allowed[kind].Contains(shard)
}

func loadStringsFromFile(path string) ([]string, error) {
	f, err := os.Open(path) // #nosec
	if err != nil {
		return nil, err
	}
	defer f.Close() // nolint: errcheck

	var list []string
	scanner := bufio.NewScanner(f)
	for scanner.Scan() {
		line := scanner.Text()
		if line == "" || line[0] == '#' {
			continue
		}
		list = append(list, line)
	}
	if err := scanner.Err(); err != nil {
		return nil, err
	}
	return list, nil
}

// ValidatorFunc is the generic interface for unstructured data field
// (string) validators.
type ValidatorFunc func(*RequestContext, string) error

type genericSet interface {
	Contains(string) bool
}

func notInSet(set genericSet) ValidatorFunc {
	return func(_ *RequestContext, value string) error {
		if set.Contains(value) {
			return errors.New("invalid value (blacklisted)")
		}
		return nil
	}
}

func minLength(minLen int) ValidatorFunc {
	return func(_ *RequestContext, value string) error {
		if len(value) < minLen {
			return fmt.Errorf("value must be at least %d characters", minLen)
		}
		return nil
	}
}

func maxLength(maxLen int) ValidatorFunc {
	return func(_ *RequestContext, value string) error {
		if len(value) > maxLen {
			return fmt.Errorf("value must be at most %d characters", maxLen)
		}
		return nil
	}
}

func matchRegexp(rx *regexp.Regexp, errmsg string) ValidatorFunc {
	return func(_ *RequestContext, value string) error {
		if !rx.MatchString(value) {
			return errors.New(errmsg)
		}
		return nil
	}
}

// The generic username regexp allows for standard usernames.
var usernameRx = regexp.MustCompile(`^([a-z0-9]+[-_.]?)+[a-z0-9]$`)

func matchUsernameRx() ValidatorFunc {
	return matchRegexp(usernameRx, "value must be an alphanumeric sequence, including . and - characters but not starting or ending with those")
}

func matchSitenameRx() ValidatorFunc {
	return matchUsernameRx()
}

// The identifier regexp is stricter and forbids [-_.] characters.
var identifierRx = regexp.MustCompile(`^[a-z0-9]+$`)

func matchIdentifierRx() ValidatorFunc {
	return matchRegexp(identifierRx, "value must be alphanumeric")
}

func validateUsernameAndDomain(validateUsername, validateDomain ValidatorFunc) ValidatorFunc {
	return func(rctx *RequestContext, value string) error {
		parts := strings.SplitN(value, "@", 2)
		if len(parts) != 2 {
			return errors.New("malformed email address")
		}
		if err := validateUsername(rctx, parts[0]); err != nil {
			return err
		}
		return validateDomain(rctx, parts[1])
	}
}

// This is not a prescriptive domain name validation regex, it's just used to
// give useful feedback to the user if the domain name looks visibly wrong.
var domainRx = regexp.MustCompile(`^(?:[a-zA-Z0-9_\-]{1,63}\.)+(?:[a-zA-Z]{2,})$`)

func isRegistered(domain string) bool {
	// TODO: ha ha, there isn't really a good way to check.
	return true
}

func validDomainName(_ *RequestContext, value string) error {
	if !domainRx.MatchString(value) {
		return errors.New("invalid domain name")
	}

	// Extract the top-level domain and check that it is registered
	// (which works also if we're hosting the top-level domain
	// ourselves already).
	domain, err := publicsuffix.EffectiveTLDPlusOne(value)
	if err != nil {
		return errors.New("invalid TLD")
	}
	if !isRegistered(domain) {
		return fmt.Errorf("the domain %s does not seem to be registered", domain)
	}
	return nil
}

// Split a (correctly formed) email address into username/domain.
func splitEmailAddr(addr string) (string, string) {
	parts := strings.SplitN(addr, "@", 2)
	return parts[0], parts[1]
}

// Returns all the possible resources in the email and mailing list
// namespaces that might conflict with the given email address.
func relatedEmails(ctx context.Context, be domainBackend, addr string) []FindResourceRequest {
	rel := []FindResourceRequest{
		{Type: ResourceTypeEmail, Name: addr},
	}
	user, _ := splitEmailAddr(addr)
	// Mailing lists and newsletters must have unique names
	// regardless of the domain, so we add potential conflicts for
	// mailing lists with the same name over all list-enabled
	// domains.
	for _, d := range be.GetAllowedDomains(ctx, ResourceTypeMailingList) {
		rel = append(rel, FindResourceRequest{
			Type: ResourceTypeMailingList,
			Name: fmt.Sprintf("%s@%s", user, d),
		})
	}
	for _, d := range be.GetAllowedDomains(ctx, ResourceTypeNewsletter) {
		rel = append(rel, FindResourceRequest{
			Type: ResourceTypeNewsletter,
			Name: fmt.Sprintf("%s@%s", user, d),
		})
	}
	return rel
}

func relatedWebsites(ctx context.Context, be domainBackend, value string) []FindResourceRequest {
	// Ignore the parent domain (websites share a global namespace).
	return []FindResourceRequest{
		{
			Type: ResourceTypeWebsite,
			Name: value,
		},
	}
}

func relatedDomains(ctx context.Context, be domainBackend, value string) []FindResourceRequest {
	return []FindResourceRequest{
		{
			Type: ResourceTypeDomain,
			Name: value,
		},
	}
}

func (v *validationContext) isAllowedDomain(rtype string) ValidatorFunc {
	return func(rctx *RequestContext, value string) error {
		if !v.domains.IsAllowedDomain(rctx, rtype, value) {
			return errors.New("unavailable domain")
		}
		return nil
	}
}

func (v *validationContext) isAllowedEmailDomain() ValidatorFunc {
	return func(rctx *RequestContext, value string) error {
		// Static lookup first.
		if v.domains.IsAllowedDomain(rctx, ResourceTypeEmail, value) {
			return nil
		}

		// Dynamic lookup for hosted domains in the database
		// that have AcceptMail=true.
		if tx, err := v.backend.NewTransaction(); err == nil {

			// Ignore the error in FindResource() since we're going to fail close anyway.
			// nolint: errcheck
			rsrc, _ := tx.FindResource(rctx.Context, FindResourceRequest{Type: ResourceTypeDomain, Name: value})
			if rsrc != nil && rsrc.Status == ResourceStatusActive && rsrc.Website.AcceptMail {
				return nil
			}
		}

		return errors.New("unavailable domain")
	}
}

func (v *validationContext) isAvailableEmailAddr() ValidatorFunc {
	return func(rctx *RequestContext, value string) error {
		rel := relatedEmails(rctx.Context, v.domains, value)

		// Run the presence check in a new transaction. Unavailability
		// of the server results in a validation error (fail close).
		tx, err := v.backend.NewTransaction()
		if err != nil {
			return err
		}
		// Errors will cause to consider the address unavailable.
		if ok, _ := tx.HasAnyResource(rctx.Context, rel); ok { // nolint
			return errors.New("address unavailable")
		}
		return nil
	}
}

func (v *validationContext) isAvailableDomain() ValidatorFunc {
	return func(rctx *RequestContext, value string) error {
		rel := relatedDomains(rctx.Context, v.domains, value)

		// Run the presence check in a new transaction. Unavailability
		// of the server results in a validation error (fail close).
		tx, err := v.backend.NewTransaction()
		if err != nil {
			return err
		}
		// Errors will cause to consider the resource unavailable.
		if ok, _ := tx.HasAnyResource(rctx.Context, rel); ok { // nolint
			return errors.New("address unavailable")
		}
		return nil
	}
}

func (v *validationContext) isAvailableWebsite() ValidatorFunc {
	return func(rctx *RequestContext, value string) error {
		rel := relatedWebsites(rctx.Context, v.domains, value)

		// Run the presence check in a new transaction. Unavailability
		// of the server results in a validation error (fail close).
		tx, err := v.backend.NewTransaction()
		if err != nil {
			return err
		}
		// Errors will cause to consider the resource unavailable.
		if ok, _ := tx.HasAnyResource(rctx.Context, rel); ok { // nolint
			return errors.New("address unavailable")
		}
		return nil
	}
}

func (v *validationContext) isAvailableDAV() ValidatorFunc {
	return func(rctx *RequestContext, value string) error {
		rel := []FindResourceRequest{
			{
				Type: ResourceTypeDAV,
				Name: value,
			},
		}

		// Run the presence check in a new transaction. Unavailability
		// of the server results in a validation error (fail close).
		tx, err := v.backend.NewTransaction()
		if err != nil {
			return err
		}
		// Errors will cause to consider the resource unavailable.
		if ok, _ := tx.HasAnyResource(rctx.Context, rel); ok { // nolint
			return errors.New("name unavailable")
		}
		return nil
	}
}

func (v *validationContext) isAvailableDatabase() ValidatorFunc {
	return func(rctx *RequestContext, value string) error {
		rel := []FindResourceRequest{
			{
				Type: ResourceTypeDatabase,
				Name: value,
			},
		}

		// Run the presence check in a new transaction. Unavailability
		// of the server results in a validation error (fail close).
		tx, err := v.backend.NewTransaction()
		if err != nil {
			return err
		}
		// Errors will cause to consider the resource unavailable.
		if ok, _ := tx.HasAnyResource(rctx.Context, rel); ok { // nolint
			return errors.New("name unavailable")
		}
		return nil
	}
}

func (v *validationContext) validHostedEmail() ValidatorFunc {
	return validateUsernameAndDomain(
		allOf(
			matchUsernameRx(),
			minLength(v.config.MinUsernameLen),
			maxLength(v.config.MaxUsernameLen),
			notInSet(v.config.forbiddenUsernames),
		),
		allOf(v.isAllowedEmailDomain()),
	)
}

func (v *validationContext) validHostedNewEmail() ValidatorFunc {
	return allOf(
		v.validHostedEmail(),
		v.isAvailableEmailAddr(),
	)
}

func (v *validationContext) validHostedMailingList() ValidatorFunc {
	return validateUsernameAndDomain(
		allOf(
			matchUsernameRx(),
			minLength(v.config.MinUsernameLen),
			maxLength(v.config.MaxUsernameLen),
			notInSet(v.config.forbiddenUsernames),
		),
		allOf(v.isAllowedDomain(ResourceTypeMailingList)),
	)
}

func (v *validationContext) validPassword() ValidatorFunc {
	return allOf(
		minLength(v.config.MinPasswordLen),
		maxLength(v.config.MaxPasswordLen),
		notInSet(v.config.forbiddenPasswords),
	)
}

func allOf(funcs ...ValidatorFunc) ValidatorFunc {
	return func(rctx *RequestContext, value string) error {
		for _, f := range funcs {
			if err := f(rctx, value); err != nil {
				return err
			}
		}
		return nil
	}
}

// ResourceValidatorFunc is a composite type validator that checks
// various fields in a Resource, depending on its type.
type ResourceValidatorFunc func(*RequestContext, *Resource, *User, bool) error

func (v *validationContext) validateResource(_ *RequestContext, r *Resource, user *User) error {
	// Validate the status enum.
	switch r.Status {
	case ResourceStatusActive, ResourceStatusInactive, ResourceStatusReadonly, ResourceStatusArchived:
	default:
		return newValidationError(nil, "status", "unknown resource status")
	}

	// If the resource has a ParentID, it must reference another
	// resource owned by the user.
	if !r.ParentID.Empty() {
		if user == nil {
			return newValidationError(nil, "parent_id", "resource can't have parent without user context")
		}
		if p := user.GetResourceByID(r.ParentID); p == nil {
			return newValidationError(nil, "parent_id", "parent references unknown resource")
		}
	}

	return nil
}

func (v *validationContext) validateShardedResource(rctx *RequestContext, r *Resource, user *User) error {
	if err := v.validateResource(rctx, r, user); err != nil {
		return err
	}
	if r.Shard == "" {
		return newValidationError(nil, "shard", "empty shard")
	}
	if !v.shards.IsAllowedShard(rctx.Context, r.Type, r.Shard) {
		return newValidationError(nil, "shard", fmt.Sprintf(
			"invalid shard %s for resource type %s (allowed: %v)",
			r.Shard,
			r.Type,
			v.shards.GetAllowedShards(rctx.Context, r.Type),
		))
	}
	if r.OriginalShard == "" {
		return newValidationError(nil, "original_shard", "empty original_shard")
	}
	return nil
}

func (v *validationContext) validEmailResource() ResourceValidatorFunc {
	emailValidator := v.validHostedEmail()
	newEmailValidator := v.validHostedNewEmail()

	return func(rctx *RequestContext, r *Resource, user *User, isNew bool) error {
		if err := v.validateShardedResource(rctx, r, user); err != nil {
			return err
		}

		// Email resources aren't nested.
		if !r.ParentID.Empty() {
			return newValidationError(nil, "parent_id", "resource should not have parent")
		}

		if r.Email == nil {
			return newValidationError(nil, "email", "resource has no email metadata")
		}

		var err error
		if isNew {
			err = newEmailValidator(rctx, r.Name)
		} else {
			err = emailValidator(rctx, r.Name)
		}
		if err != nil {
			return newValidationError(nil, "name", err.Error())
		}

		if r.Email.Maildir == "" {
			return newValidationError(nil, "email.maildir", "empty maildir")
		}
		return nil
	}
}

func (v *validationContext) validListResource() ResourceValidatorFunc {
	listValidator := v.validHostedMailingList()
	newListValidator := allOf(listValidator, v.isAvailableEmailAddr())

	return func(rctx *RequestContext, r *Resource, user *User, isNew bool) error {
		if err := v.validateShardedResource(rctx, r, user); err != nil {
			return err
		}
		if r.List == nil {
			return newValidationError(nil, "list", "resource has no list metadata")
		}

		var err error
		if isNew {
			err = newListValidator(rctx, r.Name)
		} else {
			err = listValidator(rctx, r.Name)
		}
		if err != nil {
			return newValidationError(nil, "name", err.Error())
		}

		if len(r.List.Admins) < 1 {
			return newValidationError(nil, "list.admins", "can't create a list without admins")
		}
		return nil
	}
}

func (v *validationContext) validNewsletterResource() ResourceValidatorFunc {
	listValidator := v.validHostedMailingList()
	newListValidator := allOf(listValidator, v.isAvailableEmailAddr())

	return func(rctx *RequestContext, r *Resource, user *User, isNew bool) error {
		if err := v.validateShardedResource(rctx, r, user); err != nil {
			return err
		}
		if r.Newsletter == nil {
			return newValidationError(nil, "newsletter", "resource has no newsletter metadata")
		}

		var err error
		if isNew {
			err = newListValidator(rctx, r.Name)
		} else {
			err = listValidator(rctx, r.Name)
		}
		if err != nil {
			return newValidationError(nil, "name", err.Error())
		}

		if len(r.Newsletter.Admins) < 1 {
			return newValidationError(nil, "newsletter.admins", "can't create a newsletter without admins")
		}
		return nil
	}
}

func findMatchingDAVAccount(user *User, r *Resource) *Resource {
	for _, dav := range user.GetResourcesByType(ResourceTypeDAV) {
		if isSubdir(dav.DAV.Homedir, r.Website.DocumentRoot) {
			return dav
		}
	}
	return nil
}

func hasMatchingDAVAccount(user *User, r *Resource) bool {
	return findMatchingDAVAccount(user, r) != nil
}

func (v *validationContext) validateUID(uid int, user *User) error {
	if uid == 0 {
		return errors.New("uid is not set")
	}
	if uid < v.config.MinUID || (v.config.MaxUID > 0 && uid > v.config.MaxUID) {
		return fmt.Errorf("uid %d outside of allowed range (%d-%d)", uid, v.config.MinUID, v.config.MaxUID)
	}
	if user != nil && uid != user.UID {
		return errors.New("uid of resource differs from uid of user")
	}
	return nil
}

func (v *validationContext) validateWebsiteCommon(r *Resource, user *User) error {
	// Document root checks: must not be empty, and the
	// user must have a DAV account with a home directory
	// that is a parent of this document root.
	if r.Website.DocumentRoot == "" {
		return newValidationError(nil, "document_root", "empty document_root")
	}
	if !isSubdir(v.config.WebsiteRootDir, r.Website.DocumentRoot) {
		return newValidationError(nil, "document_root", "document root outside of web root")
	}
	if !hasMatchingDAVAccount(user, r) {
		return newValidationError(nil, "website", "website has no matching DAV account")
	}

	// UID checks.
	if err := v.validateUID(r.Website.UID, user); err != nil {
		return newValidationError(nil, "website.uid", err.Error())
	}
	return nil
}

func (v *validationContext) validDomainResource() ResourceValidatorFunc {
	domainValidator := allOf(
		minLength(6),
		validDomainName,
		notInSet(v.config.forbiddenDomains),
	)
	newDomainValidator := allOf(
		domainValidator,
		v.isAvailableDomain(),
	)

	return func(rctx *RequestContext, r *Resource, user *User, isNew bool) error {
		if err := v.validateShardedResource(rctx, r, user); err != nil {
			return err
		}

		// Web resources aren't nested.
		if !r.ParentID.Empty() {
			return errors.New("resource should not have parent")
		}

		if r.Website == nil {
			return errors.New("resource has no website metadata")
		}

		var err error
		if isNew {
			err = newDomainValidator(rctx, r.Name)
		} else {
			err = domainValidator(rctx, r.Name)
		}
		if err != nil {
			return newValidationError(nil, "name", err.Error())
		}

		if r.Website.ParentDomain != "" {
			return newValidationError(nil, "parent_domain", "non-empty parent_domain on domain resource")
		}

		return v.validateWebsiteCommon(r, user)
	}
}

func (v *validationContext) validWebsiteResource() ResourceValidatorFunc {
	nameValidator := allOf(
		minLength(6),
		matchSitenameRx(),
	)
	newNameValidator := allOf(
		nameValidator,
		v.isAvailableWebsite(),
	)
	parentValidator := v.isAllowedDomain(ResourceTypeWebsite)

	return func(rctx *RequestContext, r *Resource, user *User, isNew bool) error {
		if err := v.validateShardedResource(rctx, r, user); err != nil {
			return err
		}

		// Web resources aren't nested.
		if !r.ParentID.Empty() {
			return errors.New("resource should not have parent")
		}

		if r.Website == nil {
			return errors.New("resource has no website metadata")
		}

		var err error
		if isNew {
			err = newNameValidator(rctx, r.Name)
		} else {
			err = nameValidator(rctx, r.Name)
		}
		if err != nil {
			return newValidationError(nil, "name", err.Error())
		}
		if err := parentValidator(rctx, r.Website.ParentDomain); err != nil {
			return err
		}

		return v.validateWebsiteCommon(r, user)
	}
}

func (v *validationContext) validDAVResource() ResourceValidatorFunc {
	davValidator := allOf(
		minLength(4),
		matchIdentifierRx(),
	)
	newDAVValidator := allOf(
		davValidator,
		v.isAvailableDAV(),
	)
	return func(rctx *RequestContext, r *Resource, user *User, isNew bool) error {
		if err := v.validateShardedResource(rctx, r, user); err != nil {
			return err
		}

		// DAV resources aren't nested.
		if !r.ParentID.Empty() {
			return errors.New("resource should not have parent")
		}

		var err error
		if isNew {
			err = newDAVValidator(rctx, r.Name)
		} else {
			err = davValidator(rctx, r.Name)
		}
		if err != nil {
			return newValidationError(nil, "name", err.Error())
		}

		if r.DAV == nil {
			return errors.New("resource has no dav metadata")
		}
		if !isSubdir(v.config.WebsiteRootDir, r.DAV.Homedir) {
			return newValidationError(nil, "homedir", "homedir outside of web root")
		}

		if err := v.validateUID(r.DAV.UID, user); err != nil {
			return newValidationError(nil, "dav.uid", err.Error())
		}
		return nil
	}
}

func (v *validationContext) validDatabaseResource() ResourceValidatorFunc {
	dbValidator := allOf(
		minLength(4),
		matchIdentifierRx(),
	)
	newDBValidator := allOf(
		dbValidator,
		v.isAvailableDatabase(),
	)
	return func(rctx *RequestContext, r *Resource, user *User, isNew bool) error {
		if err := v.validateShardedResource(rctx, r, user); err != nil {
			return err
		}

		// Database resources must be nested below a website.
		if r.ParentID.Empty() {
			return errors.New("database resources should be nested")
		}

		// TODO: move to global validation
		// switch r.ParentID.Type() {
		// case ResourceTypeWebsite, ResourceTypeDomain:
		// default:
		// 	return errors.New("database parent is not a website resource")
		// }

		var err error
		if isNew {
			err = newDBValidator(rctx, r.Name)
		} else {
			err = dbValidator(rctx, r.Name)
		}
		if err != nil {
			return newValidationError(nil, "name", err.Error())
		}

		if r.Database == nil {
			return errors.New("resource has no database metadata")
		}
		return nil
	}
}

// Validator for arbitrary resource types.
type resourceValidator struct {
	rvs map[string]ResourceValidatorFunc
}

func newResourceValidator(v *validationContext) *resourceValidator {
	return &resourceValidator{
		rvs: map[string]ResourceValidatorFunc{
			ResourceTypeEmail:       v.validEmailResource(),
			ResourceTypeMailingList: v.validListResource(),
			ResourceTypeNewsletter:  v.validNewsletterResource(),
			ResourceTypeDomain:      v.validDomainResource(),
			ResourceTypeWebsite:     v.validWebsiteResource(),
			ResourceTypeDAV:         v.validDAVResource(),
			ResourceTypeDatabase:    v.validDatabaseResource(),
		},
	}
}

func (v *resourceValidator) validateResource(rctx *RequestContext, r *Resource, user *User, isNew bool) error {
	// Obvious basic sanity checks on the Resource parameters.
	if r.Name == "" {
		return newValidationError(nil, "name", "resource name unset")
	}
	if r.Type == "" {
		return newValidationError(nil, "type", "resource type unset")
	}

	rv, ok := v.rvs[r.Type]
	if !ok {
		return newValidationError(nil, "type", fmt.Sprintf("unknown resource type '%s'", r.Type))
	}

	return rv(rctx, r, user, isNew)
}

// Common validators for specific field types.
type fieldValidators struct {
	password ValidatorFunc
	newEmail ValidatorFunc
}

func newFieldValidators(v *validationContext) *fieldValidators {
	return &fieldValidators{
		password: v.validPassword(),
		newEmail: v.validHostedNewEmail(),
	}
}

// UserValidatorFunc is a compound validator for User objects.
type UserValidatorFunc func(*RequestContext, *User, bool) error

// Verify that user-level invariants are respected. This check can be applied
// to new or existing objects.
//
// nolint: gocyclo
func checkUserInvariants(user *User) error {
	// Count email resources. An account must have exactly 1 email
	// resource.
	var email *Resource
	var emailCount int
	for _, rsrc := range user.Resources {
		if rsrc.Type == ResourceTypeEmail {
			email = rsrc
			emailCount++
		}
	}
	if emailCount == 0 {
		return errors.New("account is missing email resource")
	}
	if emailCount > 1 {
		return errors.New("account can't have more than one email resource")
	}
	if email.Name != user.Name {
		return errors.New("email and username do not match")
	}

	// Check UID.
	if user.UID == 0 {
		return errors.New("UID unset")
	}
	// Check that all UIDs are the same.
	for _, rsrc := range user.Resources {
		switch rsrc.Type {
		case ResourceTypeWebsite, ResourceTypeDomain:
			if rsrc.Website.UID != user.UID {
				return fmt.Errorf("UID of resource %s does not match user (%d vs %d)", rsrc.String(), rsrc.Website.UID, user.UID)
			}
		case ResourceTypeDAV:
			if rsrc.DAV.UID != user.UID {
				return fmt.Errorf("UID of resource %s does not match user (%d vs %d)", rsrc.String(), rsrc.DAV.UID, user.UID)
			}
		}
	}

	return nil
}

// A custom validator for new User objects.
func (v *validationContext) validUser() UserValidatorFunc {
	nameValidator := v.validHostedEmail()
	newNameValidator := v.validHostedNewEmail()
	return func(rctx *RequestContext, user *User, isNew bool) error {
		var err error
		if isNew {
			err = newNameValidator(rctx, user.Name)
		} else {
			err = nameValidator(rctx, user.Name)
		}
		if err != nil {
			return err
		}
		return checkUserInvariants(user)
	}
}

type templateContext struct {
	shards  shardBackend
	webroot string
}

func (c *templateContext) pickShard(ctx context.Context, r *Resource) (string, error) {
	avail := c.shards.GetAvailableShards(ctx, r.Type)
	if len(avail) == 0 {
		return "", fmt.Errorf("no available shards for resource type %s", r.Type)
	}
	return avail[rand.Intn(len(avail))], nil
}

func (c *templateContext) setResourceShard(ctx context.Context, r *Resource, ref *Resource) error {
	if r.Shard == "" {
		if ref != nil {
			r.Shard = ref.Shard
		} else {
			s, err := c.pickShard(ctx, r)
			if err != nil {
				return err
			}
			r.Shard = s
		}
	}
	if r.OriginalShard == "" {
		r.OriginalShard = r.Shard
	}
	return nil
}

func (c *templateContext) setResourceStatus(r *Resource) {
	if r.Status == "" {
		r.Status = ResourceStatusActive
	}
}

func (c *templateContext) setCommonResourceAttrs(ctx context.Context, r *Resource, ref *Resource, user *User) error {
	// If we reference another resource, ensure it has been templated.
	if ref != nil {
		if err := c.applyTemplate(ctx, ref, user); err != nil {
			return err
		}
	}

	r.CreatedAt = time.Now().UTC().Format("2006-01-02")

	c.setResourceStatus(r)
	return c.setResourceShard(ctx, r, ref)
}

// Apply default values to an Email resource.
func (c *templateContext) emailResourceTemplate(ctx context.Context, r *Resource, _ *User) error {
	// Force the email address to lowercase.
	r.Name = strings.ToLower(r.Name)

	if r.Email == nil {
		r.Email = new(Email)
	}

	addrParts := strings.Split(r.Name, "@")
	if len(addrParts) != 2 {
		return errors.New("malformed name")
	}
	r.Email.Maildir = fmt.Sprintf("%s/%s", addrParts[1], addrParts[0])
	r.Email.QuotaLimit = 4096
	return c.setCommonResourceAttrs(ctx, r, nil, nil)
}

// Apply default values to a Website or Domain resource.
func (c *templateContext) websiteResourceTemplate(ctx context.Context, r *Resource, user *User) error {
	if user == nil {
		return errors.New("website resource needs owner")
	}

	// Force the website address to lowercase.
	r.Name = strings.ToLower(r.Name)

	if r.Website == nil {
		r.Website = new(Website)
	}

	// If the client did not specify a DocumentRoot, find a DAV resource
	// and associate the website with it.
	if r.Website.DocumentRoot == "" {
		dav := user.GetSingleResourceByType(ResourceTypeDAV)
		if dav == nil {
			return errors.New("user has no DAV accounts")
		}

		// The DAV resource may not have been templatized yet.
		if dav.DAV == nil || dav.DAV.Homedir == "" {
			if err := c.davResourceTemplate(ctx, dav, user); err != nil {
				return err
			}
		}
		r.Website.DocumentRoot = filepath.Join(dav.DAV.Homedir, "html-"+r.Name)
	}
	r.Website.DocumentRoot = filepath.Clean(r.Website.DocumentRoot)

	if len(r.Website.Options) == 0 {
		r.Website.Options = []string{"nomail"}
	}

	r.Website.UID = user.UID

	dav := findMatchingDAVAccount(user, r)
	if dav == nil {
		return fmt.Errorf("no DAV resources matching website %s", r.String())
	}
	return c.setCommonResourceAttrs(ctx, r, dav, user)
}

// Apply default values to a DAV resource.
func (c *templateContext) davResourceTemplate(ctx context.Context, r *Resource, user *User) error {
	if user == nil {
		return errors.New("dav resource needs owner")
	}

	// Force the account name to lowercase.
	r.Name = strings.ToLower(r.Name)

	if r.DAV == nil {
		r.DAV = new(WebDAV)
	}
	if r.DAV.Homedir == "" {
		r.DAV.Homedir = filepath.Join(c.webroot, r.Name)
	}
	r.DAV.Homedir = filepath.Clean(r.DAV.Homedir)
	r.DAV.UID = user.UID

	return c.setCommonResourceAttrs(ctx, r, nil, user)
}

// Apply default values to a Database resource.
func (c *templateContext) databaseResourceTemplate(ctx context.Context, r *Resource, user *User) error {
	if user == nil {
		return errors.New("database resource needs owner")
	}

	// Force the database name to lowercase.
	r.Name = strings.ToLower(r.Name)

	if r.Database == nil {
		r.Database = new(Database)
	}
	if r.Database.DBUser == "" {
		r.Database.DBUser = r.Name
	}

	return c.setCommonResourceAttrs(ctx, r, user.GetResourceByID(r.ParentID), user)
}

// Apply default values to a MailingList resource.
func (c *templateContext) listResourceTemplate(ctx context.Context, r *Resource, user *User) error {
	// Force the list address to lowercase.
	r.Name = strings.ToLower(r.Name)

	if r.List == nil {
		r.List = new(MailingList)
	}

	// As a convenience, if a user is passed in the context, we add it to
	// the list admins.
	if user != nil && len(r.List.Admins) == 0 {
		r.List.Admins = []string{user.Name}
	}

	return c.setCommonResourceAttrs(ctx, r, nil, nil)
}

// Apply default values to a Newsletter resource.
func (c *templateContext) newsletterResourceTemplate(ctx context.Context, r *Resource, user *User) error {
	// Force the list address to lowercase.
	r.Name = strings.ToLower(r.Name)

	if r.Newsletter == nil {
		r.Newsletter = new(Newsletter)
	}

	// As a convenience, if a user is passed in the context, we add it to
	// the list admins.
	if user != nil && len(r.Newsletter.Admins) == 0 {
		r.Newsletter.Admins = []string{user.Name}
	}

	return c.setCommonResourceAttrs(ctx, r, nil, nil)
}

// Apply default values to a resource.
func (c *templateContext) applyTemplate(ctx context.Context, r *Resource, user *User) error {
	switch r.Type {
	case ResourceTypeEmail:
		return c.emailResourceTemplate(ctx, r, user)
	case ResourceTypeWebsite, ResourceTypeDomain:
		return c.websiteResourceTemplate(ctx, r, user)
	case ResourceTypeDAV:
		return c.davResourceTemplate(ctx, r, user)
	case ResourceTypeDatabase:
		return c.databaseResourceTemplate(ctx, r, user)
	case ResourceTypeMailingList:
		return c.listResourceTemplate(ctx, r, user)
	case ResourceTypeNewsletter:
		return c.newsletterResourceTemplate(ctx, r, user)
	}
	return nil
}

func isSubdir(root, dir string) bool {
	return strings.HasPrefix(dir, root+"/")
}
