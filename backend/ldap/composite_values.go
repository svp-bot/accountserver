package ldapbackend

import (
	"encoding/base64"

	as "git.autistici.org/ai3/accountserver"
	ct "git.autistici.org/ai3/go-common/ldap/compositetypes"
)

// Note: all functions below deliberately ignore deserialization
// errors (the broken entry is just skipped). This way, broken data in
// the database will be invisible and not break the app. Not that it
// is an overall better strategy than crashing, but it's a bit gentler
// on the user.

func newAppSpecificPassword(info as.AppSpecificPasswordInfo, pw string) *ct.AppSpecificPassword {
	return &ct.AppSpecificPassword{
		ID:                info.ID,
		Service:           info.Service,
		Comment:           info.Comment,
		EncryptedPassword: pw,
	}
}

func decodeAppSpecificPasswords(values []string) []*ct.AppSpecificPassword {
	var out []*ct.AppSpecificPassword
	for _, value := range values {
		if asp, err := ct.UnmarshalAppSpecificPassword(value); err == nil {
			out = append(out, asp)
		}
	}
	return out
}

func excludeASPFromList(asps []*ct.AppSpecificPassword, id string) []*ct.AppSpecificPassword {
	var out []*ct.AppSpecificPassword
	for _, asp := range asps {
		if asp.ID != id {
			out = append(out, asp)
		}
	}
	return out
}

func encodeAppSpecificPasswords(asps []*ct.AppSpecificPassword) []string {
	var out []string
	for _, asp := range asps {
		out = append(out, asp.Marshal())
	}
	return out
}

func getASPInfo(asps []*ct.AppSpecificPassword) []*as.AppSpecificPasswordInfo {
	var out []*as.AppSpecificPasswordInfo
	for _, asp := range asps {
		out = append(out, &as.AppSpecificPasswordInfo{
			ID:      asp.ID,
			Service: asp.Service,
			Comment: asp.Comment,
		})
	}
	return out
}

func decodeUserEncryptionKeys(values []string) []*ct.EncryptedKey {
	var out []*ct.EncryptedKey
	for _, value := range values {
		k, err := ct.UnmarshalEncryptedKey(value)
		if err != nil {
			continue
		}
		out = append(out, k)
	}
	return out
}

func encodeUserEncryptionKeys(keys []*ct.EncryptedKey) []string {
	var out []string
	for _, key := range keys {
		out = append(out, key.Marshal())
	}
	return out
}

func decodeU2FRegistrations(encRegs []string) []*as.U2FRegistration {
	var out []*as.U2FRegistration
	for _, enc := range encRegs {
		if r, err := ct.UnmarshalU2FRegistration(enc); err == nil {
			// Convert ct.U2FRegistration (internal) ->
			// as.U2FRegistration (public) by
			// base64-encoding the data.
			out = append(out, &as.U2FRegistration{
				KeyHandle: base64.StdEncoding.EncodeToString(r.KeyHandle),
				PublicKey: base64.StdEncoding.EncodeToString(r.PublicKey),
			})
		}
	}
	return out
}

func encodeU2FRegistrations(regs []*as.U2FRegistration) []string {
	var out []string
	for _, r := range regs {
		// Convert as.U2FRegistration (public) ->
		// ct.U2FRegistration (internal) by base64-decoding
		// the data.
		kh, err := base64.StdEncoding.DecodeString(r.KeyHandle)
		if err != nil {
			continue
		}
		pk, err := base64.StdEncoding.DecodeString(r.PublicKey)
		if err != nil {
			continue
		}
		ctr := ct.U2FRegistration{
			KeyHandle: kh,
			PublicKey: pk,
		}
		out = append(out, ctr.Marshal())
	}
	return out
}
