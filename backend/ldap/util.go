package ldapbackend

import (
	"os"
	"strconv"
	"strings"
	"time"

	"github.com/go-ldap/ldap/v3"
)

// queryTemplate is the template for a single parametrized LDAP query.
type queryTemplate struct {
	Base   string
	Filter string
	Scope  int
	Attrs  []string
}

// A map of variables to be replaced in a LDAP filter string.
type templateVars map[string]interface{}

// A rawVariable is a string that won't be escaped.
type rawVariable string

func (q *queryTemplate) query(vars templateVars) *ldap.SearchRequest {
	filter := q.Filter
	if filter == "" {
		filter = "(objectClass=*)"
	}

	return ldap.NewSearchRequest(
		replaceVars(q.Base, vars),
		q.Scope,
		ldap.NeverDerefAliases,
		0,
		0,
		false,
		replaceVars(filter, vars),
		q.Attrs,
		nil,
	)
}

func replaceVars(s string, vars templateVars) string {
	return os.Expand(s, func(k string) string {
		i, ok := vars[k]
		if !ok {
			return ""
		}
		switch v := i.(type) {
		case rawVariable:
			return string(v)
		case string:
			return ldap.EscapeFilter(v)
		default:
			return ""
		}
	})
}

// LDAP string to boolean value. There is no schema defined for this,
// we just match a bunch of plausible text truth values ("yes", "on",
// "true", etc).
func s2b(s string) bool {
	switch s {
	case "yes", "y", "on", "enabled", "true":
		return true
	default:
		return false
	}
}

// Bool to LDAP value. Encoded as true/false.
func b2s(b bool) string {
	if b {
		return "true"
	}
	return "false"
}

// Convert a string to a []string with a single item, or nil if the
// string is empty. Useful for optional single-valued LDAP attributes.
func s2l(s string) []string {
	if s == "" {
		return nil
	}
	return []string{s}
}

// Returns true if a LDAP object has the specified objectClass.
func isObjectClass(entry *ldap.Entry, class string) bool {
	classes := entry.GetAttributeValues("objectClass")
	for _, c := range classes {
		if c == class {
			return true
		}
	}
	return false
}

// Join some RDNs.
func joinDN(parts ...string) string {
	return strings.Join(parts, ",")
}

// Find the uid= in the DN. Return an empty string on failure.
func ownerFromDN(dn string) string {
	parsed, err := ldap.ParseDN(dn)
	if err != nil {
		return ""
	}
	// Start looking for uid from the right. The strict
	// greater-than clause ignores the first RDN (so that the
	// owner of a user is not itself).
	for i := len(parsed.RDNs) - 1; i > 0; i-- {
		attr := parsed.RDNs[i].Attributes[0]
		if attr.Type == "uid" {
			return attr.Value
		}
	}
	return ""
}

// Functions to encode/decode shadow(5) timestamps (number of days
// since the epoch).
const oneDay = 86400

func encodeShadowTimestamp(t time.Time) string {
	d := t.UTC().Unix() / oneDay
	return strconv.FormatInt(d, 10)
}

func decodeShadowTimestamp(s string) (t time.Time) {
	if i, err := strconv.ParseInt(s, 10, 64); err == nil {
		t = time.Unix(i*oneDay, 0).UTC()
	}
	return
}
