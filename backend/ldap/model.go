package ldapbackend

import (
	"bytes"
	"context"
	"errors"
	"fmt"
	"math/rand"
	"strconv"
	"strings"
	"time"

	ldaputil "git.autistici.org/ai3/go-common/ldap"
	ct "git.autistici.org/ai3/go-common/ldap/compositetypes"
	"github.com/go-ldap/ldap/v3"

	as "git.autistici.org/ai3/accountserver"
)

const (
	// Names of some well-known LDAP attributes.
	totpSecretLDAPAttr         = "totpSecret"
	preferredLanguageLDAPAttr  = "preferredLanguage"
	recoveryHintLDAPAttr       = "recoverQuestion"
	recoveryResponseLDAPAttr   = "recoverAnswer"
	aspLDAPAttr                = "appSpecificPassword"
	storagePublicKeyLDAPAttr   = "storagePublicKey"
	storagePrivateKeyLDAPAttr  = "storageEncryptedSecretKey"
	passwordLDAPAttr           = "userPassword"
	passwordLastChangeLDAPAttr = "shadowLastChange"
	u2fRegistrationsLDAPAttr   = "u2fRegistration"
	uidNumberLDAPAttr          = "uidNumber"
	gidNumberLDAPAttr          = "gidNumber"
	creationDateLDAPAttr       = "creationDate"
)

// Interface to something that adds Resources to a User.
type userResourceFinder interface {
	GetResources(context.Context, *backendTX, *as.RawUser) ([]*as.Resource, error)
}

// backend is the interface to an LDAP-backed user database.
//
// We keep a set of LDAP queries for each resource type, each having a
// "resource" query to return a specific resource belonging to a user,
// and a "presence" query that checks for existence of a resource for
// all users.
type backend struct {
	conn                ldapConn
	baseDN              string
	userQuery           *queryTemplate
	searchUserQuery     *queryTemplate
	userResourceQueries []userResourceFinder
	resources           *resourceRegistry

	// Range for new user IDs.
	minUID, maxUID int
}

const (
	defaultMinUID = 10000
	defaultMaxUID = 1000000
)

// backendTX holds the business logic (that runs within a single
// transaction).
type backendTX struct {
	*ldapTX
	backend *backend
}

const ldapPoolSize = 20

func (b *backend) NewTransaction() (as.TX, error) {
	return &backendTX{
		ldapTX:  newLDAPTX(b.conn),
		backend: b,
	}, nil
}

// NewLDAPBackend initializes an LDAPBackend object with the given LDAP
// connection pool.
func NewLDAPBackend(uri, bindDN, bindPw, base string) (as.Backend, error) {
	pool, err := ldaputil.NewConnectionPool(uri, bindDN, bindPw, ldapPoolSize)
	if err != nil {
		return nil, err
	}
	return newLDAPBackendWithConn(pool, base)
}

func newLDAPBackendWithConn(conn ldapConn, baseDN string) (*backend, error) {
	reg := newDefaultResourceRegistry(baseDN)

	return &backend{
		conn:   conn,
		baseDN: baseDN,
		userQuery: &queryTemplate{
			Base:   joinDN("uid=${user}", "ou=People", baseDN),
			Filter: "(objectClass=*)",
			Scope:  ldap.ScopeBaseObject,
		},
		searchUserQuery: &queryTemplate{
			Base:   joinDN("ou=People", baseDN),
			Filter: "(uid=${pattern}*)",
			Scope:  ldap.ScopeSingleLevel,
		},
		userResourceQueries: []userResourceFinder{
			// Find all resources that are children of the main uid object.
			newTemplateLDAPResourceFinder(&queryTemplate{
				Base:  joinDN("uid=${user}", "ou=People", baseDN),
				Scope: ldap.ScopeWholeSubtree,
			}),
			// Find mailing lists, which are nested under a
			// different root. This must be run after having
			// discovered the base resources, so we can use the list
			// of email aliases as listOwners.
			newMailingListResourceFinder(baseDN),
			newNewsletterResourceFinder(baseDN),
		},
		resources: reg,
		minUID:    defaultMinUID,
		maxUID:    defaultMaxUID,
	}, nil
}

func newUser(entry *ldap.Entry) (*as.RawUser, error) {
	// Note that some user-level attributes related to
	// authentication are stored on the uid= object, while others
	// are on the email= object. We set the latter in the GetUser
	// function later.
	//
	// The case of password recovery attributes is more complex:
	// the current schema has those on email=, but we'd like to
	// move them to uid=, so we currently have to support both
	// (but the uid= one takes precedence).
	uidNumber, _ := strconv.Atoi(entry.GetAttributeValue(uidNumberLDAPAttr)) // nolint
	user := &as.RawUser{
		User: as.User{
			Name:                    entry.GetAttributeValue("uid"),
			Lang:                    entry.GetAttributeValue(preferredLanguageLDAPAttr),
			UID:                     uidNumber,
			Status:                  entry.GetAttributeValue("status"),
			Shard:                   entry.GetAttributeValue("host"),
			LastPasswordChangeStamp: decodeShadowTimestamp(entry.GetAttributeValue(passwordLastChangeLDAPAttr)),
			AccountRecoveryHint:     entry.GetAttributeValue(recoveryHintLDAPAttr),
			U2FRegistrations:        decodeU2FRegistrations(entry.GetAttributeValues(u2fRegistrationsLDAPAttr)),
			AppSpecificPasswords:    getASPInfo(decodeAppSpecificPasswords(entry.GetAttributeValues(aspLDAPAttr))),
			HasOTP:                  entry.GetAttributeValue(totpSecretLDAPAttr) != "",
		},
		// Remove the legacy LDAP {crypt} prefix on old passwords.
		Password:         strings.TrimPrefix(entry.GetAttributeValue(passwordLDAPAttr), "{crypt}"),
		RecoveryPassword: strings.TrimPrefix(entry.GetAttributeValue(recoveryResponseLDAPAttr), "{crypt}"),
	}

	// The user has 2FA enabled if it has a TOTP secret or U2F keys.
	user.Has2FA = (user.HasOTP || (len(user.U2FRegistrations) > 0))

	if user.Lang == "" {
		user.Lang = "en"
	}
	return user, nil
}

func userToLDAP(user *as.User) (attrs []ldap.PartialAttribute) {
	// Most attributes are read-only and have specialized methods to set them.
	attrs = append(attrs, []ldap.PartialAttribute{
		{Type: "objectClass", Vals: []string{"top", "person", "posixAccount", "shadowAccount", "organizationalPerson", "inetOrgPerson", "investiciUser"}},
		{Type: "uid", Vals: s2l(user.Name)},
		{Type: "cn", Vals: s2l(user.Name)},
		{Type: uidNumberLDAPAttr, Vals: s2l(strconv.Itoa(user.UID))},
		{Type: gidNumberLDAPAttr, Vals: s2l("2000")},
		{Type: "givenName", Vals: s2l("Private")},
		{Type: "sn", Vals: s2l("Private")},
		{Type: "gecos", Vals: s2l(user.Name)},
		{Type: "loginShell", Vals: s2l("/bin/false")},
		{Type: "homeDirectory", Vals: s2l("/var/empty")},
		{Type: passwordLastChangeLDAPAttr, Vals: s2l("12345")},
		{Type: "status", Vals: s2l(user.Status)},
		{Type: "host", Vals: s2l(user.Shard)},
		{Type: "shadowWarning", Vals: s2l("7")},
		{Type: "shadowMax", Vals: s2l("99999")},
		{Type: preferredLanguageLDAPAttr, Vals: s2l(user.Lang)},
		{Type: u2fRegistrationsLDAPAttr, Vals: encodeU2FRegistrations(user.U2FRegistrations)},
	}...)
	return
}

func (tx *backendTX) getUserDN(user *as.User) string {
	return getUserDN(user, tx.backend.baseDN)
}

func getUserDN(user *as.User, baseDN string) string {
	return joinDN("uid="+user.Name, "ou=People", baseDN)
}

// CreateUser creates a new user.
func (tx *backendTX) CreateUser(ctx context.Context, user *as.User) (*as.User, error) {
	dn := tx.getUserDN(user)

	tx.create(dn)
	for _, attr := range userToLDAP(user) {
		tx.setAttr(dn, attr.Type, attr.Vals...)
	}

	// Create all resources.
	rsrcs, err := tx.CreateResources(ctx, user, user.Resources)
	if err != nil {
		return nil, err
	}
	user.Resources = rsrcs

	return user, nil
}

// UpdateUser updates values for the user only (not the resources).
func (tx *backendTX) UpdateUser(ctx context.Context, user *as.User) error {
	dn := tx.getUserDN(user)
	for _, attr := range userToLDAP(user) {
		tx.setAttr(dn, attr.Type, attr.Vals...)
	}
	return nil
}

type ldapQuery interface {
	query(*as.RawUser) *ldap.SearchRequest
}

// Find resources for a user using a LDAP queryTemplate.
type ldapTemplateQuery struct {
	*queryTemplate
}

func (t *ldapTemplateQuery) query(user *as.RawUser) *ldap.SearchRequest {
	return t.queryTemplate.query(templateVars{"user": user.Name})
}

func newTemplateLDAPResourceFinder(tpl *queryTemplate) userResourceFinder {
	return &ldapUserResourceFinder{&ldapTemplateQuery{tpl}}
}

// Find mailing lists using a dynamic LDAP query with all the user aliases.
type mailingListResourceFinder struct {
	baseDN string
}

func (m *mailingListResourceFinder) query(user *as.RawUser) *ldap.SearchRequest {
	// Build the LDAP query filter with all the user aliases.
	var b bytes.Buffer
	fmt.Fprintf(&b, "(&(objectClass=mailingList)(|(")
	for i, addr := range user.AllEmailAddrs() {
		if i > 0 {
			fmt.Fprintf(&b, ")(")
		}
		fmt.Fprintf(&b, "listOwner=%s", ldap.EscapeFilter(addr))
	}
	fmt.Fprintf(&b, ")))")

	return ldap.NewSearchRequest(
		m.baseDN,
		ldap.ScopeSingleLevel,
		ldap.NeverDerefAliases,
		0,
		0,
		false,
		b.String(),
		nil,
		nil,
	)
}

func newMailingListResourceFinder(baseDN string) userResourceFinder {
	return &ldapUserResourceFinder{&mailingListResourceFinder{joinDN("ou=Lists", baseDN)}}
}

func newNewsletterResourceFinder(baseDN string) userResourceFinder {
	return &ldapUserResourceFinder{&mailingListResourceFinder{joinDN("ou=Newsletters", baseDN)}}
}

// Find resources for a user using a generic LDAP query.
type ldapUserResourceFinder struct {
	ldapQuery
}

func (f *ldapUserResourceFinder) GetResources(ctx context.Context, tx *backendTX, user *as.RawUser) ([]*as.Resource, error) {
	result, err := tx.search(ctx, f.ldapQuery.query(user))
	if err != nil {
		return nil, err
	}

	var resources []*as.Resource
	for _, entry := range result.Entries {
		// Some user-level attributes are actually stored on the email
		// object, which is desired in some cases, but in others is a
		// shortcoming of the legacy A/I database model. Set them on the
		// main User object. For the latter, attributes on the main User
		// object take precedence.
		if isObjectClass(entry, "virtualMailUser") {
			if user.AccountRecoveryHint == "" {
				if s := entry.GetAttributeValue(recoveryHintLDAPAttr); s != "" {
					user.AccountRecoveryHint = s
				}
			}

			user.Keys = decodeUserEncryptionKeys(
				entry.GetAttributeValues(storagePrivateKeyLDAPAttr))
			user.HasEncryptionKeys = (entry.GetAttributeValue(storagePublicKeyLDAPAttr) != "")
		}

		// Parse the resource.
		if r, err := tx.backend.resources.FromLDAP(entry); err == nil {
			resources = append(resources, r)
		}
	}

	return resources, nil
}

// GetUser returns a user.
func (tx *backendTX) GetUser(ctx context.Context, username string) (*as.RawUser, error) {
	// First of all, find the main user object, and just that one.
	vars := templateVars{"user": username}
	result, err := tx.search(ctx, tx.backend.userQuery.query(vars))
	if err != nil {
		if ldap.IsErrorWithCode(err, ldap.LDAPResultNoSuchObject) {
			return nil, nil
		}
		return nil, err
	}
	if len(result.Entries) == 0 {
		return nil, nil
	}

	user, err := newUser(result.Entries[0])
	if err != nil {
		return nil, err
	}

	// Now run the resource queries, and accumulate results on the User
	// object we just created.
	// TODO: parallelize.
	// TODO: add support for non-LDAP resource queries.
	for _, f := range tx.backend.userResourceQueries {
		if resources, err := f.GetResources(ctx, tx, user); err == nil {
			user.Resources = append(user.Resources, resources...)
		}
	}

	return user, nil
}

func (tx *backendTX) SearchUser(ctx context.Context, pattern string, limit int) ([]string, error) {
	// First of all, find the main user object, and just that one.
	vars := templateVars{"pattern": rawVariable(pattern)}
	req := tx.backend.searchUserQuery.query(vars)
	result, err := tx.search(ctx, req)
	if err != nil {
		return nil, err
	}
	if len(result.Entries) == 0 {
		return nil, nil
	}

	var out []string
	for i, entry := range result.Entries {
		if limit > 0 && i >= limit {
			break
		}
		out = append(out, entry.GetAttributeValue("uid"))
	}
	return out, nil
}

func (tx *backendTX) SetUserPassword(ctx context.Context, user *as.User, encryptedPassword string) (err error) {
	dn := tx.getUserDN(user)
	tx.setAttr(dn, passwordLDAPAttr, encryptedPassword)
	tx.setAttr(dn, passwordLastChangeLDAPAttr, encodeShadowTimestamp(time.Now()))
	for _, r := range user.GetResourcesByType(as.ResourceTypeEmail) {
		dn, err = tx.backend.resources.GetDN(r.ID)
		if err != nil {
			return
		}
		tx.setAttr(dn, passwordLDAPAttr, encryptedPassword)
	}
	return
}

func (tx *backendTX) SetAccountRecoveryHint(ctx context.Context, user *as.User, hint, response string) error {
	// Write the password recovery attributes on the uid= object,
	// as per the new schema.
	dn := tx.getUserDN(user)
	tx.setAttr(dn, recoveryHintLDAPAttr, hint)
	tx.setAttr(dn, recoveryResponseLDAPAttr, response)
	return nil
}

func (tx *backendTX) DeleteAccountRecoveryHint(ctx context.Context, user *as.User) error {
	// Write the password recovery attributes on the uid= object,
	// as per the new schema.
	dn := tx.getUserDN(user)
	tx.setAttr(dn, recoveryHintLDAPAttr)
	tx.setAttr(dn, recoveryResponseLDAPAttr)
	return nil
}

func (tx *backendTX) SetUserEncryptionKeys(ctx context.Context, user *as.User, keys []*ct.EncryptedKey) error {
	encKeys := encodeUserEncryptionKeys(keys)
	for _, r := range user.GetResourcesByType(as.ResourceTypeEmail) {
		dn, err := tx.backend.resources.GetDN(r.ID)
		if err != nil {
			return err
		}
		tx.setAttr(dn, storagePrivateKeyLDAPAttr, encKeys...)
	}
	return nil
}

func (tx *backendTX) SetUserEncryptionPublicKey(ctx context.Context, user *as.User, pub []byte) error {
	for _, r := range user.GetResourcesByType(as.ResourceTypeEmail) {
		dn, err := tx.backend.resources.GetDN(r.ID)
		if err != nil {
			return err
		}
		tx.setAttr(dn, storagePublicKeyLDAPAttr, string(pub))
	}
	return nil
}

func (tx *backendTX) SetApplicationSpecificPassword(ctx context.Context, user *as.User, info *as.AppSpecificPasswordInfo, encryptedPassword string) error {
	dn := tx.getUserDN(user)
	asps := decodeAppSpecificPasswords(tx.readAttributeValues(ctx, dn, aspLDAPAttr))
	asps = append(excludeASPFromList(asps, info.ID), newAppSpecificPassword(*info, encryptedPassword))
	outASPs := encodeAppSpecificPasswords(asps)
	tx.setAttr(dn, aspLDAPAttr, outASPs...)
	return nil
}

func (tx *backendTX) DeleteApplicationSpecificPassword(ctx context.Context, user *as.User, id string) error {
	dn := tx.getUserDN(user)
	asps := decodeAppSpecificPasswords(tx.readAttributeValues(ctx, dn, aspLDAPAttr))
	asps = excludeASPFromList(asps, id)
	outASPs := encodeAppSpecificPasswords(asps)
	tx.setAttr(dn, aspLDAPAttr, outASPs...)
	return nil
}

func (tx *backendTX) SetUserTOTPSecret(ctx context.Context, user *as.User, secret string) error {
	tx.setAttr(tx.getUserDN(user), totpSecretLDAPAttr, secret)
	return nil
}

func (tx *backendTX) DeleteUserTOTPSecret(ctx context.Context, user *as.User) error {
	tx.setAttr(tx.getUserDN(user), totpSecretLDAPAttr)
	return nil
}

func (tx *backendTX) SetResourcePassword(ctx context.Context, r *as.Resource, encryptedPassword string) error {
	dn, err := tx.backend.resources.GetDN(r.ID)
	if err != nil {
		return err
	}
	tx.setAttr(dn, passwordLDAPAttr, encryptedPassword)
	return nil
}

func (tx *backendTX) hasResource(ctx context.Context, resourceType, resourceName string) (bool, error) {
	tpl, err := tx.backend.resources.SearchQuery(resourceType)
	if err != nil {
		return false, err
	}

	// Make a quick LDAP search that only fetches the DN attribute.
	tpl.Attrs = []string{"dn"}
	result, err := tx.search(ctx, tpl.query(templateVars{
		"resource": resourceName,
		"type":     resourceType,
	}))
	if err != nil {
		if ldap.IsErrorWithCode(err, ldap.LDAPResultNoSuchObject) {
			return false, nil
		}
		return false, err
	}
	if len(result.Entries) == 0 {
		return false, nil
	}
	return true, nil
}

// HasAnyResource returns true if any of the specified resources exists.
func (tx *backendTX) HasAnyResource(ctx context.Context, resourceIDs []as.FindResourceRequest) (bool, error) {
	for _, req := range resourceIDs {
		has, err := tx.hasResource(ctx, req.Type, req.Name)
		if err != nil || has {
			return has, err
		}
	}
	return false, nil
}

func (tx *backendTX) searchResourcesByType(ctx context.Context, pattern, resourceType string, limit int) ([]*as.RawResource, error) {
	tpl, err := tx.backend.resources.SearchQuery(resourceType)
	if err != nil {
		return nil, err
	}
	req := tpl.query(templateVars{
		"resource": rawVariable(pattern),
		"type":     resourceType,
	})
	result, err := tx.search(ctx, req)
	if err != nil {
		if ldap.IsErrorWithCode(err, ldap.LDAPResultNoSuchObject) {
			return nil, nil
		}
		return nil, err
	}
	if len(result.Entries) == 0 {
		return nil, nil
	}

	var out []*as.RawResource
	for i, entry := range result.Entries {
		if limit > 0 && i >= limit {
			break
		}
		rsrc, err := tx.backend.resources.FromLDAP(entry)
		if err != nil {
			return nil, err
		}
		out = append(out, &as.RawResource{
			Resource: *rsrc,
			Owner:    tx.backend.resources.GetOwner(rsrc),
		})
	}
	return out, nil
}

// FindResource fetches a specific resource by type and name.
func (tx *backendTX) FindResource(ctx context.Context, spec as.FindResourceRequest) (*as.RawResource, error) {
	result, err := tx.searchResourcesByType(ctx, spec.Name, spec.Type, 0)
	if err != nil {
		return nil, err
	}
	switch len(result) {
	case 0:
		return nil, nil
	case 1:
		return result[0], nil
	default:
		return nil, errors.New("too many results")
	}
}

// SearchResource returns all the resources matching the pattern.
func (tx *backendTX) SearchResource(ctx context.Context, pattern string, limit int) ([]*as.RawResource, error) {
	// Aggregate results for all known resource types.
	var out []*as.RawResource
	for _, typ := range tx.backend.resources.types {
		r, err := tx.searchResourcesByType(ctx, pattern, typ, limit)
		if err != nil {
			return nil, err
		}
		out = append(out, r...)
	}
	return out, nil
}

// GetResource returns a ResourceWrapper, as part of a read-modify-update transaction.
func (tx *backendTX) GetResource(ctx context.Context, rsrcID as.ResourceID) (*as.RawResource, error) {
	// From the resource ID we can obtain the DN, and fetch it
	// straight from LDAP without even doing a real search.
	dn, err := tx.backend.resources.GetDN(rsrcID)
	if err != nil {
		return nil, err
	}

	// This is just a 'point' search.
	req := ldap.NewSearchRequest(
		dn,
		ldap.ScopeBaseObject,
		ldap.NeverDerefAliases,
		0,
		0,
		false,
		"(objectClass=*)",
		nil,
		nil,
	)

	result, err := tx.search(ctx, req)
	if err != nil {
		if ldap.IsErrorWithCode(err, ldap.LDAPResultNoSuchObject) {
			return nil, nil
		}
		return nil, err
	}

	rsrc, err := tx.backend.resources.FromLDAP(result.Entries[0])
	if err != nil {
		return nil, err
	}

	return &as.RawResource{
		Resource: *rsrc,
		Owner:    tx.backend.resources.GetOwner(rsrc),
	}, nil
}

// Create a single resource, modify its ID in-place. Ignores previous ID value.
func (tx *backendTX) createSingleResource(user *as.User, rsrc *as.Resource) error {
	dn, err := tx.backend.resources.MakeDN(user, rsrc)
	if err != nil {
		return err
	}

	// Here we assign resource IDs.
	rsrc.ID = as.ResourceID(dn)

	// Now write the resource data to LDAP.
	tx.create(dn)
	for _, attr := range tx.backend.resources.ToLDAP(rsrc) {
		tx.setAttr(dn, attr.Type, attr.Vals...)
	}

	return nil
}

// Sort the resource tree (defined by parent_id relations) breadth-first.
// Horrible algorithm: inefficient on large lists, never terminates on loops.
func sortResourcesByDepth(rsrcs []*as.Resource) []*as.Resource {
	var out []*as.Resource
	seen := make(map[string]struct{})
	stack := []as.ResourceID{as.ResourceID("")}
	for len(stack) > 0 {
		cur := stack[0]
		stack = stack[1:]
		var left []*as.Resource
		for _, r := range rsrcs {
			if r.ParentID.Equal(cur) {
				out = append(out, r)
				seen[string(r.ID)] = struct{}{}
				stack = append(stack, r.ID)
			} else {
				left = append(left, r)
			}
		}
		rsrcs = left
	}
	// See if there are any resources left in rsrc that haven't
	// been copied to the result slice. These will have external
	// ParentIDs.
	for _, r := range rsrcs {
		if _, ok := seen[string(r.ID)]; !ok {
			out = append(out, r)
		}
	}
	return out
}

// CreateResources creates new LDAP-backed resource objects. It
// modifies the input resource objects in-place and assigns them a
// ResourceID. Input resource IDs are ignored (they can be empty),
// with one exception: in order to resolve ParentIDs properly,
// resource IDs can be set to user-selected values and then referenced
// in another resource's ParentID. In this case, the resulting
// ParentID will be then set to the real resource ID of the parent
// resource.
func (tx *backendTX) CreateResources(ctx context.Context, user *as.User, rsrcs []*as.Resource) ([]*as.Resource, error) {
	idMap := make(map[as.ResourceID]as.ResourceID)
	for _, rsrc := range sortResourcesByDepth(rsrcs) {
		oldID := rsrc.ID
		if !rsrc.ParentID.Empty() {
			// Attempt to resolve "local" parentID reference.
			if pid, ok := idMap[rsrc.ParentID]; ok {
				rsrc.ParentID = pid
			}
		}
		if err := tx.createSingleResource(user, rsrc); err != nil {
			return nil, err
		}
		if !oldID.Empty() {
			idMap[oldID] = rsrc.ID
		}
	}
	return rsrcs, nil
}

// UpdateResource updates a LDAP-backed resource that was obtained by a previous GetResource call.
func (tx *backendTX) UpdateResource(ctx context.Context, r *as.Resource) error {
	dn, err := tx.backend.resources.GetDN(r.ID)
	if err != nil {
		return err
	}

	// We can simply dump all attribute/value pairs and let the
	// code in ldapTX do the work of finding the differences.
	for _, attr := range tx.backend.resources.ToLDAP(r) {
		tx.setAttr(dn, attr.Type, attr.Vals...)
	}

	return nil
}

// NextUID returns an available user ID (uidNumber). It does so
// without keeping state by just guessing random UIDs in the
// minUID-maxUID range, and checking if they are available (so it's
// best to keep the range largely underutilized).
func (tx *backendTX) NextUID(ctx context.Context) (uid int, err error) {
	// Won't actually run forever, at some point the context will expire.
	var ok bool
	for !ok && err == nil {
		uid = tx.backend.minUID + rand.Intn(tx.backend.maxUID-tx.backend.minUID)
		ok, err = tx.isUIDAvailable(ctx, uid)
	}
	return
}

func (tx *backendTX) isUIDAvailable(ctx context.Context, uid int) (bool, error) {
	// Try to make this query lightweight: ask for no attributes,
	// use a size limit of 1 and treat "Size Limit Exceeded"
	// errors as a successful result.
	result, err := tx.search(ctx, ldap.NewSearchRequest(
		tx.backend.baseDN,
		ldap.ScopeWholeSubtree,
		ldap.NeverDerefAliases,
		1, // just one result is enough
		0,
		false,
		fmt.Sprintf("(uidNumber=%d)", uid),
		[]string{"dn"},
		nil,
	))
	if err != nil {
		if ldap.IsErrorWithCode(err, ldap.LDAPResultSizeLimitExceeded) {
			return false, nil
		}
		return false, err
	}
	if len(result.Entries) > 0 {
		return false, nil
	}
	return true, nil
}

func (tx *backendTX) CanAccessResource(_ context.Context, username string, rsrc *as.Resource) bool {
	switch rsrc.Type {
	case as.ResourceTypeMailingList:
		for _, a := range rsrc.List.Admins {
			if a == username {
				return true
			}
		}
		return false
	default:
		dn, err := tx.backend.resources.GetDN(rsrc.ID)
		if err != nil {
			return false
		}
		owner := ownerFromDN(dn)
		return owner == username
	}
}
