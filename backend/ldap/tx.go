package ldapbackend

import (
	"context"
	"log"
	"strings"

	"github.com/go-ldap/ldap/v3"
)

// Generic interface to LDAP - allows us to stub out the LDAP client while
// testing.
type ldapConn interface {
	Search(context.Context, *ldap.SearchRequest) (*ldap.SearchResult, error)
	Add(context.Context, *ldap.AddRequest) error
	Modify(context.Context, *ldap.ModifyRequest) error
	Close()
}

type ldapAttr struct {
	dn, attr string
	values   []string
}

// An LDAP "transaction" is really just a buffer of attribute changes,
// that are all executed at once at Commit() time.
//
// Unfortunately, in order to issue LDAP Modify requests properly
// (which have separate Add and Replace options, for one), we need to
// keep state about the observed data, so we cache the results of all
// Search operations and compare those with the new data at commit
// time. If you attempt to modify an object that you haven't Searched
// for previously in the same transaction, you're most likely to get a
// LDAP error in return. Which is fine because all our workflows are
// read/modify/update ones anyway.
//
// Since ordering of Modify requests is important in LDAP, this object
// will preserve the ordering of DNs and attributes when calling
// Commit().
//
type ldapTX struct {
	conn ldapConn

	// Read cache, containing data from the db. Only used to
	// figure out if we need to issue a ModifyRequest or an
	// AddRequest at commit time.
	rcache map[string][]string

	// Write cache, used to store modified attributes. Allows the
	// transaction to be self-consistent: you can read the data
	// that you've just written even before calling Commit().
	wcache map[string][]string

	// List of the new DNs that need to be created.
	// nolint: it's plural DN, not DNS.
	newDNs map[string]struct{}

	changes []*ldapAttr
}

func newLDAPTX(conn ldapConn) *ldapTX {
	return &ldapTX{
		conn:   conn,
		rcache: make(map[string][]string),
		wcache: make(map[string][]string),
		newDNs: make(map[string]struct{}),
	}
}

func cacheKey(dn, attr string) string {
	return strings.Join([]string{dn, attr}, ";")
}

// Search wrapper that fills the cache.
func (tx *ldapTX) search(ctx context.Context, req *ldap.SearchRequest) (*ldap.SearchResult, error) {
	res, err := tx.conn.Search(ctx, req)
	if err != nil {
		return nil, err
	}

	for _, entry := range res.Entries {
		for _, attr := range entry.Attributes {
			tx.rcache[cacheKey(entry.DN, attr.Name)] = attr.Values
		}
	}

	return res, nil
}

// Announce the intention to create a new object. To be called before
// setAttr() on the new DN.
func (tx *ldapTX) create(dn string) {
	tx.newDNs[dn] = struct{}{}
}

// setAttr modifies a single attribute of an object. To delete an
// attribute, pass an empty list of values.
func (tx *ldapTX) setAttr(dn, attr string, values ...string) {
	if dn == "" {
		panic("empty dn in setAttr!")
	}

	// Set the value in the transaction write cache.
	tx.wcache[cacheKey(dn, attr)] = values

	// Reuse previous change, if any. Prevents value duplication
	// in the same ModifyRequest.
	found := false
	for _, chg := range tx.changes {
		if chg.dn == dn && chg.attr == attr {
			chg.values = values
			found = true
			break
		}
	}
	if !found {
		tx.changes = append(tx.changes, &ldapAttr{dn: dn, attr: attr, values: values})
	}
}

// Commit the transaction, sending all changes to the LDAP server.
func (tx *ldapTX) Commit(ctx context.Context) error {
	// Iterate through the changes, and generate ModifyRequest
	// objects grouped by DN (while preserving the order of DNs).
	adds, mods, dns := tx.aggregateChanges(ctx)

	// Now issue all Modify or Add requests, one by one, in the
	// same order as we have seen them. Abort on the first error.
	for _, dn := range dns {
		var err error
		if ar, ok := adds[dn]; ok {
			if isEmptyAddRequest(ar) {
				continue
			}
			log.Printf("issuing AddRequest: %+v", ar)
			err = tx.conn.Add(ctx, ar)
		} else {
			mr := mods[dn]
			if isEmptyModifyRequest(mr) {
				continue
			}
			log.Printf("issuing ModifyRequest: %+v", mr)
			err = tx.conn.Modify(ctx, mr)
		}
		if err != nil {
			return err
		}
	}

	// Cleanup
	tx.changes = nil
	tx.newDNs = make(map[string]struct{})
	tx.wcache = make(map[string][]string)

	return nil
}

// Helper for Commit that aggregates changes into add and modify lists.
func (tx *ldapTX) aggregateChanges(ctx context.Context) (map[string]*ldap.AddRequest, map[string]*ldap.ModifyRequest, []string) {
	var dns []string
	mods := make(map[string]*ldap.ModifyRequest)
	adds := make(map[string]*ldap.AddRequest)
	for _, c := range tx.changes {
		if _, isNew := tx.newDNs[c.dn]; isNew {
			ar, ok := adds[c.dn]
			if !ok {
				ar = ldap.NewAddRequest(c.dn, nil)
				adds[c.dn] = ar
				dns = append(dns, c.dn)
			}
			if len(c.values) > 0 {
				ar.Attribute(c.attr, c.values)
			}
		} else {
			mr, ok := mods[c.dn]
			if !ok {
				mr = ldap.NewModifyRequest(c.dn, nil)
				mods[c.dn] = mr
				dns = append(dns, c.dn)
			}
			tx.updateModifyRequest(ctx, mr, c)
		}
	}
	return adds, mods, dns
}

func (tx *ldapTX) updateModifyRequest(ctx context.Context, mr *ldap.ModifyRequest, attr *ldapAttr) {
	// Pessimistic approach: if we haven't seen this attribute
	// before, try to fetch it from LDAP so we know if we need to
	// perform an Add or a Replace.
	old, ok := tx.rcache[cacheKey(attr.dn, attr.attr)]
	if !ok {
		log.Printf("tx: pessimistic fallback for %s %s", attr.dn, attr.attr)
		oldFromLDAP := tx.readAttributeValuesNoCache(ctx, attr.dn, attr.attr)
		if len(oldFromLDAP) > 0 {
			ok = true
			old = oldFromLDAP
		}
	}

	switch {
	case ok && !stringListEquals(old, attr.values):
		mr.Replace(attr.attr, attr.values)
	case ok && attr.values == nil:
		mr.Delete(attr.attr, old)
	case !ok && len(attr.values) > 0:
		mr.Add(attr.attr, attr.values)
	}
}

func (tx *ldapTX) readAttributeValuesNoCache(ctx context.Context, dn, attr string) []string {
	result, err := tx.search(ctx, ldap.NewSearchRequest(
		dn,
		ldap.ScopeBaseObject,
		ldap.NeverDerefAliases,
		0,
		0,
		false,
		"(objectClass=*)",
		[]string{attr},
		nil,
	))
	if err == nil && len(result.Entries) > 0 {
		return result.Entries[0].GetAttributeValues(attr)
	}
	return nil
}

func (tx *ldapTX) readAttributeValues(ctx context.Context, dn, attr string) []string {
	if values, ok := tx.wcache[cacheKey(dn, attr)]; ok {
		return values
	}
	return tx.readAttributeValuesNoCache(ctx, dn, attr)
}

func isEmptyModifyRequest(mr *ldap.ModifyRequest) bool {
	return len(mr.Changes) == 0
}

func isEmptyAddRequest(ar *ldap.AddRequest) bool {
	return len(ar.Attributes) == 0
}

// Unordered list comparison.
func stringListEquals(a, b []string) bool {
	if len(a) != len(b) {
		return false
	}
	tmp := make(map[string]struct{})
	for _, aa := range a {
		tmp[aa] = struct{}{}
	}
	for _, bb := range b {
		if _, ok := tmp[bb]; !ok {
			return false
		}
	}
	return true
}
