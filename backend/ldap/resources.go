package ldapbackend

import (
	"errors"
	"fmt"
	"strconv"
	"strings"

	"github.com/go-ldap/ldap/v3"

	as "git.autistici.org/ai3/accountserver"
)

// Generic resource handler interface. One for each resource type,
// mapping to exactly one LDAP object type.
type resourceHandler interface {
	MakeDN(*as.User, *as.Resource) (string, error)
	GetOwner(*as.Resource) string
	ToLDAP(*as.Resource) []ldap.PartialAttribute
	FromLDAP(*ldap.Entry) (*as.Resource, error)
	SearchQuery() *queryTemplate
}

// Registry for demultiplexing resource handling. Has a similar
// interface to a resourceHandler, with a few exceptions.
type resourceRegistry struct {
	handlers map[string]resourceHandler
	types    []string
}

func newResourceRegistry() *resourceRegistry {
	return &resourceRegistry{
		handlers: make(map[string]resourceHandler),
	}
}

func (reg *resourceRegistry) register(rtype string, h resourceHandler) {
	reg.handlers[rtype] = h
	reg.types = append(reg.types, rtype)
}

func (reg *resourceRegistry) dispatch(rsrcType string, f func(resourceHandler) error) error {
	h, ok := reg.handlers[rsrcType]
	if !ok {
		return errors.New("unknown resource type")
	}
	return f(h)
}

func (reg *resourceRegistry) GetDN(rid as.ResourceID) (string, error) {
	return string(rid), nil
}

func (reg *resourceRegistry) GetOwner(rsrc *as.Resource) (username string) {
	// Error results in no owner.
	// nolint
	reg.dispatch(rsrc.Type, func(h resourceHandler) error {
		username = h.GetOwner(rsrc)
		return nil
	})
	return
}

func (reg *resourceRegistry) MakeDN(user *as.User, rsrc *as.Resource) (dn string, err error) {
	err = reg.dispatch(rsrc.Type, func(h resourceHandler) (dnerr error) {
		dn, dnerr = h.MakeDN(user, rsrc)
		return
	})
	return
}

func (reg *resourceRegistry) ToLDAP(rsrc *as.Resource) (attrs []ldap.PartialAttribute) {
	if err := reg.dispatch(rsrc.Type, func(h resourceHandler) error {
		attrs = h.ToLDAP(rsrc)
		return nil
	}); err != nil {
		return nil
	}

	if rsrc.CreatedAt != "" {
		attrs = append(attrs, ldap.PartialAttribute{
			Type: creationDateLDAPAttr, Vals: s2l(rsrc.CreatedAt)})
	}

	attrs = append(attrs, []ldap.PartialAttribute{
		{Type: "status", Vals: s2l(rsrc.Status)},
		{Type: "host", Vals: s2l(rsrc.Shard)},
		{Type: "originalHost", Vals: s2l(rsrc.OriginalShard)},
	}...)
	return
}

func setCommonResourceAttrs(entry *ldap.Entry, rsrc *as.Resource) {
	rsrc.Status = entry.GetAttributeValue("status")
	rsrc.Shard = entry.GetAttributeValue("host")
	rsrc.OriginalShard = entry.GetAttributeValue("originalHost")
	rsrc.CreatedAt = entry.GetAttributeValue(creationDateLDAPAttr)
}

func (reg *resourceRegistry) FromLDAP(entry *ldap.Entry) (rsrc *as.Resource, err error) {
	// Since we don't know what resource type to expect, we try
	// all known handlers until one returns a valid Resource.
	// This is slightly dangerous unless all
	for _, h := range reg.handlers {
		rsrc, err = h.FromLDAP(entry)
		if err == nil {
			setCommonResourceAttrs(entry, rsrc)
			return
		}
	}
	return nil, errors.New("unknown resource")
}

func (reg *resourceRegistry) SearchQuery(rsrcType string) (q *queryTemplate, err error) {
	err = reg.dispatch(rsrcType, func(h resourceHandler) error {
		q = h.SearchQuery()
		return nil
	})
	return
}

// Email resource.
type emailResourceHandler struct {
	baseDN string
}

var (
	errWrongObjectClass = errors.New("objectClass does not match")
	errWrongSubtree     = errors.New("subtree does not match")
)

func (h *emailResourceHandler) MakeDN(user *as.User, rsrc *as.Resource) (string, error) {
	if user == nil {
		return "", errors.New("email resource requires an owner")
	}
	rdn := replaceVars("mail=${resource}", templateVars{"resource": rsrc.Name})
	return joinDN(rdn, getUserDN(user, h.baseDN)), nil
}

func (h *emailResourceHandler) GetOwner(rsrc *as.Resource) string {
	return ownerFromDN(rsrc.ID.String())
}

func (h *emailResourceHandler) FromLDAP(entry *ldap.Entry) (*as.Resource, error) {
	if !isObjectClass(entry, "virtualMailUser") {
		return nil, errWrongObjectClass
	}

	email := entry.GetAttributeValue("mail")

	return &as.Resource{
		ID:   as.ResourceID(entry.DN),
		Type: as.ResourceTypeEmail,
		Name: email,
		Email: &as.Email{
			Aliases: entry.GetAttributeValues("mailAlternateAddress"),
			Maildir: entry.GetAttributeValue("mailMessageStore"),
		},
	}, nil
}

func (h *emailResourceHandler) ToLDAP(rsrc *as.Resource) []ldap.PartialAttribute {
	return []ldap.PartialAttribute{
		{Type: "objectClass", Vals: []string{"top", "virtualMailUser"}},
		{Type: "mail", Vals: s2l(rsrc.Name)},
		{Type: "mailAlternateAddress", Vals: rsrc.Email.Aliases},
		{Type: "mailMessageStore", Vals: s2l(rsrc.Email.Maildir)},
	}
}

func (h *emailResourceHandler) SearchQuery() *queryTemplate {
	return &queryTemplate{
		Base:   joinDN("ou=People", h.baseDN),
		Filter: "(&(objectClass=virtualMailUser)(|(mail=${resource})(mailAlternateAddress=${resource})))",
		Scope:  ldap.ScopeWholeSubtree,
	}
}

// Mailing list resource. They are stored on a separate LDAP subtree.
type mailingListResourceHandler struct {
	baseDN string

	// Keep around a parsed DN for the ou=Lists root, so we can
	// easily call DN.AncestorOf in FromLDAP later.
	listsDN *ldap.DN
}

func newMailingListResourceHandler(baseDN string) *mailingListResourceHandler {
	dn, err := ldap.ParseDN(joinDN("ou=Lists", baseDN))
	if err != nil {
		panic(err)
	}
	return &mailingListResourceHandler{
		baseDN:  baseDN,
		listsDN: dn,
	}
}

func (h *mailingListResourceHandler) MakeDN(_ *as.User, rsrc *as.Resource) (string, error) {
	rdn := replaceVars("listName=${resource}", templateVars{"resource": rsrc.Name})
	return joinDN(rdn, "ou=Lists", h.baseDN), nil
}

func (h *mailingListResourceHandler) GetOwner(rsrc *as.Resource) string {
	// No exclusive owners for mailing lists.
	return ""
}

func (h *mailingListResourceHandler) FromLDAP(entry *ldap.Entry) (*as.Resource, error) {
	// Match on objectClass and subtree (the mailingList
	// objectClass is also used by newsletters).
	if !isObjectClass(entry, "mailingList") {
		return nil, errWrongObjectClass
	}
	dn, err := ldap.ParseDN(entry.DN)
	if err != nil {
		panic(err)
	}
	if !h.listsDN.AncestorOf(dn) {
		return nil, errWrongSubtree
	}

	listName := entry.GetAttributeValue("listName")
	return &as.Resource{
		ID:   as.ResourceID(entry.DN),
		Type: as.ResourceTypeMailingList,
		Name: listName,
		List: &as.MailingList{
			Public: s2b(entry.GetAttributeValue("public")),
			Admins: entry.GetAttributeValues("listOwner"),
		},
	}, nil
}

func (h *mailingListResourceHandler) ToLDAP(rsrc *as.Resource) []ldap.PartialAttribute {
	return []ldap.PartialAttribute{
		{Type: "objectClass", Vals: []string{"top", "mailingList"}},
		{Type: "listName", Vals: s2l(rsrc.Name)},
		{Type: "public", Vals: s2l(b2s(rsrc.List.Public))},
		{Type: "listOwner", Vals: rsrc.List.Admins},
	}
}

func (h *mailingListResourceHandler) SearchQuery() *queryTemplate {
	return &queryTemplate{
		Base:   joinDN("ou=Lists", h.baseDN),
		Filter: "(&(objectClass=mailingList)(listName=${resource}))",
		Scope:  ldap.ScopeSingleLevel,
	}
}

// Newsletter resource. Like mailing lists, these are stored on their
// own separate subtree.
type newsletterResourceHandler struct {
	baseDN string

	// Keep around a parsed DN for the ou=Newsletters root, so we can
	// easily call DN.AncestorOf in FromLDAP later.
	newslettersDN *ldap.DN
}

func newNewsletterResourceHandler(baseDN string) *newsletterResourceHandler {
	dn, err := ldap.ParseDN(joinDN("ou=Newsletters", baseDN))
	if err != nil {
		panic(err)
	}
	return &newsletterResourceHandler{
		baseDN:        baseDN,
		newslettersDN: dn,
	}
}

func (h *newsletterResourceHandler) MakeDN(_ *as.User, rsrc *as.Resource) (string, error) {
	rdn := replaceVars("listName=${resource}", templateVars{"resource": rsrc.Name})
	return joinDN(rdn, "ou=Newsletters", h.baseDN), nil
}

func (h *newsletterResourceHandler) GetOwner(rsrc *as.Resource) string {
	// No exclusive owners for newsletters.
	return ""
}

func (h *newsletterResourceHandler) FromLDAP(entry *ldap.Entry) (*as.Resource, error) {
	// Match on objectClass and subtree.
	if !isObjectClass(entry, "mailingList") {
		return nil, errWrongObjectClass
	}
	dn, err := ldap.ParseDN(entry.DN)
	if err != nil {
		panic(err)
	}
	if !h.newslettersDN.AncestorOf(dn) {
		return nil, errWrongSubtree
	}

	listName := entry.GetAttributeValue("listName")
	return &as.Resource{
		ID:   as.ResourceID(entry.DN),
		Type: as.ResourceTypeNewsletter,
		Name: listName,
		Newsletter: &as.Newsletter{
			Admins: entry.GetAttributeValues("listOwner"),
		},
	}, nil
}

func (h *newsletterResourceHandler) ToLDAP(rsrc *as.Resource) []ldap.PartialAttribute {
	return []ldap.PartialAttribute{
		{Type: "objectClass", Vals: []string{"top", "mailingList"}},
		{Type: "listName", Vals: s2l(rsrc.Name)},
		// TODO: check if public is actually mandated by the schema.
		{Type: "public", Vals: []string{"yes"}},
		{Type: "listOwner", Vals: rsrc.Newsletter.Admins},
	}
}

func (h *newsletterResourceHandler) SearchQuery() *queryTemplate {
	return &queryTemplate{
		Base:   joinDN("ou=Newsletters", h.baseDN),
		Filter: "(&(objectClass=mailingList)(listName=${resource}))",
		Scope:  ldap.ScopeSingleLevel,
	}
}

// Website (subsite) resource.
type websiteResourceHandler struct {
	baseDN string
}

func (h *websiteResourceHandler) MakeDN(user *as.User, rsrc *as.Resource) (string, error) {
	if user == nil {
		return "", errors.New("website resource requires an owner")
	}
	rdn := replaceVars("alias=${resource}", templateVars{"resource": rsrc.Name})
	return joinDN(rdn, getUserDN(user, h.baseDN)), nil
}

func (h *websiteResourceHandler) GetOwner(rsrc *as.Resource) string {
	return ownerFromDN(rsrc.ID.String())
}

func (h *websiteResourceHandler) FromLDAP(entry *ldap.Entry) (*as.Resource, error) {
	if !isObjectClass(entry, "subSite") {
		return nil, errWrongObjectClass
	}

	alias := entry.GetAttributeValue("alias")
	parentSite := entry.GetAttributeValue("parentSite")
	//name := fmt.Sprintf("%s/%s", parentSite, alias)
	url := fmt.Sprintf("https://www.%s/%s/", parentSite, alias)
	uid, _ := strconv.Atoi(entry.GetAttributeValue(uidNumberLDAPAttr)) // nolint
	statsID, _ := strconv.Atoi(entry.GetAttributeValue("statsId"))     // nolint

	return &as.Resource{
		ID:   as.ResourceID(entry.DN),
		Type: as.ResourceTypeWebsite,
		Name: alias,
		Website: &as.Website{
			URL:          url,
			UID:          uid,
			ParentDomain: parentSite,
			Options:      entry.GetAttributeValues("option"),
			DocumentRoot: entry.GetAttributeValue("documentRoot"),
			AcceptMail:   false,
			StatsID:      statsID,
		},
	}, nil
}

func nonZeroIntToList(i int) []string {
	if i == 0 {
		return nil
	}
	return []string{strconv.Itoa(i)}
}

func (h *websiteResourceHandler) ToLDAP(rsrc *as.Resource) []ldap.PartialAttribute {
	//bareName := strings.Split(rsrc.Name, "/")[0]
	return []ldap.PartialAttribute{
		{Type: "objectClass", Vals: []string{"top", "subSite"}},
		{Type: "alias", Vals: s2l(rsrc.Name)},
		{Type: "parentSite", Vals: s2l(rsrc.Website.ParentDomain)},
		{Type: "option", Vals: rsrc.Website.Options},
		{Type: "documentRoot", Vals: s2l(rsrc.Website.DocumentRoot)},
		{Type: uidNumberLDAPAttr, Vals: s2l(strconv.Itoa(rsrc.Website.UID))},
		{Type: "statsId", Vals: nonZeroIntToList(rsrc.Website.StatsID)},
	}
}

func (h *websiteResourceHandler) SearchQuery() *queryTemplate {
	return &queryTemplate{
		Base:   joinDN("ou=People", h.baseDN),
		Filter: "(&(objectClass=subSite)(alias=${resource}))",
		Scope:  ldap.ScopeWholeSubtree,
	}
}

// Domain (virtual host) resource.
type domainResourceHandler struct {
	baseDN string
}

func (h *domainResourceHandler) MakeDN(user *as.User, rsrc *as.Resource) (string, error) {
	if user == nil {
		return "", errors.New("domain resource requires an owner")
	}
	rdn := replaceVars("cn=${resource}", templateVars{"resource": rsrc.Name})
	return joinDN(rdn, getUserDN(user, h.baseDN)), nil
}

func (h *domainResourceHandler) GetOwner(rsrc *as.Resource) string {
	return ownerFromDN(rsrc.ID.String())
}

func (h *domainResourceHandler) FromLDAP(entry *ldap.Entry) (*as.Resource, error) {
	if !isObjectClass(entry, "virtualHost") {
		return nil, errWrongObjectClass
	}

	cn := entry.GetAttributeValue("cn")
	uid, _ := strconv.Atoi(entry.GetAttributeValue(uidNumberLDAPAttr)) // nolint
	statsID, _ := strconv.Atoi(entry.GetAttributeValue("statsId"))     // nolint

	return &as.Resource{
		ID:   as.ResourceID(entry.DN),
		Type: as.ResourceTypeDomain,
		Name: cn,
		Website: &as.Website{
			URL:          fmt.Sprintf("https://%s/", cn),
			UID:          uid,
			Options:      entry.GetAttributeValues("option"),
			DocumentRoot: entry.GetAttributeValue("documentRoot"),
			AcceptMail:   s2b(entry.GetAttributeValue("acceptMail")),
			StatsID:      statsID,
		},
	}, nil
}

func (h *domainResourceHandler) ToLDAP(rsrc *as.Resource) []ldap.PartialAttribute {
	return []ldap.PartialAttribute{
		{Type: "objectClass", Vals: []string{"top", "virtualHost"}},
		{Type: "cn", Vals: s2l(rsrc.Name)},
		{Type: "option", Vals: rsrc.Website.Options},
		{Type: "documentRoot", Vals: s2l(rsrc.Website.DocumentRoot)},
		{Type: "acceptMail", Vals: s2l(b2s(rsrc.Website.AcceptMail))},
		{Type: uidNumberLDAPAttr, Vals: s2l(strconv.Itoa(rsrc.Website.UID))},
		{Type: "statsId", Vals: nonZeroIntToList(rsrc.Website.StatsID)},
	}
}

func (h *domainResourceHandler) SearchQuery() *queryTemplate {
	return &queryTemplate{
		Base:   joinDN("ou=People", h.baseDN),
		Filter: "(&(objectClass=virtualHost)(cn=${resource}))",
		Scope:  ldap.ScopeWholeSubtree,
	}
}

// WebDAV (a.k.a. "ftp account") resource.
type webdavResourceHandler struct {
	baseDN string
}

func (h *webdavResourceHandler) MakeDN(user *as.User, rsrc *as.Resource) (string, error) {
	if user == nil {
		return "", errors.New("DAV resource requires an owner")
	}
	rdn := replaceVars("ftpname=${resource}", templateVars{"resource": rsrc.Name})
	return joinDN(rdn, getUserDN(user, h.baseDN)), nil
}

func (h *webdavResourceHandler) GetOwner(rsrc *as.Resource) string {
	return ownerFromDN(rsrc.ID.String())
}

func (h *webdavResourceHandler) FromLDAP(entry *ldap.Entry) (*as.Resource, error) {
	if !isObjectClass(entry, "ftpAccount") {
		return nil, errWrongObjectClass
	}

	name := entry.GetAttributeValue("ftpname")
	uid, _ := strconv.Atoi(entry.GetAttributeValue(uidNumberLDAPAttr)) // nolint
	return &as.Resource{
		ID:   as.ResourceID(entry.DN),
		Type: as.ResourceTypeDAV,
		Name: name,
		DAV: &as.WebDAV{
			Homedir: entry.GetAttributeValue("homeDirectory"),
			UID:     uid,
		},
	}, nil
}

func (h *webdavResourceHandler) ToLDAP(rsrc *as.Resource) []ldap.PartialAttribute {
	return []ldap.PartialAttribute{
		// Note: there is really no need to have inetOrgPerson here, but
		// we have to keep it for compatibility with our legacy schema.
		{Type: "objectClass", Vals: []string{"top", "inetOrgPerson", "posixAccount", "shadowAccount", "ftpAccount"}},
		{Type: "ftpname", Vals: s2l(rsrc.Name)},
		{Type: "homeDirectory", Vals: s2l(rsrc.DAV.Homedir)},
		{Type: uidNumberLDAPAttr, Vals: s2l(strconv.Itoa(rsrc.DAV.UID))},
		{Type: gidNumberLDAPAttr, Vals: s2l("2000")},
		{Type: "uid", Vals: s2l(rsrc.Name)},
		{Type: "cn", Vals: s2l(rsrc.Name)},
		{Type: "sn", Vals: s2l("Private")},
	}
}

func (h *webdavResourceHandler) SearchQuery() *queryTemplate {
	return &queryTemplate{
		Base:   joinDN("ou=People", h.baseDN),
		Filter: "(&(objectClass=ftpAccount)(ftpname=${resource}))",
		Scope:  ldap.ScopeWholeSubtree,
	}
}

// Databases are special: in LDAP, they encode their relation with a
// website using the database hierarchy. This means that, in order to
// satisfy the requirement to generate a DN directly from every
// resource ID, we need to encode the parent website in the resource
// ID itself. We do this using not just the website name (which would
// be ambiguous: Website or Domain?), but also the type, using an
// attr=name syntax.
type databaseResourceHandler struct {
	baseDN string
}

func (h *databaseResourceHandler) MakeDN(user *as.User, rsrc *as.Resource) (string, error) {
	rdn := replaceVars("dbname=${resource}", templateVars{"resource": rsrc.Name})
	return joinDN(rdn, string(rsrc.ParentID)), nil
}

func (h *databaseResourceHandler) GetOwner(rsrc *as.Resource) string {
	return ownerFromDN(rsrc.ID.String())
}

func (h *databaseResourceHandler) FromLDAP(entry *ldap.Entry) (*as.Resource, error) {
	if !isObjectClass(entry, "dbMysql") {
		return nil, errWrongObjectClass
	}

	name := entry.GetAttributeValue("dbname")

	// Find the parent DN.
	var parentDN string
	if n := strings.IndexByte(entry.DN, ','); n > 0 {
		parentDN = entry.DN[n+1:]
	}
	if parentDN == "" {
		return nil, errors.New("can't determine parent DN")
	}
	return &as.Resource{
		ID:       as.ResourceID(entry.DN),
		Type:     as.ResourceTypeDatabase,
		ParentID: as.ResourceID(parentDN),
		Name:     name,
		Database: &as.Database{
			DBUser: entry.GetAttributeValue("dbuser"),
		},
	}, nil
}

func (h *databaseResourceHandler) ToLDAP(rsrc *as.Resource) []ldap.PartialAttribute {
	return []ldap.PartialAttribute{
		{Type: "objectClass", Vals: []string{"top", "dbMysql"}},
		{Type: "dbname", Vals: s2l(rsrc.Name)},
		{Type: "dbuser", Vals: s2l(rsrc.Database.DBUser)},
	}
}

func (h *databaseResourceHandler) SearchQuery() *queryTemplate {
	return &queryTemplate{
		Base:   joinDN("ou=People", h.baseDN),
		Filter: "(&(objectClass=dbMysql)(dbname=${resource}))",
		Scope:  ldap.ScopeWholeSubtree,
	}
}

func newDefaultResourceRegistry(baseDN string) *resourceRegistry {
	reg := newResourceRegistry()
	reg.register(as.ResourceTypeEmail, &emailResourceHandler{baseDN: baseDN})
	reg.register(as.ResourceTypeMailingList, newMailingListResourceHandler(baseDN))
	reg.register(as.ResourceTypeNewsletter, newNewsletterResourceHandler(baseDN))
	reg.register(as.ResourceTypeDAV, &webdavResourceHandler{baseDN: baseDN})
	reg.register(as.ResourceTypeWebsite, &websiteResourceHandler{baseDN: baseDN})
	reg.register(as.ResourceTypeDomain, &domainResourceHandler{baseDN: baseDN})
	reg.register(as.ResourceTypeDatabase, &databaseResourceHandler{baseDN: baseDN})
	return reg
}
