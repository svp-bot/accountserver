package ldapbackend

import (
	"testing"

	"github.com/go-ldap/ldap/v3"
	"github.com/go-test/deep"

	as "git.autistici.org/ai3/accountserver"
)

func TestEmailResource_FromLDAP(t *testing.T) {
	entry := ldap.NewEntry(
		"mail=test@investici.org,uid=test@investici.org,ou=People,"+testBaseDN,
		map[string][]string{
			"objectClass":          []string{"top", "virtualMailUser"},
			"mail":                 []string{"test@investici.org"},
			"status":               []string{"active"},
			"host":                 []string{"host1"},
			"originalHost":         []string{"host1"},
			"mailAlternateAddress": []string{"test2@investici.org", "test3@investici.org"},
			"mailMessageStore":     []string{"test/store"},
		},
	)

	reg := newResourceRegistry()
	reg.register(as.ResourceTypeEmail, &emailResourceHandler{baseDN: testBaseDN})

	r, err := reg.FromLDAP(entry)
	if err != nil {
		t.Fatal("FromLDAP", err)
	}

	expected := &as.Resource{
		ID:            as.ResourceID("mail=test@investici.org,uid=test@investici.org,ou=People," + testBaseDN),
		Type:          "email",
		Name:          "test@investici.org",
		Status:        "active",
		Shard:         "host1",
		OriginalShard: "host1",
		Email: &as.Email{
			Aliases: []string{"test2@investici.org", "test3@investici.org"},
			Maildir: "test/store",
		},
	}
	if err := deep.Equal(r, expected); err != nil {
		t.Fatalf("bad result: %v", err)
	}
}

func TestResource_LDAPSerialization(t *testing.T) {
	var testResources = []*as.Resource{
		&as.Resource{
			ID:     as.ResourceID("mail=test@investici.org,uid=test@investici.org,ou=People," + testBaseDN),
			Type:   as.ResourceTypeEmail,
			Name:   "test@investici.org",
			Status: as.ResourceStatusActive,
			Email: &as.Email{
				Aliases: []string{"test2@investici.org", "test3@investici.org"},
				Maildir: "test/store",
			},
		},

		&as.Resource{
			ID:     as.ResourceID("cn=site.investici.org,uid=test@investici.org,ou=People," + testBaseDN),
			Type:   as.ResourceTypeDomain,
			Name:   "site.investici.org",
			Status: as.ResourceStatusActive,
			Website: &as.Website{
				URL:          "https://site.investici.org/",
				UID:          123456,
				AcceptMail:   true,
				Options:      []string{"php", "sendmail"},
				DocumentRoot: "/home/web/root",
				StatsID:      12345,
			},
		},

		&as.Resource{
			ID:     as.ResourceID("alias=site,uid=test@investici.org,ou=People," + testBaseDN),
			Type:   as.ResourceTypeWebsite,
			Name:   "site",
			Status: as.ResourceStatusActive,
			Website: &as.Website{
				URL:          "https://www.investici.org/site/",
				UID:          123456,
				ParentDomain: "investici.org",
				Options:      []string{"php", "sendmail"},
				DocumentRoot: "/home/web/root",
			},
		},

		&as.Resource{
			ID:     as.ResourceID("ftpname=test,uid=test@investici.org,ou=People," + testBaseDN),
			Type:   as.ResourceTypeDAV,
			Name:   "test",
			Status: as.ResourceStatusActive,
			DAV: &as.WebDAV{
				UID:     123456,
				Homedir: "/home/web/test",
			},
		},

		&as.Resource{
			ID:       as.ResourceID("dbname=testdb,alias=site,uid=test@investici.org,ou=People," + testBaseDN),
			ParentID: as.ResourceID("alias=site,uid=test@investici.org,ou=People," + testBaseDN),
			Type:     as.ResourceTypeDatabase,
			Name:     "testdb",
			Status:   as.ResourceStatusActive,
			Database: &as.Database{
				DBUser: "testdbuser",
			},
		},

		&as.Resource{
			ID:     as.ResourceID("listName=test@investici.org,ou=Lists," + testBaseDN),
			Type:   as.ResourceTypeMailingList,
			Name:   "test@investici.org",
			Status: as.ResourceStatusActive,
			List: &as.MailingList{
				Admins: []string{"admin@investici.org"},
				Public: true,
			},
		},
	}

	reg := newDefaultResourceRegistry(testBaseDN)

	for _, r := range testResources {
		attrs := reg.ToLDAP(r)
		e := partialAttrsToEntry(r.ID.String(), attrs)
		r2, err := reg.FromLDAP(e)
		if err != nil {
			t.Errorf("error deserializing resource %s: %v", r.String(), err)
			continue
		}
		if diff := deep.Equal(r, r2); diff != nil {
			t.Errorf("differences for resource %s: %v", r.String(), diff)
		}
	}
}

func partialAttrsToEntry(dn string, attrs []ldap.PartialAttribute) *ldap.Entry {
	amap := make(map[string][]string)
	for _, p := range attrs {
		amap[p.Type] = p.Vals
	}
	return ldap.NewEntry(dn, amap)
}
