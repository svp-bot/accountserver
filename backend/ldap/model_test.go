package ldapbackend

import (
	"context"
	"fmt"
	"testing"
	"time"

	"github.com/go-test/deep"

	as "git.autistici.org/ai3/accountserver"
	"git.autistici.org/ai3/accountserver/ldaptest"
	ct "git.autistici.org/ai3/go-common/ldap/compositetypes"
)

const (
	testLDAPPort = 42871
	testLDAPAddr = "ldap://127.0.0.1:42871"
	testUser1    = "uno@investici.org"
	testUser2    = "due@investici.org"     // has encryption keys
	testUser3    = "tre@investici.org"     // has OTP
	testUser4    = "quattro@investici.org" // has mailing lists
	testBaseDN   = "dc=example,dc=com"
)

func startServerAndGetUser(t testing.TB) (func(), as.Backend, *as.RawUser) {
	return startServerAndGetUserWithName(t, testUser1)
}

func startServerAndGetUser2(t testing.TB) (func(), as.Backend, *as.RawUser) {
	return startServerAndGetUserWithName(t, testUser2)
}

func startServerAndGetUser3(t testing.TB) (func(), as.Backend, *as.RawUser) {
	return startServerAndGetUserWithName(t, testUser3)
}

func startServerAndGetUser4(t testing.TB) (func(), as.Backend, *as.RawUser) {
	return startServerAndGetUserWithName(t, testUser4)
}

func startServer(t testing.TB) (func(), as.Backend) {
	stop := ldaptest.StartServer(t, &ldaptest.Config{
		Dir:  "../../ldaptest",
		Port: testLDAPPort,
		Base: "dc=example,dc=com",
		LDIFs: []string{
			"testdata/base.ldif",
			"testdata/test1.ldif",
			"testdata/test2.ldif",
			"testdata/test3.ldif",
			"testdata/test4.ldif",
		},
	})

	b, err := NewLDAPBackend(testLDAPAddr, "cn=manager,dc=example,dc=com", "password", "dc=example,dc=com")
	if err != nil {
		t.Fatal("NewLDAPBackend", err)
	}

	return stop, b
}

func startServerAndGetUserWithName(t testing.TB, username string) (func(), as.Backend, *as.RawUser) {
	stop, b := startServer(t)

	tx, _ := b.NewTransaction()
	user, err := tx.GetUser(context.Background(), username)
	if err != nil {
		t.Fatal("GetUser", err)
	}
	if user == nil {
		t.Fatalf("could not find test user %s", username)
	}

	return stop, b, user
}

func TestModel_GetUser_NotFound(t *testing.T) {
	stop, b := startServer(t)
	defer stop()

	tx, _ := b.NewTransaction()
	user, err := tx.GetUser(context.Background(), "wrong_user")
	if err != nil {
		t.Fatalf("GetUser(wrong_user) should have returned no error, got: %v", err)
	}
	if user != nil {
		t.Fatal("GetUser(wrong_user) returned non-nil user")
	}
}

func TestModel_GetUser(t *testing.T) {
	stop, _, user := startServerAndGetUser(t)
	defer stop()

	if user.Name != testUser1 {
		t.Errorf("bad username: expected %s, got %s", testUser1, user.Name)
	}
	if len(user.Resources) != 5 {
		t.Errorf("expected 5 resources, got %d", len(user.Resources))
	}
	expectedPwChangeStamp := time.Date(2018, 11, 14, 0, 0, 0, 0, time.UTC)
	if user.LastPasswordChangeStamp != expectedPwChangeStamp {
		t.Errorf("bad last password change timestamp: expected %s, got %s", expectedPwChangeStamp, user.LastPasswordChangeStamp)
	}

	// Test a specific resource (the database).
	db := user.GetSingleResourceByType(as.ResourceTypeDatabase)
	expectedDB := &as.Resource{
		ID:            as.ResourceID("dbname=unodb,alias=uno,uid=uno@investici.org,ou=People,dc=example,dc=com"),
		Type:          as.ResourceTypeDatabase,
		ParentID:      as.ResourceID("alias=uno,uid=uno@investici.org,ou=People,dc=example,dc=com"),
		Name:          "unodb",
		Shard:         "host2",
		OriginalShard: "host2",
		Status:        as.ResourceStatusActive,
		CreatedAt:     "01-08-2013",
		Database: &as.Database{
			DBUser: "unodb",
		},
	}
	if err := deep.Equal(db, expectedDB); err != nil {
		t.Fatalf("returned database resource differs: %v", err)
	}
}

func TestModel_GetUser_HasEncryptionKeys(t *testing.T) {
	stop, _, user := startServerAndGetUser2(t)
	defer stop()

	if !user.HasEncryptionKeys {
		t.Errorf("user %s does not appear to have encryption keys", user.Name)
	}
}

func TestModel_GetUser_Has2FA(t *testing.T) {
	stop, _, user := startServerAndGetUser3(t)
	defer stop()

	if !user.Has2FA {
		t.Errorf("user %s does not appear to have 2FA enabled", user.Name)
	}
}

func TestModel_GetUser_Resources(t *testing.T) {
	stop, b, user := startServerAndGetUser(t)
	defer stop()

	// Ensure that the user *has* resources.
	if len(user.Resources) < 1 {
		t.Fatal("test user has no resources!")
	}

	// Fetch individually all user resources, one by one, and
	// check that they match what we have already.
	tx2, _ := b.NewTransaction()
	for _, r := range user.Resources {
		fr, err := tx2.GetResource(context.Background(), r.ID)
		if err != nil {
			t.Errorf("could not fetch resource %s: %v", r.ID, err)
			continue
		}
		if fr == nil {
			t.Errorf("resource %s is missing", r.ID)
			continue
		}
		// It's ok if Group is unset in the GetResource response.
		rr := *r
		rr.Group = ""
		if err := deep.Equal(&fr.Resource, &rr); err != nil {
			t.Errorf("error in fetched resource %s: %v", r.ID, err)
			continue
		}
	}
}

func TestModel_GetUser_MailingListsAndNewsletters(t *testing.T) {
	stop, _, user := startServerAndGetUser4(t)
	defer stop()

	// Ensure that the user has the expected number of list resources.
	// The backend should find two lists, one of which has an alias as the owner.
	lists := user.GetResourcesByType(as.ResourceTypeMailingList)
	if l := len(lists); l != 2 {
		t.Errorf("test user has %d mailing lists, expected %d", l, 2)
	}

	// The test user has 1 newsletter.
	nls := user.GetResourcesByType(as.ResourceTypeNewsletter)
	if l := len(nls); l != 1 {
		t.Errorf("test user has %d newsletters, expected %d", l, 1)
	}
}

func TestModel_SearchUser(t *testing.T) {
	stop, b := startServer(t)
	defer stop()
	tx, _ := b.NewTransaction()
	users, err := tx.SearchUser(context.Background(), "uno", 0)
	if err != nil {
		t.Fatal(err)
	}
	if len(users) != 1 {
		t.Fatalf("expected 1 user, got %d", len(users))
	}
	if users[0] != testUser1 {
		t.Fatalf("expected %s, got %s", testUser1, users[0])
	}
}

func TestModel_SetResourceStatus(t *testing.T) {
	stop := ldaptest.StartServer(t, &ldaptest.Config{
		Dir:   "../../ldaptest",
		Port:  testLDAPPort,
		Base:  "dc=example,dc=com",
		LDIFs: []string{"testdata/base.ldif", "testdata/test1.ldif"},
	})
	defer stop()

	b, err := NewLDAPBackend(testLDAPAddr, "cn=manager,dc=example,dc=com", "password", "dc=example,dc=com")
	if err != nil {
		t.Fatal("NewLDAPBackend", err)
	}

	tx, _ := b.NewTransaction()
	rsrcID := as.ResourceID(fmt.Sprintf("mail=%s,uid=%s,ou=People,dc=example,dc=com", testUser1, testUser1))
	rr, err := tx.GetResource(context.Background(), rsrcID)
	if err != nil {
		t.Fatal("GetResource", err)
	}
	if rr == nil {
		t.Fatalf("could not find test resource %s", rsrcID)
	}

	rr.Status = as.ResourceStatusInactive
	if err := tx.UpdateResource(context.Background(), &rr.Resource); err != nil {
		t.Fatal("UpdateResource", err)
	}
	if err := tx.Commit(context.Background()); err != nil {
		t.Fatalf("commit error: %v", err)
	}
}

func TestModel_HasAnyResource(t *testing.T) {
	stop := ldaptest.StartServer(t, &ldaptest.Config{
		Dir:   "../../ldaptest",
		Port:  testLDAPPort,
		Base:  "dc=example,dc=com",
		LDIFs: []string{"testdata/base.ldif", "testdata/test1.ldif"},
	})
	defer stop()

	b, err := NewLDAPBackend(testLDAPAddr, "cn=manager,dc=example,dc=com", "password", "dc=example,dc=com")
	if err != nil {
		t.Fatal("NewLDAPBackend", err)
	}

	tx, _ := b.NewTransaction()

	// Request that should succeed.
	ok, err := tx.HasAnyResource(context.Background(), []as.FindResourceRequest{
		{Type: as.ResourceTypeEmail, Name: "foo"},
		{Type: as.ResourceTypeEmail, Name: testUser1},
	})
	if err != nil {
		t.Fatal("HasAnyResource", err)
	}
	if !ok {
		t.Fatal("could not find test email resource")
	}

	// Request that should fail (bad resource type).
	ok, err = tx.HasAnyResource(context.Background(), []as.FindResourceRequest{
		{Type: as.ResourceTypeDatabase, Name: testUser1},
	})
	if err != nil {
		t.Fatal("HasAnyResource", err)
	}
	if ok {
		t.Fatal("oops, found non existing resource")
	}
}

func TestModel_SearchResource(t *testing.T) {
	stop, b := startServer(t)
	defer stop()

	for _, pattern := range []string{"uno@investici.org", "uno*"} {
		tx, _ := b.NewTransaction()
		resources, err := tx.SearchResource(context.Background(), "uno@investici.org", 0)
		if err != nil {
			t.Fatalf("SearchUser(%s): %v", pattern, err)
		}
		if len(resources) != 1 {
			t.Fatalf("SearchUser(%s): expected 1 resource, got %d", pattern, len(resources))
		}
		if resources[0].Owner != "uno@investici.org" {
			t.Fatalf("SearchUser(%s): resource owner is %s, expected uno@investici.org", pattern, resources[0].Owner)
		}
	}
}

func TestModel_SetUserPassword(t *testing.T) {
	stop, b, user := startServerAndGetUser(t)
	defer stop()

	encPass := "encrypted password"

	tx, _ := b.NewTransaction()
	if err := tx.SetUserPassword(context.Background(), &user.User, encPass); err != nil {
		t.Fatal("SetUserPassword", err)
	}
	if err := tx.Commit(context.Background()); err != nil {
		t.Fatal("Commit", err)
	}

	// Verify that the new password is set.
	pwattr := tx.(*backendTX).readAttributeValues(
		context.Background(),
		"mail=uno@investici.org,uid=uno@investici.org,ou=People,dc=example,dc=com",
		"userPassword",
	)
	if len(pwattr) == 0 {
		t.Fatalf("no userPassword attribute on mail= object")
	}
	if len(pwattr) > 1 {
		t.Fatalf("more than one userPassword found on mail= object")
	}
	if pwattr[0] != encPass {
		t.Fatalf("bad userPassword, got %s, expected %s", pwattr[0], encPass)
	}
}

func TestModel_SetUserEncryptionKeys_Add(t *testing.T) {
	stop, b, user := startServerAndGetUser(t)
	defer stop()

	tx, _ := b.NewTransaction()
	keys := []*ct.EncryptedKey{
		{
			ID:           as.UserEncryptionKeyMainID,
			EncryptedKey: []byte("very secret key"),
		},
	}
	if err := tx.SetUserEncryptionKeys(context.Background(), &user.User, keys); err != nil {
		t.Fatal("SetUserEncryptionKeys", err)
	}
	if err := tx.Commit(context.Background()); err != nil {
		t.Fatal("Commit", err)
	}
}

func TestModel_SetUserEncryptionKeys_Replace(t *testing.T) {
	stop, b, user := startServerAndGetUser2(t)
	defer stop()

	tx, _ := b.NewTransaction()
	keys := []*ct.EncryptedKey{
		{
			ID:           as.UserEncryptionKeyMainID,
			EncryptedKey: []byte("very secret key"),
		},
	}
	if err := tx.SetUserEncryptionKeys(context.Background(), &user.User, keys); err != nil {
		t.Fatal("SetUserEncryptionKeys", err)
	}
	if err := tx.Commit(context.Background()); err != nil {
		t.Fatal("Commit", err)
	}
}

func TestModel_NextUID(t *testing.T) {
	stop, b, user := startServerAndGetUser(t)
	defer stop()
	tx, _ := b.NewTransaction()

	// User UID should not be available.
	ok, err := tx.(*backendTX).isUIDAvailable(context.Background(), user.UID)
	if err != nil {
		t.Fatal("isUIDAvailable", err)
	}
	if ok {
		t.Fatalf("oops, the uid of the existing user (%d) appears to be available", user.UID)
	}

	// Check a UID that should be available instead.
	freeUID := 4096
	ok, err = tx.(*backendTX).isUIDAvailable(context.Background(), freeUID)
	if err != nil {
		t.Fatal("isUIDAvailable", err)
	}
	if !ok {
		t.Fatalf("oops, a supposedly free uid (%d) appears to be unavailable", freeUID)
	}

	// Generate a new UID.
	uid, err := tx.NextUID(context.Background())
	if err != nil {
		t.Fatal("NextUID", err)
	}
	if uid == 0 {
		t.Fatal("uid is 0")
	}
	if uid == user.UID { // not that it's likely
		t.Fatalf("uid is the same as the existing user ID (%d)", user.UID)
	}
}

func TestSortResources(t *testing.T) {
	rsrcs := []*as.Resource{
		&as.Resource{
			ID:       "id1",
			ParentID: "id4",
		},
		&as.Resource{
			ID:       "id2",
			ParentID: "id4",
		},
		&as.Resource{
			ID: "id3",
		},
		&as.Resource{
			ID:       "id4",
			ParentID: "id3",
		},
	}
	expected := []string{"id3", "id4", "id1", "id2"}

	var ids []string
	for _, r := range sortResourcesByDepth(rsrcs) {
		ids = append(ids, r.ID.String())
	}
	if diff := deep.Equal(ids, expected); diff != nil {
		t.Fatalf("bad ordering: got %v, expected %v", ids, expected)
	}
}

func TestSortResources_ExternalParentID(t *testing.T) {
	rsrcs := []*as.Resource{
		&as.Resource{
			ID:       "id1",
			ParentID: "pre-existing-id",
		},
	}
	expected := []string{"id1"}

	var ids []string
	for _, r := range sortResourcesByDepth(rsrcs) {
		ids = append(ids, r.ID.String())
	}
	if diff := deep.Equal(ids, expected); diff != nil {
		t.Fatalf("bad ordering: got %v, expected %v", ids, expected)
	}
}
