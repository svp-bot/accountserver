package cachebackend

import (
	"time"

	"github.com/prometheus/client_golang/prometheus"
)

// There's only going to be one cache per server process in production
// so we can just use globals for instrumentation metrics.
var (
	cacheHits = prometheus.NewCounter(prometheus.CounterOpts{
		Name: "accountserver_cache_hits",
		Help: "Cache hit counter",
	})
	cacheMisses = prometheus.NewCounter(prometheus.CounterOpts{
		Name: "accountserver_cache_misses",
		Help: "Cache miss counter",
	})
	cacheWrites = prometheus.NewCounter(prometheus.CounterOpts{
		Name: "accountserver_cache_writes",
		Help: "Cache write counter",
	})
	cacheDeletes = prometheus.NewCounter(prometheus.CounterOpts{
		Name: "accountserver_cache_deletes",
		Help: "Cache delete counter",
	})
)

func init() {
	prometheus.MustRegister(cacheHits)
	prometheus.MustRegister(cacheMisses)
	prometheus.MustRegister(cacheWrites)
	prometheus.MustRegister(cacheDeletes)
}

type instrumentedCache struct {
	internalCache
}

func instrument(c internalCache) *instrumentedCache {
	return &instrumentedCache{c}
}

func (c *instrumentedCache) Get(key string) (interface{}, bool) {
	res, ok := c.internalCache.Get(key)
	if ok {
		cacheHits.Inc()
	} else {
		cacheMisses.Inc()
	}
	return res, ok
}

func (c *instrumentedCache) Set(key string, value interface{}, d time.Duration) {
	c.internalCache.Set(key, value, d)
	cacheWrites.Inc()
}

func (c *instrumentedCache) Delete(username string) {
	c.internalCache.Delete(username)
	cacheDeletes.Inc()
}
