package cachebackend

import (
	"context"
	"net/http"
	"time"

	"github.com/patrickmn/go-cache"
	"golang.org/x/sync/singleflight"

	as "git.autistici.org/ai3/accountserver"
	"git.autistici.org/ai3/go-common/clientutil"
	ct "git.autistici.org/ai3/go-common/ldap/compositetypes"
)

var (
	defaultExpiration = 600 * time.Second
	cleanupInterval   = 180 * time.Second
)

// This is the interface for the actual cache, an in-memory LRU cache
// with optional replicated invalidation.
type internalCache interface {
	Get(string) (interface{}, bool)
	Set(string, interface{}, time.Duration)
	Delete(string)
}

// CacheBackend is both an accountserver Backend and a http.Handler
// that implements the replicated invalidation RPCs.
//
// CacheBackend implements a simple in-memory cache of user objects
// (not resources yet), in order to reduce the database and processing
// load in presence of a heavily read-oriented workload. The cache is
// very simple, and any update to a user or its resources cause us to
// go back to the database backend.
//
// User objects are kept in memory for a short while and periodically
// cleaned up. Memory usage thus depends on the load and is difficult
// to estimate in advance.
//
type CacheBackend struct {
	as.Backend
	http.Handler

	cache internalCache
}

// Wrap a Backend with a cache.
func Wrap(b as.Backend, peers []string, tls *clientutil.TLSClientConfig) (*CacheBackend, error) {
	var h http.Handler = http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		http.NotFound(w, r)
	})

	var c internalCache = cache.New(defaultExpiration, cleanupInterval)

	if len(peers) > 0 {
		rc, err := newCacheWithReplicatedInvalidation(c, peers, tls)
		if err != nil {
			return nil, err
		}
		c = rc
		h = rc
	}

	return &CacheBackend{
		Backend: b,
		Handler: h,
		cache:   instrument(c),
	}, nil
}

// NewTransaction returns a new accountserver.TX unit-of-work object.
func (b *CacheBackend) NewTransaction() (as.TX, error) {
	innerTX, err := b.Backend.NewTransaction()
	if err != nil {
		return nil, err
	}
	return &cacheTX{
		TX:    innerTX,
		cache: b.cache,
	}, nil
}

// The cacheTX type wraps an accountserver.TX object.
type cacheTX struct {
	as.TX
	cache internalCache
}

func (c *cacheTX) invalidateUser(username string) {
	c.cache.Delete(username)
}

// Updates to the cache are controlled by a singleflight.Group to
// ensure that we only update each user once even with multiple
// callers.
var update singleflight.Group

func (c *cacheTX) GetUser(ctx context.Context, name string) (*as.RawUser, error) {
	obj, ok := c.cache.Get(name)
	if ok {
		return obj.(*as.RawUser), nil
	}

	tmp, err, _ := update.Do(name, func() (interface{}, error) {
		user, err := c.TX.GetUser(ctx, name)
		if err != nil {
			return nil, err
		}
		c.cache.Set(name, user, cache.DefaultExpiration)
		return user, nil
	})

	if err != nil {
		return nil, err
	}
	return tmp.(*as.RawUser), nil
}

func (c *cacheTX) UpdateUser(ctx context.Context, user *as.User) error {
	err := c.TX.UpdateUser(ctx, user)
	if err == nil {
		c.invalidateUser(user.Name)
	}
	return err
}

func (c *cacheTX) SetUserPassword(ctx context.Context, user *as.User, pw string) error {
	err := c.TX.SetUserPassword(ctx, user, pw)
	if err == nil {
		c.invalidateUser(user.Name)
	}
	return err
}

func (c *cacheTX) SetAccountRecoveryHint(ctx context.Context, user *as.User, rh, ra string) error {
	err := c.TX.SetAccountRecoveryHint(ctx, user, rh, ra)
	if err == nil {
		c.invalidateUser(user.Name)
	}
	return err
}

func (c *cacheTX) DeleteAccountRecoveryHint(ctx context.Context, user *as.User) error {
	err := c.TX.DeleteAccountRecoveryHint(ctx, user)
	if err == nil {
		c.invalidateUser(user.Name)
	}
	return err
}

func (c *cacheTX) SetUserEncryptionKeys(ctx context.Context, user *as.User, keys []*ct.EncryptedKey) error {
	err := c.TX.SetUserEncryptionKeys(ctx, user, keys)
	if err == nil {
		c.invalidateUser(user.Name)
	}
	return err
}

func (c *cacheTX) SetUserEncryptionPublicKey(ctx context.Context, user *as.User, pubKey []byte) error {
	err := c.TX.SetUserEncryptionPublicKey(ctx, user, pubKey)
	if err == nil {
		c.invalidateUser(user.Name)
	}
	return err
}

func (c *cacheTX) SetApplicationSpecificPassword(ctx context.Context, user *as.User, asp *as.AppSpecificPasswordInfo, pw string) error {
	err := c.TX.SetApplicationSpecificPassword(ctx, user, asp, pw)
	if err == nil {
		c.invalidateUser(user.Name)
	}
	return err
}

func (c *cacheTX) DeleteApplicationSpecificPassword(ctx context.Context, user *as.User, pw string) error {
	err := c.TX.DeleteApplicationSpecificPassword(ctx, user, pw)
	if err == nil {
		c.invalidateUser(user.Name)
	}
	return err
}

func (c *cacheTX) SetUserTOTPSecret(ctx context.Context, user *as.User, secret string) error {
	err := c.TX.SetUserTOTPSecret(ctx, user, secret)
	if err == nil {
		c.invalidateUser(user.Name)
	}
	return err
}

func (c *cacheTX) DeleteUserTOTPSecret(ctx context.Context, user *as.User) error {
	err := c.TX.DeleteUserTOTPSecret(ctx, user)
	if err == nil {
		c.invalidateUser(user.Name)
	}
	return err
}

func (c *cacheTX) UpdateResource(ctx context.Context, r *as.Resource) error {
	err := c.TX.UpdateResource(ctx, r)
	// TODO: invalidate user, but we currently do not know who it is!
	return err
}

func (c *cacheTX) CreateResources(ctx context.Context, user *as.User, resources []*as.Resource) ([]*as.Resource, error) {
	result, err := c.TX.CreateResources(ctx, user, resources)
	if err == nil && user != nil {
		c.invalidateUser(user.Name)
	}
	return result, err
}

//func (c *cacheTX) CreateUser(_ context.Context, user *as.User) (*as.User, error) {}
//func (c *cacheTX) GetResource(_ context.Context, id as.ResourceID) (*as.RawResource, error) {}
//func (c *cacheTX) SetResourcePassword(_ context.Context, r *as.Resource, pw string) error {}
//func (c *cacheTX) HasAnyResource(_ context.Context, f []as.FindResourceRequest) (bool, error) {}
//func (c *cacheTX) SearchUser(_ context.Context, string) ([]string, error) {}
//func (c *cacheTX) SearchResource(_ context.Context, string) ([]*as.RawResource, error) {}
//func (c *cacheTX) CanAccessResource(_ context.Context, principal string, r *as.Resource) bool {}
//func (c *cacheTX) NextUID(_ context.Context) (int, error)                                     {}
