package server

import (
	"crypto/tls"
	"encoding/json"
	"errors"
	"fmt"
	"log"
	"net/http"
	"net/http/httputil"
	"net/url"
	"reflect"
	"strings"

	"git.autistici.org/ai3/go-common/clientutil"
	"git.autistici.org/ai3/go-common/serverutil"
	"github.com/prometheus/client_golang/prometheus"

	as "git.autistici.org/ai3/accountserver"
)

var (
	requestCounter = prometheus.NewCounterVec(prometheus.CounterOpts{
		Name: "accountserver_requests_total",
		Help: "Total number of requests, by type",
	},
		[]string{"method", "status"})
)

func init() {
	prometheus.MustRegister(requestCounter)
}

type actionRegistry struct {
	service  *as.AccountService
	handlers map[string]reflect.Type
}

func newActionRegistry(service *as.AccountService) *actionRegistry {
	return &actionRegistry{
		service:  service,
		handlers: make(map[string]reflect.Type),
	}
}

func (r *actionRegistry) Register(path string, rtype as.Request) {
	r.handlers[path] = reflect.ValueOf(rtype).Elem().Type()
}

// Create a new instance of a Request type, along with the type name
// (used for monitoring).
func (r *actionRegistry) newRequest(path string) (as.Request, string, bool) {
	h, ok := r.handlers[path]
	if !ok {
		return nil, "", false
	}
	return reflect.New(h).Interface().(as.Request),
		strings.TrimSuffix(h.Name(), "Request"),
		true
}

func (r *actionRegistry) ServeHTTP(w http.ResponseWriter, httpReq *http.Request) {
	// Create a new empty request object based on the request
	// path, then decode the HTTP request JSON body onto it.
	req, reqTypeName, ok := r.newRequest(httpReq.URL.Path)
	if !ok {
		http.NotFound(w, httpReq)
		return
	}
	if !serverutil.DecodeJSONRequest(w, httpReq, req) {
		http.Error(w, "bad request", http.StatusBadRequest)
		return
	}

	resp, err := r.service.Handle(httpReq.Context(), req)
	if err != nil {
		// Handle structured errors, serve a JSON response.
		status := errToStatus(err)
		var verr *as.ValidationError
		if errors.As(err, &verr) {
			w.Header().Set("Content-Type", "application/json")
			w.WriteHeader(status)
			w.Write(verr.JSON()) // nolint
		} else {
			http.Error(w, err.Error(), status)
		}
	} else {
		// Don't send nulls, send empty dicts instead.
		if resp == nil {
			resp = emptyResponse
		}
		serverutil.EncodeJSONResponse(w, resp)
	}

	// Now that all is done, we can log the request/response
	// (sanitization might modify the objects in place).
	reqData := marshalJSONSanitized(req)
	reqStatus := "error"
	if err != nil {
		log.Printf("request: %s %s -> ERROR: %v", httpReq.URL.Path, reqData, err)
	} else {
		respData := marshalJSONSanitized(resp)
		log.Printf("request: %s %s -> %s", httpReq.URL.Path, reqData, respData)
		reqStatus = "ok"
	}

	// Increment the request metric.
	requestCounter.WithLabelValues(reqTypeName, reqStatus).Inc()
}

// APIServer is the HTTP API interface to AccountService. It
// implements the http.Handler interface.
type APIServer struct {
	*http.ServeMux
}

type apiEndpoint struct {
	path    string
	handler as.Request
}

var (
	readOnlyEndpoints = []apiEndpoint{
		{"/api/user/get", &as.GetUserRequest{}},
		{"/api/user/search", &as.SearchUserRequest{}},
		{"/api/resource/get", &as.GetResourceRequest{}},
		{"/api/resource/search", &as.SearchResourceRequest{}},
		{"/api/resource/check_availability", &as.CheckResourceAvailabilityRequest{}},
	}
	writeEndpoints = []apiEndpoint{
		{"/api/user/create", &as.CreateUserRequest{}},
		{"/api/user/update", &as.UpdateUserRequest{}},
		{"/api/user/disable", &as.DisableUserRequest{}},
		{"/api/user/admin_update", &as.AdminUpdateUserRequest{}},
		{"/api/user/change_password", &as.ChangeUserPasswordRequest{}},
		{"/api/user/reset_password", &as.ResetPasswordRequest{}},
		{"/api/user/set_account_recovery_hint", &as.SetAccountRecoveryHintRequest{}},
		{"/api/user/enable_otp", &as.EnableOTPRequest{}},
		{"/api/user/disable_otp", &as.DisableOTPRequest{}},
		{"/api/user/create_app_specific_password", &as.CreateApplicationSpecificPasswordRequest{}},
		{"/api/user/delete_app_specific_password", &as.DeleteApplicationSpecificPasswordRequest{}},
		{"/api/resource/set_status", &as.SetResourceStatusRequest{}},
		{"/api/resource/create", &as.CreateResourcesRequest{}},
		{"/api/resource/update", &as.AdminUpdateResourceRequest{}},
		{"/api/resource/move", &as.MoveResourceRequest{}},
		{"/api/resource/reset_password", &as.ResetResourcePasswordRequest{}},
		{"/api/resource/email/add_alias", &as.AddEmailAliasRequest{}},
		{"/api/resource/email/delete_alias", &as.DeleteEmailAliasRequest{}},
		{"/api/recover_account", &as.AccountRecoveryRequest{}},
	}
)

// New creates a new APIServer. If leaderAddr is not the empty string,
// write requests will be forwarded to that address.
func New(service *as.AccountService, backend as.Backend, leaderAddr string, clientTLS *clientutil.TLSClientConfig) (*APIServer, error) {
	registry := newActionRegistry(service)
	mux := http.NewServeMux()

	for _, ep := range readOnlyEndpoints {
		registry.Register(ep.path, ep.handler)
	}

	var fs *forwardServer
	if leaderAddr != "" {
		var err error
		fs, err = newForwardServer(leaderAddr, clientTLS)
		if err != nil {
			return nil, err
		}
	}
	for _, ep := range writeEndpoints {
		if leaderAddr == "" {
			registry.Register(ep.path, ep.handler)
		} else {
			mux.Handle(ep.path, fs)
		}
	}

	mux.Handle("/", registry)

	return &APIServer{ServeMux: mux}, nil
}

var emptyResponse struct{}

// A forwardServer is just a fancy httputil.ReverseProxy with loop
// detection (we don't want to receive proxied requests if we are not
// the leader).
type forwardServer struct {
	proxy *httputil.ReverseProxy
}

const (
	loopHdr      = "X-Accountserver-Forwarded"
	loopHdrValue = "true"
)

func newForwardServer(leaderURL string, tlsClientConf *clientutil.TLSClientConfig) (*forwardServer, error) {
	leader, err := url.Parse(strings.TrimRight(leaderURL, "/"))
	if err != nil {
		return nil, err
	}

	var tlsConf *tls.Config
	if tlsClientConf != nil {
		tlsConf, err = tlsClientConf.TLSConfig()
		if err != nil {
			return nil, err
		}
	}

	proxy := &httputil.ReverseProxy{
		Director: func(req *http.Request) {
			req.URL.Scheme = leader.Scheme
			req.URL.Host = leader.Host
			req.URL.Path = leader.Path + req.URL.Path

			// Loop protection.
			req.Header.Set(loopHdr, loopHdrValue)

			// Explicitly disable User-Agent so it's not set to default value.
			if _, ok := req.Header["User-Agent"]; !ok {
				req.Header.Set("User-Agent", "")
			}
		},
		Transport: &http.Transport{
			TLSClientConfig: tlsConf,
		},
		BufferPool: newBufferPool(8192, 64),
	}
	return &forwardServer{proxy: proxy}, nil
}

func (s *forwardServer) ServeHTTP(w http.ResponseWriter, req *http.Request) {
	// Simple forwarding loop checker, using out-of-band data in
	// the form of a custom HTTP header.
	if req.Header.Get(loopHdr) == loopHdrValue {
		log.Printf("received leader request from %s, aborted", req.RemoteAddr)
		http.Error(w, "This node is not the leader", http.StatusInternalServerError)
		return
	}

	s.proxy.ServeHTTP(w, req)
}

func errToStatus(err error) int {
	switch {
	case err == as.ErrUserNotFound, err == as.ErrResourceNotFound:
		return http.StatusNotFound
	case as.IsAuthError(err):
		return http.StatusForbidden
	case as.IsRequestError(err):
		return http.StatusBadRequest
	default:
		return http.StatusInternalServerError
	}
}

// Some requests contain private information that should not be
// logged: these objects should implement a Sanitize() method that
// modifies the object in-place by editing out the private fields.
type hasSanitize interface {
	Sanitize()
}

func marshalJSONSanitized(obj interface{}) string {
	if s, ok := obj.(hasSanitize); ok {
		s.Sanitize()
	}
	data, err := json.Marshal(obj)
	if err != nil {
		return fmt.Sprintf("SERIALIZATION ERROR: %v", err)
	}
	return string(data)
}

// Simple buffer pool for httputil.ReverseProxy.
type bufferPool struct {
	ch      chan []byte
	bufSize int
}

func newBufferPool(bufSize, poolSize int) *bufferPool {
	pool := make(chan []byte, poolSize)
	for i := 0; i < poolSize; i++ {
		pool <- make([]byte, bufSize)
	}
	return &bufferPool{
		ch:      pool,
		bufSize: bufSize,
	}
}

func (p *bufferPool) Get() (b []byte) {
	select {
	case b = <-p.ch:
	default:
		b = make([]byte, p.bufSize)
	}
	return
}

func (p *bufferPool) Put(b []byte) {
	select {
	case p.ch <- b:
	default:
	}
}
