module git.autistici.org/id/go-sso

go 1.14

require (
	git.autistici.org/ai3/go-common v0.0.0-20210110180225-a05c683cfe23
	git.autistici.org/id/auth v0.0.0-20210110171913-dd493db32815
	git.autistici.org/id/keystore v0.0.0-20210110165905-d5b171e81071
	github.com/crewjam/saml v0.4.5
	github.com/elazarl/go-bindata-assetfs v1.0.1
	github.com/gorilla/csrf v1.7.0
	github.com/gorilla/mux v1.8.0
	github.com/gorilla/securecookie v1.1.1
	github.com/jonboulle/clockwork v0.2.2 // indirect
	github.com/mattermost/xml-roundtrip-validator v0.0.0-20201219040909-8fd2afad43d1 // indirect
	github.com/mssola/user_agent v0.5.2
	github.com/oschwald/maxminddb-golang v1.8.0
	github.com/prometheus/client_golang v1.9.0
	github.com/rs/cors v1.7.0
	github.com/tstranex/u2f v1.0.0
	go.opencensus.io v0.22.5
	golang.org/x/crypto v0.0.0-20201221181555-eec23a3978ad
	gopkg.in/yaml.v2 v2.4.0
)
