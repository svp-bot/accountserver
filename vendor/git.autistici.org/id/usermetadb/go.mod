module git.autistici.org/id/usermetadb

go 1.14

require (
	git.autistici.org/ai3/go-common v0.0.0-20210507084757-2cf72583ce0e
	github.com/golang-migrate/migrate/v4 v4.14.1
	github.com/google/go-cmp v0.5.5
	github.com/gorilla/mux v1.8.0 // indirect
	github.com/hashicorp/errwrap v1.1.0 // indirect
	github.com/mattn/go-sqlite3 v1.14.7
	gopkg.in/yaml.v2 v2.4.0
)
