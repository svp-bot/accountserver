package usermetadb

//go:generate go-bindata --nocompress --pkg migrations --ignore \.go$ -o migrations/bindata.go -prefix migrations/ ./migrations

import (
	"errors"
	"time"
)

type CheckDeviceRequest struct {
	Username   string      `json:"username"`
	DeviceInfo *DeviceInfo `json:"device_info"`
}

type CheckDeviceResponse struct {
	Seen bool `json:"seen"`
}

const (
	LogTypeLogin          = "login"
	LogTypeLogout         = "logout"
	LogTypePasswordReset  = "password_reset"
	LogTypePasswordChange = "password_change"
	LogTypeOTPEnabled     = "otp_enabled"
	LogTypeOTPDisabled    = "otp_disabled"
)

const (
	LoginMethodPassword = "password"
	LoginMethodOTP      = "otp"
	LoginMethodU2F      = "u2f"
)

// DeviceInfo holds information about the client device. We use a
// simple persistent cookie to track the same client device across
// multiple session.
type DeviceInfo struct {
	ID         string `json:"id"`
	RemoteAddr string `json:"remote_addr"`
	RemoteZone string `json:"remote_zone"`
	UserAgent  string `json:"user_agent"`
	Browser    string `json:"browser"`
	OS         string `json:"os"`
	Mobile     bool   `json:"mobile"`
}

func (d *DeviceInfo) EncodeToMap(m map[string]string, prefix string) {
	m[prefix+"id"] = d.ID
	m[prefix+"remote_addr"] = d.RemoteAddr
	m[prefix+"remote_zone"] = d.RemoteZone
	m[prefix+"browser"] = d.Browser
	m[prefix+"os"] = d.OS
	m[prefix+"user_agent"] = d.UserAgent
	if d.Mobile {
		m[prefix+"mobile"] = "true"
	} else {
		m[prefix+"mobile"] = "false"
	}
}

func DecodeDeviceInfoFromMap(m map[string]string, prefix string) *DeviceInfo {
	return &DeviceInfo{
		ID:         m[prefix+"id"],
		RemoteAddr: m[prefix+"remote_addr"],
		RemoteZone: m[prefix+"remote_zone"],
		Browser:    m[prefix+"browser"],
		OS:         m[prefix+"os"],
		UserAgent:  m[prefix+"user_agent"],
		Mobile:     m[prefix+"mobile"] == "true",
	}
}

// LogEntry represents an authentication event in the user-specific log.
type LogEntry struct {
	Timestamp   time.Time   `json:"timestamp"`
	Username    string      `json:"username"`
	Type        string      `json:"log_type"`
	Message     string      `json:"message,omitempty"`
	Service     string      `json:"service,omitempty"`
	LoginMethod string      `json:"login_method,omitempty"`
	DeviceInfo  *DeviceInfo `json:"device_info,omitempty"`
}

func (e *LogEntry) Validate() error {
	if e.Username == "" {
		return errors.New("invalid log entry: missing username")
	}
	switch e.Type {
	case LogTypeLogin, LogTypeLogout, LogTypePasswordReset, LogTypePasswordChange, LogTypeOTPEnabled, LogTypeOTPDisabled:
	default:
		return errors.New("invalid log entry: unknown log type")
	}

	if e.DeviceInfo != nil {
		if e.DeviceInfo.ID == "" {
			return errors.New("invalid device info in log entry")
		}
	}

	return nil
}

type AddLogRequest struct {
	Log *LogEntry `json:"log"`
}

type AddLogResponse struct{}

type GetUserDevicesRequest struct {
	Username string `json:"username"`
}

type MetaDeviceInfo struct {
	DeviceInfo *DeviceInfo `json:"device_info"`
	FirstSeen  time.Time   `json:"first_seen"`
	LastSeen   time.Time   `json:"last_seen"`
	NumLogins  int         `json:"num_logins"`
}

type GetUserDevicesResponse struct {
	Devices []*MetaDeviceInfo `json:"devices"`
}

type GetUserLogsRequest struct {
	Username string `json:"username"`
	MaxDays  int    `json:"max_days"`
	Limit    int    `json:"limit"`
}

type GetUserLogsResponse struct {
	Results []*LogEntry `json:"result"`
}

type LastLoginEntry struct {
	Timestamp time.Time `json:"timestamp"`
	Username  string    `json:"username"`
	Service   string    `json:"service"`
}

func (e *LastLoginEntry) Validate() error {
	if e.Username == "" {
		return errors.New("invalid last login entry: missing username")
	}
	if e.Service == "" {
		return errors.New("invalid last login entry: missing service")
	}

	return nil
}

type SetLastLoginRequest struct {
	LastLogin *LastLoginEntry `json:"last_login"`
}

type SetLastLoginResponse struct{}

type GetLastLoginRequest struct {
	Username string `json:"username"`
	Service  string `json:"service,omitempty"`
}

type GetLastLoginResponse struct {
	Results []*LastLoginEntry `json:"result"`
}

type GetUnusedAccountsRequest struct {
	Usernames []string `json:"usernames"`
	Days      int      `json:"days"`
}

type GetUnusedAccountsResponse struct {
	UnusedUsernames []string `json:"unused_usernames"`
}
