module git.autistici.org/id/auth

go 1.14

require (
	git.autistici.org/ai3/go-common v0.0.0-20210507084757-2cf72583ce0e
	git.autistici.org/id/usermetadb v0.0.0-20210507085300-ad16aa223703
	github.com/bradfitz/gomemcache v0.0.0-20190913173617-a41fca850d0b
	github.com/cenkalti/backoff/v4 v4.1.0
	github.com/coreos/go-systemd/v22 v22.3.1
	github.com/go-ldap/ldap/v3 v3.3.0
	github.com/go-sql-driver/mysql v1.6.0
	github.com/google/go-cmp v0.5.5
	github.com/lib/pq v1.10.1
	github.com/mattn/go-sqlite3 v1.14.7
	github.com/patrickmn/go-cache v0.0.0-20180815053127-5633e0862627
	github.com/pquerna/otp v1.3.0
	github.com/prometheus/client_golang v1.10.0
	github.com/theckman/go-flock v0.8.0
	github.com/tstranex/u2f v1.0.0
	go.opencensus.io v0.23.0
	golang.org/x/sync v0.0.0-20210220032951-036812b2e83c
	gopkg.in/yaml.v2 v2.4.0
)
