package server

import (
	"bytes"
	"encoding/gob"

	"github.com/tstranex/u2f"
)

const u2fCacheExpirationSeconds = 300

func init() {
	gob.Register(&u2f.Challenge{})
}

// Short-term U2F challenge storage.
type u2fStorage struct {
	cacheClient
}

func newU2FStorage(cache cacheClient) *u2fStorage {
	return &u2fStorage{cache}
}

func (m *u2fStorage) SetUserChallenge(user string, chal *u2f.Challenge) error {
	data, err := serializeU2FChallenge(chal)
	if err != nil {
		return err
	}
	return m.writeAll(u2fChallengeKey(user), data, u2fCacheExpirationSeconds)
}

func (m *u2fStorage) GetUserChallenge(user string) (*u2f.Challenge, bool) {
	value, ok := m.readAny(u2fChallengeKey(user))
	if !ok {
		return nil, false
	}
	chal, err := deserializeU2FChallenge(value)
	if err != nil {
		return nil, false
	}
	return chal, true
}

func u2fChallengeKey(user string) string {
	return "u2f/" + user
}

func serializeU2FChallenge(chal *u2f.Challenge) ([]byte, error) {
	var buf bytes.Buffer
	if err := gob.NewEncoder(&buf).Encode(chal); err != nil {
		return nil, err
	}
	return buf.Bytes(), nil
}

func deserializeU2FChallenge(data []byte) (*u2f.Challenge, error) {
	var chal u2f.Challenge
	if err := gob.NewDecoder(bytes.NewReader(data)).Decode(&chal); err != nil {
		return nil, err
	}
	return &chal, nil
}
