package server

import (
	"context"
	"errors"
	"fmt"
	"log"
	"path/filepath"
	"time"

	"github.com/pquerna/otp/totp"
	"github.com/prometheus/client_golang/prometheus"
	"github.com/tstranex/u2f"

	"git.autistici.org/ai3/go-common/pwhash"
	"git.autistici.org/id/auth"
	"git.autistici.org/id/auth/backend"

	fileBE "git.autistici.org/id/auth/backend/file"
	ldapBE "git.autistici.org/id/auth/backend/ldap"
	sqlBE "git.autistici.org/id/auth/backend/sql"
)

// OTPShortTermStorage stores short-term otp tokens for replay
// protection purposes.
type OTPShortTermStorage interface {
	AddToken(string, string) error
	HasToken(string, string) bool
}

// U2FShortTermStorage stores short-term u2f challenges.
type U2FShortTermStorage interface {
	SetUserChallenge(string, *u2f.Challenge) error
	GetUserChallenge(string) (*u2f.Challenge, bool)
}

// A filter that can be applied to a request, potentially changing it.
type requestFilter interface {
	Filter(*backend.User, *auth.Request, *auth.Response) *auth.Response
}

// Authenticate users for a specific service.
type service struct {
	rl                []*authRatelimiter
	bl                []*authBlacklist
	filters           []requestFilter
	backends          []backend.ServiceBackend
	challengeResponse bool
	enforce2FA        bool
	ignore2FA         bool
	aspService        string
}

func (s *service) checkRateLimits(username string, req *auth.Request) (bool, string) {
	for _, rl := range s.rl {
		if !rl.AllowIncr(username, req) {
			return false, rl.rl.name
		}
	}
	for _, bl := range s.bl {
		if !bl.Allow(username, req) {
			return false, bl.bl.name
		}
	}
	return true, ""
}

func (s *service) notifyBlacklists(username string, req *auth.Request, resp *auth.Response) {
	for _, bl := range s.bl {
		bl.Incr(username, req, resp)
	}
}

// A serviceBackend wrapper that sets additional group memberships.
type staticGroupsServiceBackend struct {
	backend.ServiceBackend
	groups []string
}

func withStaticGroups(wrap backend.ServiceBackend, groups []string) backend.ServiceBackend {
	return &staticGroupsServiceBackend{
		ServiceBackend: wrap,
		groups:         groups,
	}
}

func (b *staticGroupsServiceBackend) GetUser(ctx context.Context, name string) (*backend.User, bool) {
	user, ok := b.ServiceBackend.GetUser(ctx, name)
	if ok {
		user.Groups = append(user.Groups, b.groups...)
	}
	return user, ok
}

// Create backend implementations from the given Config.
func createBackends(config *Config) (map[string]backend.UserBackend, error) {
	// Get the config base dir to evaluate relative paths in
	// backend configuration.
	configDir := filepath.Dir(config.path)
	backends := make(map[string]backend.UserBackend)
	for name, params := range config.Backends {
		var b backend.UserBackend
		var err error
		switch name {
		case "file":
			b, err = fileBE.New(params, configDir)
		case "ldap":
			b, err = ldapBE.New(params, configDir)
		case "sql":
			b, err = sqlBE.New(params, configDir)
		default:
			err = fmt.Errorf("unknown backend type %s", name)
		}
		if err != nil {
			return nil, err
		}
		backends[name] = b
	}
	return backends, nil
}

// Create a service backend from the given BackendSpec.
func createServiceBackend(spec *backend.Spec, backends map[string]backend.UserBackend) (backend.ServiceBackend, error) {
	b, ok := backends[spec.BackendName]
	if !ok {
		return nil, fmt.Errorf("unknown backend %s", spec.BackendName)
	}
	bb, err := b.NewServiceBackend(spec)
	if err != nil {
		return nil, err
	}
	if len(spec.StaticGroups) > 0 {
		bb = withStaticGroups(bb, spec.StaticGroups)
	}
	return bb, nil
}

// Create a service from the given ServiceConfig.
func createService(config *Config, sc *ServiceConfig, backends map[string]backend.UserBackend, ratelimiters map[string]*authRatelimiter, blacklists map[string]*authBlacklist) (*service, error) {
	s := &service{
		enforce2FA:        sc.Enforce2FA,
		ignore2FA:         sc.Ignore2FA,
		challengeResponse: sc.ChallengeResponse,
		aspService:        sc.ASPService,
	}

	// Parse all BackendSpecs.
	for _, spec := range sc.BackendSpecs {
		b, err := createServiceBackend(spec, backends)
		if err != nil {
			return nil, err
		}
		s.backends = append(s.backends, b)
	}

	// Link to the configured ratelimit objects.
	for _, name := range sc.Ratelimits {
		if rl, ok := ratelimiters[name]; ok {
			s.rl = append(s.rl, rl)
		} else if bl, ok := blacklists[name]; ok {
			s.bl = append(s.bl, bl)
		} else {
			return nil, fmt.Errorf("unknown rate limiter '%s'", name)
		}
	}

	if sc.EnableLastLoginReporting {
		if config.UserMetaDBConfig == nil {
			return nil, errors.New("usermetadb config is missing")
		}
		llr, err := newLastLoginFilter(config.UserMetaDBConfig)
		if err != nil {
			return nil, err
		}
		s.filters = append(s.filters, llr)
	}

	// Enabling device tracking also enables user activity
	// logging.
	if sc.EnableDeviceTracking {
		if config.UserMetaDBConfig == nil {
			return nil, errors.New("usermetadb config is missing")
		}
		dt, err := newDeviceFilter(config.UserMetaDBConfig)
		if err != nil {
			return nil, err
		}
		s.filters = append(s.filters, dt)

		// The logger conveniently comes last.
		lf, err := newUserActivityLogFilter(config.UserMetaDBConfig)
		if err != nil {
			return nil, err
		}
		s.filters = append(s.filters, lf)
	}

	return s, nil
}

// Create the global rate limiters and blacklists.
func createRatelimiters(config *Config) (map[string]*authRatelimiter, map[string]*authBlacklist, error) {
	ratelimiters := make(map[string]*authRatelimiter)
	blacklists := make(map[string]*authBlacklist)
	for name, params := range config.RateLimiters {
		if params.BlacklistTime > 0 {
			bl, err := newAuthBlacklist(name, params)
			if err != nil {
				return nil, nil, err
			}
			blacklists[name] = bl
		} else {
			rl, err := newAuthRatelimiter(name, params)
			if err != nil {
				return nil, nil, err
			}
			ratelimiters[name] = rl
		}
	}
	return ratelimiters, blacklists, nil
}

// Server is the main authentication server object.
type Server struct {
	services     map[string]*service
	backends     map[string]backend.UserBackend
	u2fShortTerm U2FShortTermStorage
	otpShortTerm OTPShortTermStorage
}

// NewServer creates a Server using the given configuration.
func NewServer(config *Config) (*Server, error) {
	// Build backends, rate limiters and blacklists. These are global
	// objects which are then shared by the various services.
	backends, err := createBackends(config)
	if err != nil {
		return nil, err
	}

	ratelimiters, blacklists, err := createRatelimiters(config)
	if err != nil {
		return nil, err
	}

	// Build the service map, connecting services to backends.
	services := make(map[string]*service)
	for name, sc := range config.Services {
		svc, err := createService(config, sc, backends, ratelimiters, blacklists)
		if err != nil {
			return nil, err
		}
		services[name] = svc
	}

	// Connect to memcache, if configured to do so, otherwise fall back to
	// a simple in-memory cache.
	var cache cacheClient
	if len(config.MemcacheServers) > 0 {
		var err error
		cache, err = newMemcacheReplicatedClient(config.MemcacheServers)
		if err != nil {
			return nil, err
		}
	} else {
		cache = newInprocessCache()
	}

	return &Server{
		services:     services,
		backends:     backends,
		u2fShortTerm: newU2FStorage(cache),
		otpShortTerm: newOTPStorage(cache),
	}, nil
}

// Close the authentication server and release all associated resources.
func (s *Server) Close() {
	for _, b := range s.backends {
		b.Close()
	}
}

func (s *Server) getService(name string) (*service, bool) {
	svc, ok := s.services[name]
	if !ok {
		svc, ok = s.services["default"]
	}
	return svc, ok
}

func (s *Server) getUser(ctx context.Context, service *service, username string) (*backend.User, bool) {
	for _, b := range service.backends {
		if user, ok := b.GetUser(ctx, username); ok {
			return user, true
		}
	}
	return nil, false
}

// Authenticate a user with the parameters specified in the incoming AuthRequest.
func (s *Server) Authenticate(ctx context.Context, req *auth.Request) *auth.Response {
	var errmsg string
	resp, err := s.doAuthenticate(ctx, req)
	if err != nil {
		errmsg = fmt.Sprintf(" error=%v", err)
	}
	if resp == nil {
		resp = newError()
	}

	// Log the request.
	var ipstr string
	if req.DeviceInfo != nil && req.DeviceInfo.RemoteAddr != "" {
		ipstr = fmt.Sprintf(" ip=%s", req.DeviceInfo.RemoteAddr)
	}
	log.Printf("auth: user=%s service=%s status=%s%s%s", req.Username, req.Service, resp.Status.String(), ipstr, errmsg)

	// Increment the request counter.
	authRequestsCounter.With(prometheus.Labels{
		"service": req.Service,
		"status":  resp.Status.String(),
	}).Inc()
	return resp
}

var (
	errServiceUnknown = errors.New("unknown service")
	errUserUnknown    = errors.New("unknown user")
	errNoMechanisms   = errors.New("no authentication mechanisms available")
	errBadPassword    = errors.New("wrong password")
	errBadASP         = errors.New("wrong app-specific password")
	errBadU2F         = errors.New("bad U2F response")
	errBadOTP         = errors.New("invalid OTP")
)

// Function with the actual authentication API logic - we return both
// an auth.Response and an error. The outer Authenticate() wrapper
// turns the latter into a Response with Status: Error anyway, but
// adds explicit logging of the error message (useful for internal
// server errors etc).
func (s *Server) doAuthenticate(ctx context.Context, req *auth.Request) (*auth.Response, error) {
	start := time.Now()

	svc, ok := s.getService(req.Service)
	if !ok {
		return nil, errServiceUnknown
	}

	// Apply rate limiting and blacklisting _before_ invoking the
	// authentication handlers, as they may be CPU intensive.
	if allowed, blockedBy := svc.checkRateLimits(req.Username, req); !allowed {
		ratelimitCounter.With(prometheus.Labels{
			"service": req.Service,
		}).Inc()
		return nil, fmt.Errorf("ratelimited (%s)", blockedBy)
	}

	// When a user is unknown, we still want to invoke the
	// blacklists, but without a username: this will still apply
	// IP-based blacklists, but will prevent memory explosion for
	// dictionary scans of the username space.
	var blUsername string
	var resp *auth.Response
	err := errUserUnknown
	user, ok := s.getUser(ctx, svc, req.Username)
	if !ok {
		// User is unknown to all backends.
		goto fail
	}
	blUsername = user.Name

	resp, err = s.authenticateUser(req, svc, user)

	// Increment stats counters.
	authRequestLatency.With(prometheus.Labels{
		"service": req.Service,
	}).Observe(time.Since(start).Seconds() * 1000)

fail:
	// Notify blacklists of the result.
	svc.notifyBlacklists(blUsername, req, resp)

	return resp, err
}

// Authenticate a user. Returning an error should result in an
// AuthResponse with StatusError.
func (s *Server) authenticateUser(req *auth.Request, svc *service, user *backend.User) (resp *auth.Response, err error) {
	err = errNoMechanisms

	// The service can override the ASP service name presented in
	// the request.
	aspService := req.Service
	if svc.aspService != "" {
		aspService = svc.aspService
	}

	// Verify different credentials depending on whether the user
	// has 2FA enabled or not, and on whether the service itself
	// supports challenge-response authentication.
	if svc.ignore2FA {
		resp, err = s.authenticateUserWithPassword(user, req)
	} else if svc.challengeResponse {
		if svc.enforce2FA || user.Has2FA() {
			resp, err = s.authenticateUserWith2FA(user, req)
		} else if user.HasASPs(aspService) {
			// It doesn't make much sense to support ASPs
			// for a challenge-response service, but we
			// still do it for completeness.
			//
			// Rewrite the 'service' for app-specific
			// password matching, if necessary.
			resp, err = s.authenticateUserWithASP(user, req, aspService)
		} else {
			resp, err = s.authenticateUserWithPassword(user, req)
		}
	} else if svc.enforce2FA || user.HasASPs(aspService) {
		resp, err = s.authenticateUserWithASP(user, req, aspService)
	} else if !user.Has2FA() {
		resp, err = s.authenticateUserWithPassword(user, req)
	}
	if err != nil {
		return
	}

	// Process the response through filters (device info checks,
	// etc) that may or may not change the response itself.
	//
	// TODO: it's unclear why we'd want to bail on errors, it
	// makes it quite confusing for StatusInsufficientCredentials.
	//
	// Perhaps it would be easier to just run filters only on
	// StatusOK?
	for _, f := range svc.filters {
		if resp.Status == auth.StatusError {
			break
		}
		resp = f.Filter(user, req, resp)
	}

	// If the response is successful, augment it with user information.
	if resp.Status == auth.StatusOK {
		resp.UserInfo = user.UserInfo()
	}
	return
}

func (s *Server) authenticateUserWithPassword(user *backend.User, req *auth.Request) (*auth.Response, error) {
	// Ok we only need to check the password here.
	if checkPassword(req.Password, user.EncryptedPassword) {
		return newOK(), nil
	}
	return nil, errBadPassword
}

func (s *Server) authenticateUserWithASP(user *backend.User, req *auth.Request, aspService string) (*auth.Response, error) {
	for _, asp := range user.AppSpecificPasswords {
		if asp.Service == aspService && checkPassword(req.Password, asp.EncryptedPassword) {
			return newOK(), nil
		}
	}
	return nil, errBadASP
}

func (s *Server) authenticateUserWith2FA(user *backend.User, req *auth.Request) (*auth.Response, error) {
	// First of all verify the password.
	if !checkPassword(req.Password, user.EncryptedPassword) {
		return nil, errBadPassword
	}

	// If the request contains one of the 2FA attributes, verify
	// it. But if it contains none, we return with
	// AuthStatusInsufficientCredentials and potentially a 2FA
	// hint (for U2F).
	switch {
	case req.U2FResponse != nil:
		if user.HasU2F() && s.checkU2F(user, req.U2FResponse) {
			return newOK(), nil
		}
		return nil, errBadU2F
	case req.OTP != "":
		if user.HasOTP() && s.checkOTP(user, req.OTP, user.TOTPSecret) {
			// Save the token for replay protection.
			if err := s.otpShortTerm.AddToken(user.Name, req.OTP); err != nil {
				log.Printf("error saving OTP token to short-term storage: %v", err)
			}
			return newOK(), nil
		}
		return nil, errBadOTP
	default:
		resp := &auth.Response{
			Status: auth.StatusInsufficientCredentials,
		}
		// Two-factor mechanisms are returned in order of
		// decreasing preference, so start with U2F.
		if req.U2FAppID != "" && user.HasU2F() {
			resp.TFAMethods = append(resp.TFAMethods, auth.TFAMethodU2F)
			signReq, err := s.u2fSignRequest(user, req.U2FAppID)
			if err != nil {
				return nil, err
			}
			resp.U2FSignRequest = signReq
		}
		if user.HasOTP() {
			resp.TFAMethods = append(resp.TFAMethods, auth.TFAMethodOTP)
		}
		return resp, nil
	}
}

func (s *Server) u2fSignRequest(user *backend.User, appID string) (*u2f.WebSignRequest, error) {
	challenge, err := u2f.NewChallenge(appID, []string{appID})
	if err != nil {
		return nil, err
	}

	if err := s.u2fShortTerm.SetUserChallenge(user.Name, challenge); err != nil {
		return nil, err
	}

	return challenge.SignRequest(user.U2FRegistrations), nil
}

func (s *Server) checkU2F(user *backend.User, resp *u2f.SignResponse) bool {
	challenge, ok := s.u2fShortTerm.GetUserChallenge(user.Name)
	if !ok {
		return false
	}
	for _, reg := range user.U2FRegistrations {
		_, err := reg.Authenticate(*resp, *challenge, 0)
		if err == nil {
			return true
		}
	}
	return false
}

func checkPassword(password, hash []byte) bool {
	return pwhash.ComparePassword(string(hash), string(password))
}

func (s *Server) checkOTP(user *backend.User, otp, secret string) bool {
	// Check our short-ttl blacklist for the token (replay protection).
	if s.otpShortTerm.HasToken(user.Name, otp) {
		log.Printf("replay protection triggered for %s", user.Name)
		return false
	}

	return totp.Validate(otp, secret)
}

func newError() *auth.Response {
	return &auth.Response{Status: auth.StatusError}
}

func newOK() *auth.Response {
	return &auth.Response{Status: auth.StatusOK}
}

// Instrumentation.
var (
	authRequestsCounter = prometheus.NewCounterVec(
		prometheus.CounterOpts{
			Name: "auth_requests",
			Help: "Number of authentication requests.",
		},
		[]string{"service", "status"},
	)
	authRequestLatency = prometheus.NewHistogramVec(
		prometheus.HistogramOpts{
			Name:    "auth_latency",
			Help:    "Latency of authentication requests (server side).",
			Buckets: prometheus.ExponentialBuckets(5, 1.4142, 16),
		},
		[]string{"service"},
	)
	ratelimitCounter = prometheus.NewCounterVec(
		prometheus.CounterOpts{
			Name: "auth_requests_ratelimited",
			Help: "Number of rate-limited authentication requests.",
		},
		[]string{"service"},
	)
)

func init() {
	prometheus.MustRegister(authRequestsCounter)
	prometheus.MustRegister(authRequestLatency)
	prometheus.MustRegister(ratelimitCounter)
}
