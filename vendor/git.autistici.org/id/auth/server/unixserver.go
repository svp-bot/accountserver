package server

import (
	"bytes"
	"context"
	"errors"
	"fmt"

	"git.autistici.org/id/auth"
	"git.autistici.org/id/auth/lineproto"
)

// SocketServer accepts connections on a UNIX socket, speaking the
// line-based wire protocol, and dispatches incoming requests to the
// wrapped Server.
type SocketServer struct {
	codec auth.Codec
	auth  *Server
}

// NewSocketServer returns a new SocketServer listening on the given path.
func NewSocketServer(authServer *Server) *SocketServer {
	return &SocketServer{
		auth:  authServer,
		codec: auth.DefaultCodec,
	}
}

// ServeLine handles a single request and writes a
// response. Implements the unix.LineHandler interface.
func (s *SocketServer) ServeLine(ctx context.Context, lw lineproto.LineResponseWriter, line []byte) error {
	// Parse the incoming command. The only two known
	// commands are 'auth' for an authentication request,
	// and 'quit' to terminate the connection (closing the
	// connection also works). We shut down the connection
	// on any protocol error.
	parts := bytes.SplitN(line, []byte{' '}, 2)
	nargs := len(parts)
	if nargs < 1 {
		return errors.New("no command given")
	}
	cmd := string(parts[0])
	switch {
	case nargs == 1 && cmd == "quit":
		return lineproto.ErrCloseConnection
	case nargs == 2 && cmd == "auth":
		return s.handleAuth(ctx, lw, parts[1])
	default:
		return errors.New("syntax error")
	}
}

func (s *SocketServer) handleAuth(ctx context.Context, lw lineproto.LineResponseWriter, arg []byte) error {
	var req auth.Request
	if err := s.codec.Decode(arg, &req); err != nil {
		return fmt.Errorf("decoding error: %v", err)
	}

	resp := s.auth.Authenticate(ctx, &req)

	return lw.WriteLine(s.codec.Encode(resp))
}
