package server

import (
	"errors"
	"sync"
	"time"

	"git.autistici.org/ai3/go-common/clientutil"
	"github.com/bradfitz/gomemcache/memcache"
	cache "github.com/patrickmn/go-cache"
)

var (
	memcacheTimeout      = 10 * time.Second
	memcacheMaxIdleConns = 3
)

// Client for a short-term cache.
//
// Data should only be consistent over a short period of time, and the
// worst case scenario (challenge can't be retrieved) will simply
// cause the user to retry, so even in a replicated setup we do not
// need a strong consistency strategy: we simply fan out all reads and
// writes to all memcache servers in parallel.
type cacheClient interface {
	writeAll(string, []byte, int) error
	readAny(string) ([]byte, bool)
}

// A cacheClient that uses one or more memcached servers. DNS names
// will be watched and the backend list will be updated dynamically if
// the addresses change.
type memcacheReplicatedClient struct {
	watcher *clientutil.MultiDNSWatcher

	mx          sync.RWMutex
	caches      []*memcache.Client
	initialized bool
	initCh      chan struct{}
}

func newMemcacheReplicatedClient(addrs []string) (*memcacheReplicatedClient, error) {
	w, err := clientutil.NewMultiDNSWatcher(addrs)
	if err != nil {
		return nil, err
	}

	// Initialize our client with the DNS watcher, and wait until
	// we have at least one address to connect to.
	m := &memcacheReplicatedClient{
		initCh:  make(chan struct{}),
		watcher: w,
	}
	go m.watchForChanges()
	<-m.initCh

	return m, nil
}

func (m *memcacheReplicatedClient) clients() []*memcache.Client {
	m.mx.RLock()
	defer m.mx.RUnlock()
	return m.caches
}

// Watch for DNS changes and rebuild the list of memcache clients.
func (m *memcacheReplicatedClient) watchForChanges() {
	for addrs := range m.watcher.Changes() {
		var caches []*memcache.Client
		for _, addr := range addrs {
			c := memcache.New(addr)
			c.Timeout = memcacheTimeout
			c.MaxIdleConns = memcacheMaxIdleConns
			caches = append(caches, c)
		}
		m.mx.Lock()
		m.caches = caches
		if !m.initialized {
			m.initialized = true
			close(m.initCh)
		}
		m.mx.Unlock()
	}
}

func (m *memcacheReplicatedClient) writeAll(key string, value []byte, ttl int) error {
	item := &memcache.Item{
		Key:        key,
		Value:      value,
		Expiration: int32(ttl),
	}

	// Write to all memcache servers. We return as soon as the first
	// write succeeds, keeping a goroutine around to close the channel
	// when all the writes are done. This leaks goroutines in case of
	// network issues, so keep a short timeout.
	ch := make(chan error, 1)
	var wg sync.WaitGroup

	for _, c := range m.clients() {
		wg.Add(1)
		go func(c *memcache.Client) {
			ch <- c.Set(item)
			wg.Done()
		}(c)
	}

	go func() {
		wg.Wait()
		close(ch)
	}()

	for err := range ch {
		if err == nil {
			return nil
		}
	}
	return errors.New("all memcache servers failed")
}

func (m *memcacheReplicatedClient) readAny(key string) ([]byte, bool) {
	// Run all reads in parallel, return the first non-error result.
	//
	// This would be better if the memcache API took a Context, so
	// we could cancel all pending calls as soon as a result is
	// received. This way, we keep them running in the background,
	// ignore their results, and fire a goroutine to avoid leaking
	// the result channel.
	ch := make(chan []byte, 1)
	var wg sync.WaitGroup

	for _, c := range m.clients() {
		wg.Add(1)
		go func(c *memcache.Client) {
			if item, err := c.Get(key); err == nil {
				select {
				case ch <- item.Value:
				default:
				}
			}
			wg.Done()
		}(c)
	}

	go func() {
		wg.Wait()
		close(ch)
	}()

	value := <-ch
	if value == nil {
		return nil, false
	}
	return value, true
}

// A cacheClient that uses an in-process cache.
type inprocessCache struct {
	cache *cache.Cache
}

func newInprocessCache() *inprocessCache {
	return &inprocessCache{
		cache: cache.New(5*time.Minute, 10*time.Minute),
	}
}

func (c *inprocessCache) writeAll(key string, value []byte, ttl int) error {
	// Force the value to string in order to make a copy.
	c.cache.Set(key, string(value), time.Duration(ttl)*time.Second)
	return nil
}

func (c *inprocessCache) readAny(key string) ([]byte, bool) {
	value, ok := c.cache.Get(key)
	if !ok {
		return nil, false
	}
	return []byte(value.(string)), ok
}
