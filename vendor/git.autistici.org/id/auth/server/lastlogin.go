package server

import (
	"context"
	"log"
	"time"

	"git.autistici.org/ai3/go-common/clientutil"
	"git.autistici.org/id/auth"
	"git.autistici.org/id/auth/backend"
	"git.autistici.org/id/usermetadb"
	"git.autistici.org/id/usermetadb/client"
)

type setLastLoginClient interface {
	SetLastLogin(context.Context, string, *usermetadb.LastLoginEntry) error
}

type lastloginFilter struct {
	client setLastLoginClient
}

func newLastLoginFilter(config *clientutil.BackendConfig) (*lastloginFilter, error) {
	c, err := client.New(config)
	if err != nil {
		return nil, err
	}
	return &lastloginFilter{c}, nil
}

var lastloginTimeout = 30 * time.Second

func (f *lastloginFilter) Filter(user *backend.User, req *auth.Request, resp *auth.Response) *auth.Response {
	if resp.Status != auth.StatusOK {
		return resp
	}

	entry := usermetadb.LastLoginEntry{
		Timestamp: time.Now(),
		Username:  user.Name,
		Service:   req.Service,
	}

	// Make the log RPC in the background, no need to wait for it to complete.
	go func() {
		ctx, cancel := context.WithTimeout(context.Background(), lastloginTimeout)
		defer cancel()
		if err := f.client.SetLastLogin(ctx, user.Shard, &entry); err != nil {
			log.Printf("usermetadb.SetLastLogin error for %s: %v", user.Name, err)
		}
	}()

	return resp
}
