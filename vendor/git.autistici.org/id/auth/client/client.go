package client

import (
	"context"
	"net"
	"strings"

	"github.com/cenkalti/backoff/v4"
	"go.opencensus.io/trace"

	"git.autistici.org/id/auth"
	"git.autistici.org/id/auth/lineproto"
)

var (
	DefaultSocketPath = "/run/auth/socket"
	DefaultPoolSize   = 3
)

type Client interface {
	Authenticate(context.Context, *auth.Request) (*auth.Response, error)
}

type socketClient struct {
	socketPath string
	codec      auth.Codec
	pool       *Pool
}

func New(socketPath string) Client {
	return &socketClient{
		socketPath: socketPath,
		codec:      auth.DefaultCodec,
		pool: NewPool(func() (*lineproto.Conn, error) {
			c, err := net.Dial("unix", socketPath)
			if err != nil {
				return nil, err
			}
			return lineproto.NewConn(c, ""), nil
		}, DefaultPoolSize),
	}
}

func (c *socketClient) Authenticate(ctx context.Context, req *auth.Request) (*auth.Response, error) {
	// Create a tracing span for the authentication request.
	sctx, span := trace.StartSpan(ctx, "auth-server.Authenticate",
		trace.WithSpanKind(trace.SpanKindClient))
	defer span.End()
	span.AddAttributes(
		trace.StringAttribute("auth.service", req.Service),
		trace.StringAttribute("auth.username", req.Username),
	)

	// Retry the request, with backoff, if we get a temporary
	// network error.
	var resp *auth.Response
	err := backoff.Retry(func() error {
		var err error
		resp, err = c.doAuthenticate(sctx, req)
		if err == nil {
			return nil
		} else if strings.Contains(err.Error(), "use of closed network connection") {
			return err
		} else if netErr, ok := err.(net.Error); ok && netErr.Temporary() {
			return netErr
		}
		return backoff.Permanent(err)
	}, backoff.WithContext(backoff.NewExponentialBackOff(), sctx))

	span.SetStatus(responseToTraceStatus(resp, err))

	return resp, err
}

func (c *socketClient) doAuthenticate(ctx context.Context, req *auth.Request) (*auth.Response, error) {
	var resp auth.Response
	err := c.pool.WithConn(func(conn *lineproto.Conn) error {
		// Make space in the channel for at least one element, or we
		// will leak a goroutine whenever the authentication request
		// times out.
		done := make(chan error, 1)

		go func() {
			defer close(done)

			// Write the auth command to the connection.
			if err := conn.WriteLine([]byte("auth "), c.codec.Encode(req)); err != nil {
				done <- err
				return
			}

			// Read the response.
			line, err := conn.ReadLine()
			if err != nil {
				done <- err
				return
			}
			if err := c.codec.Decode(line, &resp); err != nil {
				done <- err
				return
			}
			done <- nil
		}()

		// Wait for the call to terminate, or the context to time out,
		// whichever happens first.
		select {
		case err := <-done:
			return err
		case <-ctx.Done():
			return ctx.Err()
		}
	})
	return &resp, err
}

func responseToTraceStatus(resp *auth.Response, err error) trace.Status {
	switch err {
	case nil:
		switch resp.Status {
		case auth.StatusOK:
			return trace.Status{Code: trace.StatusCodeOK, Message: "OK"}
		case auth.StatusInsufficientCredentials:
			return trace.Status{Code: trace.StatusCodePermissionDenied, Message: "Insufficient Credentials"}
		default:
			return trace.Status{Code: trace.StatusCodePermissionDenied, Message: "Authentication Failure"}
		}
	case context.Canceled:
		return trace.Status{Code: trace.StatusCodeCancelled, Message: "CANCELED"}
	case context.DeadlineExceeded:
		return trace.Status{Code: trace.StatusCodeDeadlineExceeded, Message: "DEADLINE_EXCEEDED"}
	default:
		return trace.Status{Code: trace.StatusCodeUnknown, Message: err.Error()}
	}
}
