package client

import (
	"git.autistici.org/id/auth/lineproto"
)

type PoolDialer func() (*lineproto.Conn, error)

type Pool struct {
	ch     chan *lineproto.Conn
	dialer PoolDialer
}

func NewPool(dialer PoolDialer, size int) *Pool {
	return &Pool{
		ch:     make(chan *lineproto.Conn, size),
		dialer: dialer,
	}
}

func (p *Pool) WithConn(f func(*lineproto.Conn) error) error {
	// Acquire a connection.
	var conn *lineproto.Conn
	select {
	case conn = <-p.ch:
	default:
		var err error
		conn, err = p.dialer()
		if err != nil {
			return err
		}
	}

	// Run the function and inspect its return value.
	err := f(conn)
	if err != nil {
		conn.Close()
	} else {
		select {
		case p.ch <- conn:
		default:
			conn.Close()
		}
	}

	return err
}
