package server

import (
	"context"
	"database/sql"
	"errors"
	"log"

	ct "git.autistici.org/ai3/go-common/ldap/compositetypes"
	"github.com/tstranex/u2f"
	"gopkg.in/yaml.v2"

	_ "github.com/go-sql-driver/mysql"
	_ "github.com/lib/pq"
	_ "github.com/mattn/go-sqlite3"

	"git.autistici.org/id/auth/backend"
)

// Names for the known SQL queries.
const (
	sqlQueryGetUser   = "get_user"
	sqlQueryGetGroups = "get_user_groups"
	sqlQueryGetU2F    = "get_user_u2f"
	sqlQueryGetASP    = "get_user_asp"
)

// Default SQL queries.
var defaultSQLQueries = map[string]string{
	sqlQueryGetUser: `
SELECT email, password, totp_secret, '' AS shard FROM users WHERE name = ?
`,
}

type sqlConfig struct {
	Driver string `yaml:"driver"`
	URI    string `yaml:"db_uri"`
}

type sqlServiceConfig struct {
	Queries map[string]string `yaml:"queries"`
}

type sqlBackend struct {
	db *sql.DB
}

type sqlServiceBackend struct {
	db    *sql.DB
	stmts map[string]*sql.Stmt
}

func compileStatements(db *sql.DB, queries map[string]string) (map[string]*sql.Stmt, error) {
	m := make(map[string]*sql.Stmt)
	for name, query := range queries {
		stmt, err := db.Prepare(query)
		if err != nil {
			return nil, err
		}
		m[name] = stmt
	}
	return m, nil
}

// New returns a new SQL backend.
func New(params yaml.MapSlice, _ string) (backend.UserBackend, error) {
	var sc sqlConfig
	if err := backend.UnmarshalMapSlice(params, &sc); err != nil {
		return nil, err
	}
	if sc.Driver == "" {
		return nil, errors.New("driver is empty")
	}

	db, err := sql.Open(sc.Driver, sc.URI)
	if err != nil {
		return nil, err
	}

	return &sqlBackend{
		db: db,
	}, nil
}

func (b *sqlBackend) Close() {
	b.db.Close()
}

func (b *sqlBackend) NewServiceBackend(spec *backend.Spec) (backend.ServiceBackend, error) {
	var sc sqlServiceConfig
	if err := backend.UnmarshalMapSlice(spec.Params, &sc); err != nil {
		return nil, err
	}
	return newSQLServiceBackend(b.db, &sc)
}

func newSQLServiceBackend(db *sql.DB, sc *sqlServiceConfig) (*sqlServiceBackend, error) {
	// Apply default queries.
	for name, q := range defaultSQLQueries {
		if _, ok := sc.Queries[name]; !ok {
			sc.Queries[name] = q
		}
	}

	// Compile the SQL statements.
	stmts, err := compileStatements(db, sc.Queries)
	if err != nil {
		return nil, err
	}
	return &sqlServiceBackend{
		db:    db,
		stmts: stmts,
	}, nil
}

func (b *sqlServiceBackend) GetUser(ctx context.Context, name string) (*backend.User, bool) {
	tx, err := b.db.Begin()
	if err != nil {
		return nil, false
	}
	defer tx.Rollback() // nolint

	user := backend.User{Name: name}

	// Use NullStrings for optional fields.
	var nullableTOTP, nullableShard sql.NullString
	stmt := tx.Stmt(b.stmts[sqlQueryGetUser])
	defer stmt.Close()
	row := stmt.QueryRow(name)
	if err := row.Scan(&user.Email, &user.EncryptedPassword, &nullableTOTP, &nullableShard); err != nil {
		return nil, false
	}
	if nullableTOTP.Valid {
		user.TOTPSecret = nullableTOTP.String
	}
	if nullableShard.Valid {
		user.Shard = nullableShard.String
	}

	// Now read the one-to-many relations.
	if groups, err := b.getUserGroups(tx, name); err == nil {
		user.Groups = groups
	}
	if regs, err := b.getUserU2FRegistrations(tx, name); err == nil {
		user.U2FRegistrations = regs
	}
	if asps, err := b.getUserASPs(tx, name); err == nil {
		user.AppSpecificPasswords = asps
	}

	return &user, true
}

func (b *sqlServiceBackend) getUserU2FRegistrations(tx *sql.Tx, name string) ([]u2f.Registration, error) {
	stmtTpl, ok := b.stmts[sqlQueryGetU2F]
	if !ok {
		return nil, nil
	}
	stmt := tx.Stmt(stmtTpl)
	defer stmt.Close()
	rows, err := stmt.Query(name)
	if err != nil {
		return nil, err
	}
	defer rows.Close()

	// Use the compositetypes.U2FRegistration type to decode the
	// U2F registration public key data into a usable format.
	var out []u2f.Registration
	for rows.Next() {
		var ctr ct.U2FRegistration
		if err := rows.Scan(&ctr.PublicKey, &ctr.KeyHandle); err != nil {
			continue
		}
		reg, err := ctr.Decode()
		if err != nil {
			log.Printf("invalid u2f registration: %v", err)
			continue
		}
		out = append(out, *reg)
	}
	return out, rows.Err()
}

func (b *sqlServiceBackend) getUserASPs(tx *sql.Tx, name string) ([]*backend.AppSpecificPassword, error) {
	stmtTpl, ok := b.stmts[sqlQueryGetASP]
	if !ok {
		return nil, nil
	}
	stmt := tx.Stmt(stmtTpl)
	defer stmt.Close()
	rows, err := stmt.Query(name)
	if err != nil {
		return nil, err
	}
	defer rows.Close()

	var out []*backend.AppSpecificPassword
	for rows.Next() {
		var asp backend.AppSpecificPassword
		if err := rows.Scan(&asp.Service, &asp.EncryptedPassword); err != nil {
			continue
		}
		out = append(out, &asp)
	}
	return out, rows.Err()
}

func (b *sqlServiceBackend) getUserGroups(tx *sql.Tx, name string) ([]string, error) {
	stmtTpl, ok := b.stmts[sqlQueryGetGroups]
	if !ok {
		return nil, nil
	}
	stmt := tx.Stmt(stmtTpl)
	defer stmt.Close()
	rows, err := stmt.Query(name)
	if err != nil {
		return nil, err
	}
	defer rows.Close()

	var out []string
	for rows.Next() {
		var group string
		if err := rows.Scan(&group); err != nil {
			continue
		}
		out = append(out, group)
	}
	return out, rows.Err()
}
