package backend

import (
	"io/ioutil"
	"path/filepath"

	"gopkg.in/yaml.v2"
)

// Unmarshal a partially-parsed yaml.MapSlice.
func UnmarshalMapSlice(raw yaml.MapSlice, obj interface{}) error {
	b, err := yaml.Marshal(raw)
	if err != nil {
		return err
	}
	return yaml.Unmarshal(b, obj)
}

// Load and unmarshal a YAML file.
func LoadYAML(path string, obj interface{}) error {
	data, err := ioutil.ReadFile(path) // #nosec
	if err != nil {
		return err
	}
	return yaml.Unmarshal(data, obj)
}

// ResolvePath returns the path evaluated as relative to base.
func ResolvePath(path, base string) string {
	if !filepath.IsAbs(path) {
		path = filepath.Join(base, path)
	}
	return path
}
