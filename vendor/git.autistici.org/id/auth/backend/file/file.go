package server

import (
	"context"
	"encoding/base64"
	"encoding/hex"
	"log"

	ct "git.autistici.org/ai3/go-common/ldap/compositetypes"
	"github.com/tstranex/u2f"
	"gopkg.in/yaml.v2"

	"git.autistici.org/id/auth/backend"
)

// BackendSpec parameters for the file backend.
type fileServiceParams struct {
	Src string `yaml:"src"`
}

type fileUser struct {
	Name              string   `yaml:"name"`
	Email             string   `yaml:"email"`
	Shard             string   `yaml:"shard"`
	EncryptedPassword string   `yaml:"password"`
	TOTPSecret        string   `yaml:"totp_secret"`
	Groups            []string `yaml:"groups"`

	// U2F registrations are encoded in a similar format as the
	// one produced by 'pamu2fcfg': the key handle is
	// base64-encoded (this is "websafe" base64, without padding),
	// the public key is hex encoded.
	U2FRegistrations []struct {
		KeyHandle string `yaml:"key_handle"`
		PublicKey string `yaml:"public_key"`
	} `yaml:"u2f_registrations"`

	AppSpecificPasswords []struct {
		Service           string `yaml:"service"`
		EncryptedPassword string `yaml:"password"`
	} `yaml:"app_specific_passwords"`
}

func (f *fileUser) getU2FRegistrations(filename string) []u2f.Registration {
	var out []u2f.Registration
	for _, r := range f.U2FRegistrations {
		kh, err := base64.RawURLEncoding.DecodeString(r.KeyHandle)
		if err != nil {
			log.Printf("warning: %s: user %s: could not decode U2F key handle: %v", filename, f.Name, err)
			continue
		}
		pk, err := hex.DecodeString(r.PublicKey)
		if err != nil {
			log.Printf("warning: %s: user %s: could not decode U2F public key: %v", filename, f.Name, err)
			continue
		}
		ctr := ct.U2FRegistration{
			KeyHandle: kh,
			PublicKey: pk,
		}
		reg, err := ctr.Decode()
		if err != nil {
			log.Printf("warning: %s: user %s: could not decode U2F registration: %v", filename, f.Name, err)
			continue
		}
		out = append(out, *reg)
	}
	return out
}

func (f *fileUser) toUser(filename string) *backend.User {
	u := &backend.User{
		Name:              f.Name,
		Email:             f.Email,
		Shard:             f.Shard,
		EncryptedPassword: []byte(f.EncryptedPassword),
		TOTPSecret:        f.TOTPSecret,
		Groups:            f.Groups,
		U2FRegistrations:  f.getU2FRegistrations(filename),
	}
	for _, asp := range f.AppSpecificPasswords {
		u.AppSpecificPasswords = append(u.AppSpecificPasswords, &backend.AppSpecificPassword{
			Service:           asp.Service,
			EncryptedPassword: []byte(asp.EncryptedPassword),
		})
	}
	return u
}

// Simple file-based authentication backend, list users and their
// credentials in a YAML-encoded file.
type fileBackend struct {
	files     map[string]map[string]*backend.User
	configDir string
}

func loadUsersFile(path string) (map[string]*backend.User, error) {
	var userList []*fileUser
	if err := backend.LoadYAML(path, &userList); err != nil {
		return nil, err
	}
	users := make(map[string]*backend.User)
	for _, u := range userList {
		users[u.Name] = u.toUser(path)
	}
	return users, nil
}

// New creates a new file-based UserBackend.
func New(_ yaml.MapSlice, configDir string) (backend.UserBackend, error) {
	return &fileBackend{
		files:     make(map[string]map[string]*backend.User),
		configDir: configDir,
	}, nil
}

func (b *fileBackend) getUserMap(path string) (map[string]*backend.User, error) {
	m, ok := b.files[path]
	if !ok {
		var err error
		m, err = loadUsersFile(path)
		if err != nil {
			return nil, err
		}
		b.files[path] = m
	}
	return m, nil
}

func (b *fileBackend) Close() {}

func (b *fileBackend) NewServiceBackend(spec *backend.Spec) (backend.ServiceBackend, error) {
	var params fileServiceParams
	if err := backend.UnmarshalMapSlice(spec.Params, &params); err != nil {
		return nil, err
	}
	m, err := b.getUserMap(backend.ResolvePath(params.Src, b.configDir))
	if err != nil {
		return nil, err
	}
	return fileServiceBackend(m), nil
}

type fileServiceBackend map[string]*backend.User

func (b fileServiceBackend) GetUser(_ context.Context, name string) (*backend.User, bool) {
	u, ok := b[name]
	return u, ok
}
