package backend

import (
	"context"

	"github.com/tstranex/u2f"
	"gopkg.in/yaml.v2"

	"git.autistici.org/id/auth"
)

// User contains the attributes of a user account as relevant to the
// authentication server. It is only used internally, to communicate
// between the authserver and its storage backends.
type User struct {
	Name                 string
	Email                string
	Shard                string
	EncryptedPassword    []byte
	TOTPSecret           string
	U2FRegistrations     []u2f.Registration
	AppSpecificPasswords []*AppSpecificPassword
	Groups               []string
}

// AppSpecificPassword is a password tied to a single service.
type AppSpecificPassword struct {
	Service           string
	EncryptedPassword []byte
}

// Has2FA returns true if the user supports any interactive 2FA method.
func (u *User) Has2FA() bool {
	return u.HasU2F() || u.HasOTP()
}

// HasASPs returns true if the user has app-specific passwords.
func (u *User) HasASPs(service string) bool {
	for _, asp := range u.AppSpecificPasswords {
		if asp.Service == service {
			return true
		}
	}
	return false
}

// HasOTP returns true if the user supports (T)OTP.
func (u *User) HasOTP() bool {
	return u.TOTPSecret != ""
}

// HasU2F returns true if the user supports U2F.
func (u *User) HasU2F() bool {
	return len(u.U2FRegistrations) > 0
}

// UserInfo returns extra user information in the format required by
// the auth wire protocol.
func (u *User) UserInfo() *auth.UserInfo {
	return &auth.UserInfo{
		Email:  u.Email,
		Shard:  u.Shard,
		Groups: u.Groups,
	}
}

// Spec specifies backend-specific configuration for a service.
type Spec struct {
	BackendName  string        `yaml:"backend"`
	Params       yaml.MapSlice `yaml:"params"`
	StaticGroups []string      `yaml:"static_groups"`
}

// UserBackend provides us with per-service user information.
type UserBackend interface {
	Close()
	NewServiceBackend(*Spec) (ServiceBackend, error)
}

// ServiceBackend looks up user info for a specific service.
type ServiceBackend interface {
	GetUser(context.Context, string) (*User, bool)
}
