// Package compositetypes provides Go types for the composite values
// stored in our LDAP database, so that various authentication
// packages can agree on their serialized representation.
//
// These are normally 1-to-many associations that are wrapped into
// repeated LDAP attributes instead of separate nested objects, for
// simplicity and latency reasons.
//
// Whenever there is an 'id' field, it's a unique (per-user)
// identifier used to recognize a specific entry on modify/delete.
//
// The serialized values can be arbitrary []byte sequences (the LDAP
// schema should specify the right types for the associated
// attributes).
//
package compositetypes

import (
	"crypto/elliptic"
	"errors"
	"strings"

	"github.com/tstranex/u2f"
)

// AppSpecificPassword stores information on an application-specific
// password.
//
// Serialized as colon-separated fields with the format:
//
//     id:service:encrypted_password:comment
//
// Where 'comment' is free-form and can contain colons, no escaping is
// performed.
type AppSpecificPassword struct {
	ID                string `json:"id"`
	Service           string `json:"service"`
	EncryptedPassword string `json:"encrypted_password"`
	Comment           string `json:"comment"`
}

// Marshal returns the serialized format.
func (p *AppSpecificPassword) Marshal() string {
	return strings.Join([]string{
		p.ID,
		p.Service,
		p.EncryptedPassword,
		p.Comment,
	}, ":")
}

// UnmarshalAppSpecificPassword parses a serialized representation of
// an AppSpecificPassword.
func UnmarshalAppSpecificPassword(s string) (*AppSpecificPassword, error) {
	parts := strings.SplitN(s, ":", 4)
	if len(parts) != 4 {
		return nil, errors.New("badly encoded app-specific password")
	}
	return &AppSpecificPassword{
		ID:                parts[0],
		Service:           parts[1],
		EncryptedPassword: parts[2],
		Comment:           parts[3],
	}, nil
}

// EncryptedKey stores a password-encrypted secret key.
//
// Serialized as colon-separated fields with the format:
//
//     id:encrypted_key
//
// The encrypted key is stored as a raw, unencoded byte sequence.
type EncryptedKey struct {
	ID           string `json:"id"`
	EncryptedKey []byte `json:"encrypted_key"`
}

// Marshal returns the serialized format.
func (k *EncryptedKey) Marshal() string {
	var b []byte
	b = append(b, []byte(k.ID)...)
	b = append(b, ':')
	b = append(b, k.EncryptedKey...)
	return string(b)
}

// UnmarshalEncryptedKey parses the serialized representation of an
// EncryptedKey.
func UnmarshalEncryptedKey(s string) (*EncryptedKey, error) {
	idx := strings.IndexByte(s, ':')
	if idx < 0 {
		return nil, errors.New("badly encoded key")
	}
	return &EncryptedKey{
		ID:           s[:idx],
		EncryptedKey: []byte(s[idx+1:]),
	}, nil
}

// U2FRegistration stores information on a single U2F device
// registration.
//
// The serialized format follows part of the U2F standard and just
// stores 64 bytes of the public key immediately followed by the key
// handle data, with no encoding. Note that the public key itself is a
// serialization of the elliptic curve parameters.
//
// The data in U2FRegistration is still encoded, but it can be turned
// into a usable form (github.com/tstranex/u2f.Registration) later.
type U2FRegistration struct {
	KeyHandle []byte `json:"key_handle"`
	PublicKey []byte `json:"public_key"`
}

// Marshal returns the serialized format.
func (r *U2FRegistration) Marshal() string {
	var b []byte
	b = append(b, r.PublicKey...)
	b = append(b, r.KeyHandle...)
	return string(b)
}

// UnmarshalU2FRegistration parses a U2FRegistration from its serialized format.
func UnmarshalU2FRegistration(s string) (*U2FRegistration, error) {
	if len(s) < 65 {
		return nil, errors.New("badly encoded u2f registration")
	}
	b := []byte(s)
	return &U2FRegistration{
		PublicKey: b[:65],
		KeyHandle: b[65:],
	}, nil
}

// Decode returns a u2f.Registration object with the decoded public
// key ready for use in verification.
func (r *U2FRegistration) Decode() (*u2f.Registration, error) {
	x, y := elliptic.Unmarshal(elliptic.P256(), r.PublicKey)
	if x == nil {
		return nil, errors.New("invalid public key")
	}
	var reg u2f.Registration
	reg.PubKey.Curve = elliptic.P256()
	reg.PubKey.X = x
	reg.PubKey.Y = y
	reg.KeyHandle = r.KeyHandle
	return &reg, nil
}

// NewU2FRegistrationFromData creates a U2FRegistration from a
// u2f.Registration object.
func NewU2FRegistrationFromData(reg *u2f.Registration) *U2FRegistration {
	pk := elliptic.Marshal(reg.PubKey.Curve, reg.PubKey.X, reg.PubKey.Y)
	return &U2FRegistration{
		PublicKey: pk,
		KeyHandle: reg.KeyHandle,
	}
}
