package ldaputil

import (
	"context"
	"errors"
	"net"
	"net/url"
	"time"

	"github.com/cenkalti/backoff/v4"
	"github.com/go-ldap/ldap/v3"
	"github.com/prometheus/client_golang/prometheus"
	"go.opencensus.io/trace"
)

// Parameters that define the exponential backoff algorithm used.
var (
	ExponentialBackOffInitialInterval = 100 * time.Millisecond
	ExponentialBackOffMultiplier      = 1.4142
)

// newExponentialBackOff creates a backoff.ExponentialBackOff object
// with our own default values.
func newExponentialBackOff() *backoff.ExponentialBackOff {
	b := backoff.NewExponentialBackOff()
	b.InitialInterval = ExponentialBackOffInitialInterval
	b.Multiplier = ExponentialBackOffMultiplier

	// Set MaxElapsedTime to 0 because we expect the overall
	// timeout to be dictated by the request Context.
	b.MaxElapsedTime = 0

	return b
}

// ConnectionPool provides a goroutine-safe pool of long-lived LDAP
// connections that will reconnect on errors.
type ConnectionPool struct {
	network string
	addr    string
	bindDN  string
	bindPw  string

	c chan *ldap.Conn
}

var defaultConnectTimeout = 5 * time.Second

func (p *ConnectionPool) connect(ctx context.Context) (*ldap.Conn, error) {
	connectionsCounter.Inc()

	// Dial the connection with a timeout, if the context has a
	// deadline (as it should). If the context does not have a
	// deadline, we set a default timeout.
	deadline, ok := ctx.Deadline()
	if !ok {
		deadline = time.Now().Add(defaultConnectTimeout)
	}

	c, err := net.DialTimeout(p.network, p.addr, time.Until(deadline))
	if err != nil {
		connectionErrors.Inc()
		return nil, err
	}

	conn := ldap.NewConn(c, false)
	conn.Start()

	if p.bindDN != "" {
		conn.SetTimeout(time.Until(deadline))
		if _, err = conn.SimpleBind(ldap.NewSimpleBindRequest(p.bindDN, p.bindPw, nil)); err != nil {
			connectionErrors.Inc()
			conn.Close()
			return nil, err
		}
	}

	return conn, nil
}

// Get a fresh connection from the pool.
func (p *ConnectionPool) Get(ctx context.Context) (*ldap.Conn, error) {
	// Grab a connection from the cache, or create a new one if
	// there are no available connections.
	select {
	case conn := <-p.c:
		return conn, nil
	default:
		return p.connect(ctx)
	}
}

// Release a used connection onto the pool.
func (p *ConnectionPool) Release(conn *ldap.Conn, err error) {
	// Connections that failed should not be reused.
	if err != nil && !isProtocolError(err) {
		conn.Close()
		return
	}

	// Return the connection to the cache, or close it if it's
	// full.
	select {
	case p.c <- conn:
	default:
		conn.Close()
	}
}

// Close all connections. Not implemented yet.
func (p *ConnectionPool) Close() {}

// Parse a LDAP URI into network and address strings suitable for
// ldap.Dial.
func parseLDAPURI(uri string) (string, string, error) {
	u, err := url.Parse(uri)
	if err != nil {
		return "", "", err
	}

	network := "tcp"
	addr := "localhost:389"
	switch u.Scheme {
	case "ldap":
		if u.Host != "" {
			addr = u.Host
		}
	case "ldapi":
		network = "unix"
		addr = u.Path
	default:
		return "", "", errors.New("unsupported scheme")
	}

	return network, addr, nil
}

// NewConnectionPool creates a new pool of LDAP connections to the
// specified server, using the provided bind credentials. The pool
// will cache at most cacheSize connections.
func NewConnectionPool(uri, bindDN, bindPw string, cacheSize int) (*ConnectionPool, error) {
	network, addr, err := parseLDAPURI(uri)
	if err != nil {
		return nil, err
	}

	return &ConnectionPool{
		c:       make(chan *ldap.Conn, cacheSize),
		network: network,
		addr:    addr,
		bindDN:  bindDN,
		bindPw:  bindPw,
	}, nil
}

func (p *ConnectionPool) doRequest(ctx context.Context, name string, attrs []trace.Attribute, fn func(*ldap.Conn) error) error {
	// Tracing: initialize a new client span.
	sctx, span := trace.StartSpan(ctx, name,
		trace.WithSpanKind(trace.SpanKindClient))
	defer span.End()

	if len(attrs) > 0 {
		span.AddAttributes(attrs...)
	}

	rerr := backoff.Retry(func() error {
		conn, err := p.Get(sctx)
		if err != nil {
			// Here conn is nil, so we don't need to Release it.
			if isTemporaryLDAPError(err) {
				return err
			}
			return backoff.Permanent(err)
		}

		if deadline, ok := sctx.Deadline(); ok {
			conn.SetTimeout(time.Until(deadline))
		}

		err = fn(conn)
		p.Release(conn, err)
		if err != nil && !isTemporaryLDAPError(err) {
			err = backoff.Permanent(err)
		}
		return err
	}, backoff.WithContext(newExponentialBackOff(), ctx))

	// Tracing: set the final status.
	span.SetStatus(errorToTraceStatus(rerr))
	requestsCounter.WithLabelValues(name).Inc()
	if rerr != nil {
		requestErrors.WithLabelValues(name).Inc()
	}

	return rerr
}

// Search performs the given search request. It will retry the request
// on temporary errors.
func (p *ConnectionPool) Search(ctx context.Context, searchRequest *ldap.SearchRequest) (*ldap.SearchResult, error) {
	var result *ldap.SearchResult
	err := p.doRequest(ctx, "ldap.Search", []trace.Attribute{
		trace.StringAttribute("ldap.base", searchRequest.BaseDN),
		trace.StringAttribute("ldap.filter", searchRequest.Filter),
		trace.Int64Attribute("ldap.scope", int64(searchRequest.Scope)),
	}, func(conn *ldap.Conn) (cerr error) {
		result, cerr = conn.Search(searchRequest)
		return
	})
	return result, err
}

// Modify issues a ModifyRequest to the LDAP server.
func (p *ConnectionPool) Modify(ctx context.Context, modifyRequest *ldap.ModifyRequest) error {
	return p.doRequest(ctx, "ldap.Modify", []trace.Attribute{
		trace.StringAttribute("ldap.dn", modifyRequest.DN),
	}, func(conn *ldap.Conn) error {
		return conn.Modify(modifyRequest)
	})
}

// Add issues an AddRequest to the LDAP server.
func (p *ConnectionPool) Add(ctx context.Context, addRequest *ldap.AddRequest) error {
	return p.doRequest(ctx, "ldap.Add", []trace.Attribute{
		trace.StringAttribute("ldap.dn", addRequest.DN),
	}, func(conn *ldap.Conn) error {
		return conn.Add(addRequest)
	})
}

// Interface matched by net.Error.
type hasTemporary interface {
	Temporary() bool
}

// Treat network errors as temporary. Other errors are permanent by
// default.
func isTemporaryLDAPError(err error) bool {
	switch v := err.(type) {
	case *ldap.Error:
		switch v.ResultCode {
		case ldap.ErrorNetwork:
			return true
		default:
			return false
		}
	case hasTemporary:
		return v.Temporary()
	default:
		return false
	}
}

// Return true if the error is protocol-level, i.e. we have not left
// the LDAP connection in a problematic state. This relies on the
// explicit numeric values of the ResultCode attribute in ldap.Error.
func isProtocolError(err error) bool {
	if ldapErr, ok := err.(*ldap.Error); ok {
		// All protocol-level errors have values < 200.
		return ldapErr.ResultCode < ldap.ErrorNetwork
	}
	return false
}

func errorToTraceStatus(err error) trace.Status {
	switch err {
	case nil:
		return trace.Status{Code: trace.StatusCodeOK, Message: "OK"}
	case context.Canceled:
		return trace.Status{Code: trace.StatusCodeCancelled, Message: "CANCELED"}
	case context.DeadlineExceeded:
		return trace.Status{Code: trace.StatusCodeDeadlineExceeded, Message: "DEADLINE_EXCEEDED"}
	default:
		return trace.Status{Code: trace.StatusCodeUnknown, Message: err.Error()}
	}
}

var (
	connectionsCounter = prometheus.NewCounter(prometheus.CounterOpts{
		Name: "ldap_connections_total",
		Help: "Counter of new LDAP connections.",
	})
	connectionErrors = prometheus.NewCounter(prometheus.CounterOpts{
		Name: "ldap_connection_errors_total",
		Help: "Counter of LDAP connection errors.",
	})
	requestsCounter = prometheus.NewCounterVec(prometheus.CounterOpts{
		Name: "ldap_requests_total",
		Help: "Counter of LDAP requests.",
	}, []string{"method"})
	requestErrors = prometheus.NewCounterVec(prometheus.CounterOpts{
		Name: "ldap_errors_total",
		Help: "Counter of LDAP requests.",
	}, []string{"method"})
)

func init() {
	prometheus.MustRegister(
		connectionsCounter,
		connectionErrors,
		requestsCounter,
		requestErrors,
	)
}
