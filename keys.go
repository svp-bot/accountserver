package accountserver

import (
	ct "git.autistici.org/ai3/go-common/ldap/compositetypes"
	"git.autistici.org/ai3/go-common/userenckey"
)

// A list of encrypted keys, all copies of the same key but encrypted with
// different passwords.
type encryptedKeyList []*ct.EncryptedKey

func newEncryptionKeys(encryptionPassword string) ([]byte, encryptedKeyList, error) {
	pub, priv, err := userenckey.GenerateKey()
	if err != nil {
		return nil, nil, err
	}
	encrypted, err := userenckey.Encrypt(priv, []byte(encryptionPassword))
	if err != nil {
		return nil, nil, err
	}
	l := encryptedKeyList([]*ct.EncryptedKey{
		&ct.EncryptedKey{
			ID:           UserEncryptionKeyMainID,
			EncryptedKey: encrypted,
		},
	})
	return pub, l, nil
}

func keysToBytes(keys []*ct.EncryptedKey) [][]byte {
	var rawKeys [][]byte
	for _, k := range keys {
		rawKeys = append(rawKeys, k.EncryptedKey)
	}
	return rawKeys
}

func (l encryptedKeyList) add(keyID, unlockPassword, encryptionPassword string) (encryptedKeyList, error) {
	decrypted, err := userenckey.Decrypt(keysToBytes(l), []byte(unlockPassword))
	if err != nil {
		return nil, err
	}
	encrypted, err := userenckey.Encrypt(decrypted, []byte(encryptionPassword))
	if err != nil {
		return nil, err
	}

	l = l.deleteByID(keyID)
	return append(l, &ct.EncryptedKey{
		ID:           keyID,
		EncryptedKey: encrypted,
	}), nil
}

func (l encryptedKeyList) deleteByID(keyID string) encryptedKeyList {
	var out encryptedKeyList
	for _, k := range l {
		if k.ID != keyID {
			out = append(out, k)
		}
	}
	return out
}

// Return the ID for the encrypted key associated with an app-specific
// password.
func aspKeyID(aspID string) string {
	return "asp_" + aspID
}
