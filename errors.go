package accountserver

import (
	"encoding/json"
	"errors"
	"fmt"
	"strings"
)

var (
	// ErrUserNotFound is returned when a user object is not found.
	ErrUserNotFound = errors.New("user not found")

	// ErrResourceNotFound is returned when a resource object is not found.
	ErrResourceNotFound = errors.New("resource not found")
)

// It is important to distinguish between different classes of errors,
// so that they can be translated into distinct HTTP status codes and
// transmitted back to the client. Since we also want to retain the
// original error message, we wrap the original error with our custom
// types.

// AuthError is an authentication error.
type AuthError struct {
	error
}

func (e *AuthError) Unwrap() error { return e.error }

func newAuthError(err error) error {
	return &AuthError{err}
}

// IsAuthError returns true if err is an authentication /
// authorization error.
func IsAuthError(err error) bool {
	var a *AuthError
	return errors.As(err, &a)
}

// RequestError indicates an issue with validating the request.
type RequestError struct {
	error
}

func (e *RequestError) Unwrap() error { return e.error }

func newRequestError(err error) error {
	return &RequestError{err}
}

// IsRequestError returns true if err is a request error (bad
// request).
func IsRequestError(err error) bool {
	var r *RequestError
	return errors.As(err, &r)
}

type BackendError struct {
	error
}

func (e *BackendError) Unwrap() error { return e.error }

func newBackendError(err error) error {
	return &BackendError{err}
}

// IsBackendError returns true if err is a backend error.
func IsBackendError(err error) bool {
	var b *BackendError
	return errors.As(err, &b)
}

// ValidationError holds field-specific information that can be
// serialized as JSON if desired.
type ValidationError struct {
	fields map[string][]string
}

// IsValidationError returns true if err is a validation error.
func IsValidationError(err error) bool {
	var v *ValidationError
	return errors.As(err, &v)
}

// Initialize or extend a validationError.
func newValidationError(err *ValidationError, field, msg string) *ValidationError {
	if err == nil {
		err = &ValidationError{fields: make(map[string][]string)}
	}
	if field == "" {
		field = "_form"
	}
	err.fields[field] = append(err.fields[field], msg)
	return err
}

func (v *ValidationError) Error() string {
	var p []string
	if v.fields != nil {
		for f, l := range v.fields {
			p = append(p, fmt.Sprintf("%s: %s", f, strings.Join(l, ", ")))
		}
	} else {
		p = append(p, "INVALID ERROR")
	}
	return fmt.Sprintf("request validation error: %s", strings.Join(p, "; "))
}

func (v *ValidationError) JSON() []byte {
	data, _ := json.Marshal(v.fields) // nolint
	return data
}

// orNil solves the problem with nil-wrapping interfaces by returning
// an unwrapped nil if the validationError is nil.
func (v *ValidationError) orNil() error {
	if v == nil {
		return nil
	}
	return v
}
