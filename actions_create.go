package accountserver

import (
	"context"
	"errors"
	"fmt"
	"log"
	"strings"

	"git.autistici.org/ai3/go-common/pwhash"
)

// CreateResourcesRequest lets administrators create one or more resources.
type CreateResourcesRequest struct {
	AdminRequestBase

	// Username the resources will belong to (optional).
	Username string `json:"username"`

	// Resources to create. All must either be global resources
	// (no user ownership), or belong to the same user.
	Resources []*Resource `json:"resources"`
}

// PopulateContext extracts information from the request and stores it
// into the RequestContext.
func (r *CreateResourcesRequest) PopulateContext(rctx *RequestContext) error {
	if r.Username != "" {
		user, err := getUserOrDie(rctx.Context, rctx.TX, r.Username)
		if err != nil {
			return err
		}
		rctx.User = user
	}
	return r.AdminRequestBase.PopulateContext(rctx)
}

// CreateResourcesResponse is the response type for CreateResourcesRequest.
type CreateResourcesResponse struct {
	// Resources that were created.
	Resources []*Resource `json:"resources"`
}

// Merge two resource lists by ID, return a new list. If a resource is
// duplicated (detected by type/name matching), return an error.
func mergeResources(a, b []*Resource) ([]*Resource, error) {
	tmp := make(map[string]struct{})
	var out []*Resource
	for _, l := range [][]*Resource{a, b} {
		for _, r := range l {
			key := r.String()
			if _, seen := tmp[key]; seen {
				return nil, fmt.Errorf("resource %s already exists", key)
			}
			tmp[key] = struct{}{}
			out = append(out, r)
		}
	}
	return out, nil
}

// Validate the request.
func (r *CreateResourcesRequest) Validate(rctx *RequestContext) error {
	var tplUser *User

	if rctx.User != nil {
		// To provide resource validators with a view of what the User should be
		// with the new resources, we merge the ones from the database with the
		// ones from the request. This is also a good time to check for
		// uniqueness (even though it would fail at commit time anyway).
		merged, err := mergeResources(rctx.User.Resources, r.Resources)
		if err != nil {
			return err
		}
		userCopy := rctx.User.User
		userCopy.Resources = merged
		tplUser = &userCopy
	}

	for _, rsrc := range r.Resources {
		// Apply resource templates.
		if err := rctx.resourceTemplates.applyTemplate(rctx.Context, rsrc, tplUser); err != nil {
			return err
		}

		// Validate the resource.
		if err := rctx.resourceValidator.validateResource(rctx, rsrc, tplUser, true); err != nil {
			log.Printf("validation error while creating resource %s: %v", rsrc.String(), err)
			return err
		}
	}

	if tplUser != nil {
		// If the resource has an owner, validate it (checks that the new
		// resources do not violate user invariants).
		if err := checkUserInvariants(tplUser); err != nil {
			log.Printf("validation error while creating resources: %v", err)
			return newValidationError(nil, "user", err.Error())
		}
	}

	return nil
}

// Serve the request.
func (r *CreateResourcesRequest) Serve(rctx *RequestContext) (interface{}, error) {
	var user *User
	if rctx.User != nil {
		user = &rctx.User.User
	}
	rsrcs, err := rctx.TX.CreateResources(rctx.Context, user, r.Resources)
	if err != nil {
		return nil, err
	}
	for _, rsrc := range rsrcs {
		rctx.audit.Log(rctx, rsrc, "resource created")
	}
	return &CreateResourcesResponse{
		Resources: rsrcs,
	}, nil
}

// CreateUserRequest lets administrators create a new user along with the
// associated resources.
type CreateUserRequest struct {
	AdminRequestBase

	// User to create, along with associated resources.
	User *User `json:"user"`
}

// applyTemplate fills in default values for the resources in the request.
func (r *CreateUserRequest) applyTemplate(rctx *RequestContext) error {
	// Usernames must be lowercase.
	r.User.Name = strings.ToLower(r.User.Name)

	// Some fields should be always unset because there are
	// specific methods to modify them.
	r.User.Status = UserStatusActive
	r.User.Has2FA = false
	r.User.HasOTP = false
	r.User.HasEncryptionKeys = true // set to true so that resetPassword will create keys.
	r.User.AccountRecoveryHint = ""
	r.User.AppSpecificPasswords = nil
	r.User.U2FRegistrations = nil
	if r.User.Lang == "" {
		r.User.Lang = "en"
	}

	// Allocate a new user ID.
	uid, err := rctx.TX.NextUID(rctx.Context)
	if err != nil {
		return err
	}
	r.User.UID = uid

	// Apply templates to all resources in the request.
	for _, rsrc := range r.User.Resources {
		if err := rctx.resourceTemplates.applyTemplate(rctx.Context, rsrc, r.User); err != nil {
			return err
		}
		// Set the user shard to match the email resource shard.
		if rsrc.Type == ResourceTypeEmail {
			r.User.Shard = rsrc.Shard
		}
	}

	return nil
}

// Validate the request.
func (r *CreateUserRequest) Validate(rctx *RequestContext) error {
	if err := r.applyTemplate(rctx); err != nil {
		return err
	}

	// Validate the user *and* all resources.  The request must contain at
	// least one email resource with the same name as the user.
	for _, rsrc := range r.User.Resources {
		if err := rctx.resourceValidator.validateResource(rctx, rsrc, r.User, true); err != nil {
			log.Printf("validation error while creating resource %+v: %v", rsrc, err)
			return err
		}
	}
	if err := rctx.userValidator(rctx, r.User, true); err != nil {
		log.Printf("validation error while creating user %+v: %v", r.User, err)
		return err
	}

	return nil
}

// CreateUserResponse is the response type for CreateUserRequest.
type CreateUserResponse struct {
	User     *User  `json:"user"`
	Password string `json:"password"`
}

// Sanitize the response.
func (r *CreateUserResponse) Sanitize() {
	if r.Password != "" {
		r.Password = sanitizedValue
	}
}

// Serve the request
func (r *CreateUserRequest) Serve(rctx *RequestContext) (interface{}, error) {
	var resp CreateUserResponse

	// Create the user first, along with all the resources.
	user, err := rctx.TX.CreateUser(rctx.Context, r.User)
	if err != nil {
		return nil, err
	}
	resp.User = user

	// Now set a password for the user and return it, and
	// set random passwords for all the resources
	// (currently, we don't care about those, the user
	// will reset them later). However, we could return
	// them in the response as well, if necessary.
	u := &RawUser{User: *user}
	newPassword := randomPassword()
	if err := u.resetPassword(rctx.Context, rctx.TX, newPassword); err != nil {
		return nil, err
	}
	resp.Password = newPassword

	// Fake a RawUser in the RequestContext just for the purpose
	// of audit logging.
	rctx.User = u
	rctx.audit.Log(rctx, nil, "user created")

	for _, rsrc := range r.User.Resources {
		rctx.audit.Log(rctx, rsrc, "resource created")
		if resourceHasPassword(rsrc) {
			if _, err := doResetResourcePassword(rctx.Context, rctx.TX, rsrc); err != nil {
				// Just log, don't fail.
				log.Printf("can't set random password for resource %s: %v", rsrc.String(), err)
			}
		}
	}

	return &resp, nil
}

func resourceHasPassword(r *Resource) bool {
	switch r.Type {
	case ResourceTypeDAV, ResourceTypeDatabase, ResourceTypeMailingList:
		return true
	default:
		return false
	}
}

func doResetResourcePassword(ctx context.Context, tx TX, rsrc *Resource) (string, error) {
	newPassword := randomPassword()
	encPassword := pwhash.Encrypt(newPassword)

	// TODO: this needs a resource type-switch.
	if err := tx.SetResourcePassword(ctx, rsrc, encPassword); err != nil {
		return "", err
	}
	return newPassword, nil
}

// DisableUserRequest lets administrators or users themselves disable
// an entire account and all associated resources.
type DisableUserRequest struct {
	UserRequestBase
}

// Serve the request.
func (r *DisableUserRequest) Serve(rctx *RequestContext) (interface{}, error) {
	user := rctx.User
	if user.Status == UserStatusInactive {
		return nil, errors.New("user is already inactive")
	}

	// Disable all resources except for lists/newsletters, which
	// may have multiple ownership and in general have slightly
	// different "ownership" semantics than other resources.
	for _, rsrc := range user.Resources {
		if rsrc.Type == ResourceTypeMailingList || rsrc.Type == ResourceTypeNewsletter {
			continue
		}
		if rsrc.Status == ResourceStatusActive || rsrc.Status == ResourceStatusReadonly {
			rsrc.Status = ResourceStatusInactive
			if err := rctx.TX.UpdateResource(rctx.Context, rsrc); err != nil {
				return nil, err
			}
		}
	}
	// Disable the user.
	user.Status = UserStatusInactive
	if err := rctx.TX.UpdateUser(rctx.Context, &user.User); err != nil {
		return nil, err
	}

	rctx.audit.Log(rctx, nil, "user disabled")

	return nil, nil
}
