package integrationtest

import (
	"bytes"
	"context"
	"encoding/json"
	"errors"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"net/http/httptest"
	"os"
	"path/filepath"
	"testing"
	"time"

	as "git.autistici.org/ai3/accountserver"
	cachebackend "git.autistici.org/ai3/accountserver/backend/cache"
	ldapbackend "git.autistici.org/ai3/accountserver/backend/ldap"
	"git.autistici.org/ai3/accountserver/ldaptest"
	"git.autistici.org/ai3/accountserver/server"
	ct "git.autistici.org/ai3/go-common/ldap/compositetypes"
	"git.autistici.org/ai3/go-common/pwhash"
	"git.autistici.org/ai3/go-common/userenckey"
	"git.autistici.org/id/auth/lineproto"
	authserver "git.autistici.org/id/auth/server"
	"git.autistici.org/id/go-sso"
	"golang.org/x/crypto/ed25519"
)

const (
	testLDAPPort = 42872
	testLDAPAddr = "ldap://127.0.0.1:42872"

	testSSODomain  = "domain"
	testSSOService = "accountserver.domain/"
	testAdminUser  = "admin"
	testAdminGroup = "admins"
)

func withSSO(t testing.TB) (func(), sso.Signer, string) {
	tmpf, err := ioutil.TempFile("", "")
	if err != nil {
		t.Fatal(err)
	}

	pub, priv, err := ed25519.GenerateKey(nil)
	if err != nil {
		t.Fatal(err)
	}
	tmpf.Write(pub) // nolint
	tmpf.Close()

	signer, err := sso.NewSigner(priv)
	if err != nil {
		t.Fatal(err)
	}

	return func() {
		os.Remove(tmpf.Name())
	}, signer, tmpf.Name()
}

// Start a real auth-server, talking to our test LDAP server.
func withAuthServer(t testing.TB, configFiles map[string]string) (func(), string) {
	dir, err := ioutil.TempDir("", "")
	if err != nil {
		t.Fatal(err)
	}
	socketPath := filepath.Join(dir, "auth-socket")

	for path, data := range configFiles {
		os.MkdirAll(filepath.Dir(filepath.Join(dir, path)), 0700)
		ioutil.WriteFile(filepath.Join(dir, path), []byte(data), 0600)
	}

	config, err := authserver.LoadConfig(filepath.Join(dir, "config.yml"))
	if err != nil {
		t.Fatal("authserver.LoadConfig()", err)
	}
	authSrv, err := authserver.NewServer(config)
	if err != nil {
		t.Fatal("authserver.NewServer()", err)
	}
	l, err := lineproto.NewUNIXSocketListener(socketPath)
	if err != nil {
		t.Fatal("lineproto.NewUNIXSocketListener()", err)
	}
	sockSrv := lineproto.NewServer("unix", l, lineproto.NewLineServer(authserver.NewSocketServer(authSrv)))

	go func() {
		sockSrv.Serve() // nolint: errcheck
	}()

	return func() {
		sockSrv.Close()
		os.RemoveAll(dir)
	}, socketPath
}

type testClient struct {
	srvURL string
	signer sso.Signer
}

func (c *testClient) ssoTicket(username string, groups ...string) string {
	if len(groups) == 0 && username == testAdminUser {
		groups = append(groups, testAdminGroup)
	}
	signed, err := c.signer.Sign(sso.NewTicket(username, testSSOService, testSSODomain, "", groups, 1*time.Hour))
	if err != nil {
		panic(err)
	}
	return signed
}

func (c *testClient) request(uri string, req, out interface{}) error {
	data, _ := json.Marshal(req)
	resp, err := http.Post(c.srvURL+uri, "application/json", bytes.NewReader(data))
	if err != nil {
		return err
	}
	defer resp.Body.Close()
	data, _ = ioutil.ReadAll(resp.Body)

	if resp.StatusCode >= 400 && resp.StatusCode < 500 {
		log.Printf("request error: %s", string(data))
		return errors.New(string(data))
	}
	if resp.StatusCode != 200 {
		log.Printf("remote error: %s", string(data))
		return fmt.Errorf("http status code %d", resp.StatusCode)
	}
	if resp.Header.Get("Content-Type") != "application/json" {
		return fmt.Errorf("unexpected content-type %s", resp.Header.Get("Content-Type"))
	}

	if out == nil {
		return nil
	}
	return json.Unmarshal(data, out)
}

func startServiceWithConfigAndCache(t testing.TB, svcConfig as.Config, enableCache bool) (func(), as.Backend, *testClient) {
	stop := ldaptest.StartServer(t, &ldaptest.Config{
		Dir:  "../ldaptest",
		Port: testLDAPPort,
		Base: "dc=example,dc=com",
		LDIFs: []string{
			"testdata/base.ldif",
			"testdata/test1.ldif",
			"testdata/test2.ldif",
			"testdata/test3.ldif",
		},
	})

	be, err := ldapbackend.NewLDAPBackend(testLDAPAddr, "cn=manager,dc=example,dc=com", "password", "dc=example,dc=com")
	if err != nil {
		t.Fatal("NewLDAPBackend", err)
	}
	if enableCache {
		be, err = cachebackend.Wrap(be, nil, nil)
		if err != nil {
			t.Fatal("cachebackend.Wrap", err)
		}
	}

	ssoStop, signer, ssoPubKeyFile := withSSO(t)

	authCleanup, authSocketPath := withAuthServer(t, map[string]string{
		"config.yml": `---
backends:
  ldap:
      uri: "ldap://127.0.0.1:42872"
      bind_dn: "cn=manager,dc=example,dc=com"
      bind_pw: "password"
services:
  accountserver:
      ignore_2fa: true
      backends:
        - backend: ldap
          params:
            search_base: "uid=%s,ou=People,dc=example,dc=com"
            search_filter: "(status=active)"
            scope: "base"
  accountserver-recovery:
      ignore_2fa: true
      backends:
        - backend: ldap
          params:
            search_base: "uid=%s,ou=People,dc=example,dc=com"
            search_filter: "(status=active)"
            scope: "base"
            attrs:
              password: recoverAnswer
`,
	})

	svcConfig.AuthSocket = authSocketPath
	svcConfig.SSO.PublicKeyFile = ssoPubKeyFile
	svcConfig.SSO.Domain = testSSODomain
	svcConfig.SSO.Service = testSSOService
	svcConfig.SSO.AdminGroup = testAdminGroup
	svcConfig.Validation.ForbiddenUsernames = []string{"forbidden"}
	svcConfig.Validation.AvailableDomains = map[string][]string{
		as.ResourceTypeEmail:       []string{"example.com"},
		as.ResourceTypeMailingList: []string{"example.com"},
	}
	svcConfig.Validation.WebsiteRootDir = "/home/users/investici.org"
	shards := []string{"host1", "host2", "host3"}
	svcConfig.Shards.Available = map[string][]string{
		as.ResourceTypeEmail:       shards,
		as.ResourceTypeMailingList: shards,
		as.ResourceTypeWebsite:     shards,
		as.ResourceTypeDomain:      shards,
		as.ResourceTypeDAV:         shards,
		as.ResourceTypeDatabase:    shards,
	}
	svcConfig.Shards.Allowed = svcConfig.Shards.Available

	service, err := as.NewAccountService(be, &svcConfig)
	if err != nil {
		stop()
		t.Fatal("NewAccountService", err)
	}

	as, err := server.New(service, be, "", nil)
	if err != nil {
		t.Fatal("server.New", err)
	}
	srv := httptest.NewServer(as)

	c := &testClient{
		srvURL: srv.URL,
		signer: signer,
	}

	return func() {
		stop()
		srv.Close()
		authCleanup()
		ssoStop()
	}, be, c
}

func startServiceWithConfig(t testing.TB, svcConfig as.Config) (func(), as.Backend, *testClient) {
	return startServiceWithConfigAndCache(t, svcConfig, false)
}

func startService(t testing.TB) (func(), as.Backend, *testClient) {
	return startServiceWithConfig(t, as.Config{})
}

// Verify that some user authentication invariants are true. Returns
// the RawUser for further checks.
func checkUserInvariants(t *testing.T, be as.Backend, username, primaryPassword string) *as.RawUser {
	tx, _ := be.NewTransaction()
	user, err := tx.GetUser(context.Background(), username)
	if err != nil {
		t.Fatalf("GetUser(%s): %v", username, err)
	}

	// Verify that the password is correct.
	if !pwhash.ComparePassword(user.Password, primaryPassword) {
		t.Fatalf("password for user %s is not %s", username, primaryPassword)
	}

	// Verify that we can successfully encrypt keys.
	if user.HasEncryptionKeys {
		if _, err := userenckey.Decrypt(keysToBytes(user.Keys), []byte(primaryPassword)); err != nil {
			t.Fatalf("password for user %s can't decrypt keys", username)
		}
	}

	// Verify that the user shard matches the email resource shard.
	email := user.GetSingleResourceByType(as.ResourceTypeEmail)
	if email == nil {
		t.Fatalf("no email resources for user %s", username)
	}
	if user.Shard != email.Shard {
		t.Fatalf("user and email shards differ ('%s' vs '%s')", user.Shard, email.Shard)
	}

	return user
}

func keysToBytes(keys []*ct.EncryptedKey) [][]byte {
	var rawKeys [][]byte
	for _, k := range keys {
		rawKeys = append(rawKeys, k.EncryptedKey)
	}
	return rawKeys
}
