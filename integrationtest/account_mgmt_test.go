package integrationtest

import (
	"testing"

	as "git.autistici.org/ai3/accountserver"
)

// Verify that authentication on GetUser works as expected:
// - users can fetch their own data but not other users'
// - admins can read everything.
func TestIntegration_GetUser_Auth(t *testing.T) {
	stop, _, c := startService(t)
	defer stop()

	testdata := []struct {
		authUser   string
		authGroup  string
		expectedOk bool
	}{
		{"uno@investici.org", "", true},
		{"uno@investici.org", "users", true},
		{"due@investici.org", "users", false},
		{testAdminUser, testAdminGroup, true},
	}

	for _, td := range testdata {
		var user as.User
		var groups []string
		if td.authGroup != "" {
			groups = append(groups, td.authGroup)
		}
		err := c.request("/api/user/get", &as.GetUserRequest{
			UserRequestBase: as.UserRequestBase{
				RequestBase: as.RequestBase{
					SSO: c.ssoTicket(td.authUser, groups...),
				},
				Username: "uno@investici.org",
			},
		}, &user)
		if td.expectedOk && err != nil {
			t.Errorf("access error for user %s: expected ok, got error: %v", td.authUser, err)
		} else if !td.expectedOk && err == nil {
			t.Errorf("access error for user %s: expected error, got ok", td.authUser)
		}
	}
}

// Verify that a user can't change someone else's password.
func TestIntegration_ChangeUserPassword_AuthFail(t *testing.T) {
	stop, _, c := startService(t)
	defer stop()

	err := c.request("/api/user/change_password", &as.ChangeUserPasswordRequest{
		PrivilegedRequestBase: as.PrivilegedRequestBase{
			UserRequestBase: as.UserRequestBase{
				RequestBase: as.RequestBase{
					SSO: c.ssoTicket("due@investici.org"),
				},
				Username: "uno@investici.org",
			},
			CurPassword: "password",
		},
		Password: "new_password",
	}, nil)

	if err == nil {
		t.Fatal("ChangePassword for another user succeeded")
	}
}

// Verify various attempts at changing the password (user has no encryption keys).
func TestIntegration_ChangeUserPassword(t *testing.T) {
	runChangeUserPasswordTest(t, "uno@investici.org", as.Config{})
}

// Verify various attempts at changing the password (user has no encryption keys).
func TestIntegration_ChangeUserPassword_WithOpportunisticEncryption(t *testing.T) {
	user := runChangeUserPasswordTest(t, "uno@investici.org", as.Config{
		EnableOpportunisticEncryption: true,
	})
	if !user.HasEncryptionKeys {
		t.Fatal("encryption keys were not created on password change")
	}
}

// Verify various attempts at changing the password (user with encryption keys).
func TestIntegration_ChangeUserPassword_WithEncryptionKeys(t *testing.T) {
	runChangeUserPasswordTest(t, "due@investici.org", as.Config{})
}

func runChangeUserPasswordTest(t *testing.T, username string, cfg as.Config) *as.RawUser {
	stop, be, c := startServiceWithConfig(t, cfg)
	defer stop()

	testdata := []struct {
		password    string
		newPassword string
		expectedOk  bool
	}{
		// Ordering is important as it is meant to emulate
		// setting the password, failing to reset it, then
		// succeeding.
		{"password", "new_password", true},
		{"BADPASS", "new_password_2", false},
		{"new_password", "password", true},
	}
	for _, td := range testdata {
		err := c.request("/api/user/change_password", &as.ChangeUserPasswordRequest{
			PrivilegedRequestBase: as.PrivilegedRequestBase{
				UserRequestBase: as.UserRequestBase{
					RequestBase: as.RequestBase{
						SSO: c.ssoTicket(username),
					},
					Username: username,
				},
				CurPassword: td.password,
			},
			Password: td.newPassword,
		}, nil)
		if err == nil && !td.expectedOk {
			t.Fatalf("ChangeUserPassword(old=%s new=%s) should have failed but didn't", td.password, td.newPassword)
		} else if err != nil && td.expectedOk {
			t.Fatalf("ChangeUserPassword(old=%s new=%s) failed: %v", td.password, td.newPassword, err)
		}
	}

	// The password that should work at the end of the above
	// series of checks is still "password".
	return checkUserInvariants(t, be, username, "password")
}

func TestIntegration_AccountRecovery(t *testing.T) {
	runAccountRecoveryTest(t, "uno@investici.org", false, false)
}

func TestIntegration_AccountRecovery_WithEncryptionKeys(t *testing.T) {
	user := runAccountRecoveryTest(t, "due@investici.org", false, false)
	if !user.HasEncryptionKeys {
		t.Fatal("encryption keys not enabled after account recovery")
	}
}

func TestIntegration_AccountRecovery_WithOpportunisticEncryption(t *testing.T) {
	user := runAccountRecoveryTest(t, "uno@investici.org", false, true)
	if !user.HasEncryptionKeys {
		t.Fatal("encryption keys not enabled after account recovery")
	}
}

func TestIntegration_AccountRecovery_WithCache(t *testing.T) {
	runAccountRecoveryTest(t, "uno@investici.org", true, false)
}

func TestIntegration_AccountRecovery_WithEncryptionKeysAndCache(t *testing.T) {
	user := runAccountRecoveryTest(t, "due@investici.org", true, false)
	if !user.HasEncryptionKeys {
		t.Fatal("encryption keys not enabled after account recovery")
	}
}

func TestIntegration_AccountRecovery_ClearsAppSpecificPasswords(t *testing.T) {
	user := runAccountRecoveryTest(t, "tre@investici.org", false, false)
	if len(user.AppSpecificPasswords) > 0 {
		t.Fatal("app-specific passwords were not cleared after account recovery")
	}
}

func runAccountRecoveryTest(t *testing.T, username string, enableCache, enableOpportunisticEncryption bool) *as.RawUser {
	cfg := as.Config{
		EnableOpportunisticEncryption: enableOpportunisticEncryption,
	}
	stop, be, c := startServiceWithConfig(t, cfg)
	defer stop()

	hint := "secret code?"
	secondaryPw := "open sesame!"
	err := c.request("/api/user/set_account_recovery_hint", &as.SetAccountRecoveryHintRequest{
		PrivilegedRequestBase: as.PrivilegedRequestBase{
			UserRequestBase: as.UserRequestBase{
				RequestBase: as.RequestBase{
					SSO: c.ssoTicket(username),
				},
				Username: username,
			},
			CurPassword: "password",
		},
		Hint:     hint,
		Response: secondaryPw,
	}, nil)
	if err != nil {
		t.Fatalf("SetAccountRecoveryHint failed: %v", err)
	}

	// The first request just fetches the recovery hint.
	var resp as.AccountRecoveryResponse
	err = c.request("/api/recover_account", &as.AccountRecoveryRequest{
		Username: username,
	}, &resp)
	if err != nil {
		t.Fatalf("AccountRecovery (hint only) failed: %v", err)
	}
	if resp.Hint != hint {
		t.Fatalf("bad AccountRecovery hint, got '%s' expected '%s'", resp.Hint, hint)
	}

	// Now recover the account and set a new password.
	newPw := "new password"
	err = c.request("/api/recover_account", &as.AccountRecoveryRequest{
		Username:         username,
		RecoveryPassword: secondaryPw,
		Password:         newPw,
	}, &resp)
	if err != nil {
		t.Fatalf("AccountRecovery failed: %v", err)
	}

	return checkUserInvariants(t, be, username, newPw)
}

func TestIntegration_AppSpecificPassword(t *testing.T) {
	stop, _, c := startService(t)
	defer stop()

	username := "tre@investici.org"

	var resp as.CreateApplicationSpecificPasswordResponse
	err := c.request("/api/user/create_app_specific_password", &as.CreateApplicationSpecificPasswordRequest{
		PrivilegedRequestBase: as.PrivilegedRequestBase{
			UserRequestBase: as.UserRequestBase{
				RequestBase: as.RequestBase{
					SSO: c.ssoTicket(username),
				},
				Username: username,
			},
			CurPassword: "password",
		},
		Service: "service",
		Notes:   "notes",
	}, &resp)
	if err != nil {
		t.Fatalf("CreateApplicationSpecificPassword failed: %v", err)
	}

	var user as.User
	err = c.request("/api/user/get", &as.GetUserRequest{
		UserRequestBase: as.UserRequestBase{
			RequestBase: as.RequestBase{
				SSO: c.ssoTicket(testAdminUser, testAdminGroup),
			},
			Username: username,
		},
	}, &user)
	if err != nil {
		t.Fatalf("GetUser error: %v", err)
	}
	found := false
	for _, asp := range user.AppSpecificPasswords {
		if asp.Service == "service" {
			found = true
			break
		}
	}
	if !found {
		t.Errorf("could not find the ASPs that was just created: %+v", user)
	}
}
