package integrationtest

import (
	"testing"

	as "git.autistici.org/ai3/accountserver"
)

func TestIntegration_AddEmailAlias(t *testing.T) {
	stop, _, c := startService(t)
	defer stop()

	// The following are basically checks for email validation.
	testdata := []struct {
		addr       string
		expectedOk bool
	}{
		{"alias@example.com", false},     // already taken
		{"alias@otherdomain.com", false}, // bad domain
		{"x@example.com", false},         // too short
		{"........@example.com", false},  // malformed
		{"due@investici.org", false},     // already taken
		{"forbidden@example.com", false}, // reserved
		{"alias1@example.com", true},
		{"alias2@example.com", true},
		{"alias3@example.com", true},
		{"alias4@example.com", true},
		{"alias5@example.com", false}, // limit of 5 aliases reached
	}

	// Cheat a bit, we know the DN.
	rsrcID := as.ResourceID("mail=uno@investici.org,uid=uno@investici.org,ou=People,dc=example,dc=com")

	for _, td := range testdata {
		err := c.request("/api/resource/email/add_alias", &as.AddEmailAliasRequest{
			ResourceRequestBase: as.ResourceRequestBase{
				RequestBase: as.RequestBase{
					SSO: c.ssoTicket("uno@investici.org"),
				},
				ResourceID: rsrcID,
			},
			Addr: td.addr,
		}, nil)
		if err == nil && !td.expectedOk {
			t.Errorf("AddEmailAlias(%s) should have failed but didn't", td.addr)
		} else if err != nil && td.expectedOk {
			t.Errorf("AddEmailAlias(%s) failed: %v", td.addr, err)
		}
	}
}
