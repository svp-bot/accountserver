// Package integrationtest runs a test suite on the accountserver with
// a real LDAP database, using the HTTP API.
//
package integrationtest
