package integrationtest

import (
	"log"
	"testing"

	as "git.autistici.org/ai3/accountserver"
)

func TestIntegration_CreateUser(t *testing.T) {
	stop, be, c := startService(t)
	defer stop()

	testdata := []struct {
		name       string
		user       *as.User
		expectedOk bool
	}{
		{
			// Bare user creation request, most values will be filled
			// in by the server using resource templates.
			"email_only",
			&as.User{
				Name: "newuser1@example.com",
				Resources: []*as.Resource{
					&as.Resource{
						Type: as.ResourceTypeEmail,
						Name: "newuser1@example.com",
					},
				},
			},
			true,
		},

		{
			// Valid request but already existing user.
			"email_only_dup",
			&as.User{
				Name: "newuser1@example.com",
				Resources: []*as.Resource{
					&as.Resource{
						Type: as.ResourceTypeEmail,
						Name: "newuser1@example.com",
					},
				},
			},
			false,
		},

		{
			// Valid request but already existing user (case-folded).
			"email_only_dup_case_folded",
			&as.User{
				Name: "NEWUSER1@example.com",
				Resources: []*as.Resource{
					&as.Resource{
						Type: as.ResourceTypeEmail,
						Name: "NEWUSER1@example.com",
					},
				},
			},
			false,
		},

		{
			// User creation request without any resources at all.
			"no_resources",
			&as.User{
				Name: "newuser2@example.com",
			},
			false,
		},

		{
			// User creation request without an email account.
			"no_email",
			&as.User{
				Name: "newuser3@example.com",
				Resources: []*as.Resource{
					&as.Resource{
						Type: as.ResourceTypeDAV,
						Name: "davuser3",
					},
				},
			},
			false,
		},

		{
			// Invalid request: the email name does not
			// match the user name.
			"email_user_mismatch",
			&as.User{
				Name: "newuser4@example.com",
				Resources: []*as.Resource{
					&as.Resource{
						Type: as.ResourceTypeEmail,
						Name: "newuser7@example.com",
					},
				},
			},
			false,
		},
	}

	for _, td := range testdata {
		log.Printf("running test %s", td.name)

		var resp as.CreateUserResponse
		err := c.request("/api/user/create", &as.CreateUserRequest{
			AdminRequestBase: as.AdminRequestBase{
				RequestBase: as.RequestBase{
					SSO: c.ssoTicket(testAdminUser),
				},
			},
			User: td.user,
		}, &resp)

		if err == nil && !td.expectedOk {
			t.Errorf("'%s' should have failed but didn't", td.name)
		} else if err != nil && td.expectedOk {
			t.Errorf("'%s' failed: %v", td.name, err)
		} else if td.expectedOk {
			if resp.Password == "" {
				t.Fatalf("no password in response (%v)", resp)
			}

			// Verify that the new password works.
			checkUserInvariants(t, be, td.user.Name, resp.Password)
		}
	}
}
