package integrationtest

import (
	"testing"

	as "git.autistici.org/ai3/accountserver"
)

func TestIntegration_MoveResource(t *testing.T) {
	stop, be, c := startService(t)
	defer stop()

	var resp as.MoveResourceResponse
	err := c.request("/api/resource/move", &as.MoveResourceRequest{
		AdminResourceRequestBase: as.AdminResourceRequestBase{
			ResourceRequestBase: as.ResourceRequestBase{
				RequestBase: as.RequestBase{
					SSO: c.ssoTicket(testAdminUser),
				},
				ResourceID: as.ResourceID("mail=uno@investici.org,uid=uno@investici.org,ou=People,dc=example,dc=com"),
			},
		},
		Shard: "host1",
	}, &resp)
	if err != nil {
		t.Fatalf("MoveResource: %v", err)
	}

	checkUserInvariants(t, be, "uno@investici.org", "password")
}
