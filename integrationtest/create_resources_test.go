package integrationtest

import (
	"testing"

	as "git.autistici.org/ai3/accountserver"
)

func TestIntegration_CreateResources(t *testing.T) {
	stop, _, c := startService(t)
	defer stop()

	testdata := []struct {
		resource   *as.Resource
		expectedOk bool
	}{
		// Create a domain resource.
		{
			&as.Resource{
				Name:          "example2.com",
				Type:          as.ResourceTypeDomain,
				Status:        as.ResourceStatusActive,
				Shard:         "host2",
				OriginalShard: "host2",
				Website: &as.Website{
					URL:          "https://example2.com",
					DocumentRoot: "/home/users/investici.org/uno/html-example2.com",
					AcceptMail:   true,
				},
			},
			true,
		},

		// Duplicate of the above request, should fail due to conflict.
		{
			&as.Resource{
				Type:          as.ResourceTypeDomain,
				Name:          "example2.com",
				Status:        as.ResourceStatusActive,
				Shard:         "host2",
				OriginalShard: "host2",
				Website: &as.Website{
					URL:          "https://example2.com",
					DocumentRoot: "/home/users/investici.org/uno/html-example2.com",
				},
			},
			false,
		},

		// Empty document root will be fixed by templating.
		{
			&as.Resource{
				Type:          as.ResourceTypeDomain,
				Name:          "example3.com",
				Status:        as.ResourceStatusActive,
				Shard:         "host2",
				OriginalShard: "host2",
				Website:       &as.Website{},
			},
			true,
		},

		// Malformed resource metadata (name fails validation).
		{
			&as.Resource{
				Type:          as.ResourceTypeDomain,
				Name:          "example$.com",
				Status:        as.ResourceStatusActive,
				Shard:         "host2",
				OriginalShard: "host2",
				Website: &as.Website{
					URL:          "https://example$.com",
					DocumentRoot: "/home/users/investici.org/uno/html-example3.com",
				},
			},
			false,
		},

		// Bad shard.
		{
			&as.Resource{
				Type:          as.ResourceTypeDomain,
				Name:          "example4.com",
				Status:        as.ResourceStatusActive,
				Shard:         "zebra",
				OriginalShard: "zebra",
				Website: &as.Website{
					URL:          "https://example4.com",
					DocumentRoot: "/home/users/investici.org/uno/html-example4.com",
				},
			},
			false,
		},

		// The document root has no associated DAV account.
		{
			&as.Resource{
				Type:          as.ResourceTypeDomain,
				Name:          "example5.com",
				Status:        as.ResourceStatusActive,
				Shard:         "host2",
				OriginalShard: "host2",
				Website: &as.Website{
					URL:          "https://example5.com",
					DocumentRoot: "/home/users/investici.org/nonexisting",
				},
			},
			false,
		},

		// Create a database resource (nested below a website).
		{
			&as.Resource{
				Name:     "unodb2",
				Type:     as.ResourceTypeDatabase,
				ParentID: "alias=uno,uid=uno@investici.org,ou=People,dc=example,dc=com",
			},
			true,
		},
	}

	for _, td := range testdata {
		var resp as.CreateResourcesResponse
		err := c.request("/api/resource/create", &as.CreateResourcesRequest{
			AdminRequestBase: as.AdminRequestBase{
				RequestBase: as.RequestBase{
					SSO: c.ssoTicket(testAdminUser),
				},
			},
			Username:  "uno@investici.org",
			Resources: []*as.Resource{td.resource},
		}, &resp)
		if err == nil && !td.expectedOk {
			t.Errorf("CreateResource(%s) should have failed but didn't", td.resource.Name)
		} else if err != nil && td.expectedOk {
			t.Errorf("CreateResource(%s) failed: %v", td.resource.Name, err)
		}

		// Validate the response.
		if err == nil {
			if len(resp.Resources) < 1 {
				t.Errorf("CreateResource(%s): no resources in response", td.resource.Name)
				continue
			}
			if resp.Resources[0].ID == "" {
				t.Errorf("CreateResources(%s): resource ID unset in response", td.resource.Name)
			}
		}
	}
}

func TestIntegration_CreateMultipleResources(t *testing.T) {
	stop, _, c := startService(t)
	defer stop()

	testdata := []struct {
		name       string
		username   string
		resources  []*as.Resource
		expectedOk bool
	}{
		{
			// The create request is very bare, most values will be
			// filled in by the server using resource
			// templates. Note that we specify client-side IDs and
			// use them in ParentIDs to specify nesting.
			"site_with_db",
			"uno@investici.org",
			[]*as.Resource{
				&as.Resource{
					ID:   as.ResourceID("example3_site"),
					Type: as.ResourceTypeDomain,
					Name: "example3.com",
				},
				&as.Resource{
					Type: as.ResourceTypeDAV,
					Name: "example3dav",
				},
				&as.Resource{
					Type:     as.ResourceTypeDatabase,
					ParentID: as.ResourceID("example3_site"),
					Name:     "example3",
				},
			},
			true,
		},

		{
			// Same as above, but without an associated user.
			"site_with_db_no_user",
			"",
			[]*as.Resource{
				&as.Resource{
					ID:   as.ResourceID("example3_site"),
					Type: as.ResourceTypeDomain,
					Name: "example3.com",
				},
				&as.Resource{
					Type: as.ResourceTypeDAV,
					Name: "example3dav",
				},
				&as.Resource{
					Type:     as.ResourceTypeDatabase,
					ParentID: as.ResourceID("example3_site"),
					Name:     "example3",
				},
			},
			false,
		},

		{
			// An attempt to create resources without client-side
			// IDs. It should fail the database validation.
			"site_with_db_no_nesting",
			"uno@investici.org",
			[]*as.Resource{
				&as.Resource{
					Type: as.ResourceTypeDomain,
					Name: "example4.com",
				},
				&as.Resource{
					Type: as.ResourceTypeDAV,
					Name: "example4dav",
				},
				&as.Resource{
					Type: as.ResourceTypeDatabase,
					Name: "example4",
				},
			},
			false,
		},

		{
			// An attempt to create a website without an associated
			// DAV account, it should pick up the DAV account that
			// already exists for user #1.
			"site_with_db_no_dav_picks_up_existing_account",
			"uno@investici.org",
			[]*as.Resource{
				&as.Resource{
					ID:   as.ResourceID("example5_site"),
					Type: as.ResourceTypeDomain,
					Name: "example5.com",
				},
				&as.Resource{
					ParentID: as.ResourceID("example5_site"),
					Type:     as.ResourceTypeDatabase,
					Name:     "example5",
				},
			},
			true,
		},

		{
			// An attempt to create a website without an associated
			// DAV account, for a user without an existing DAV
			// account. Should fail.
			"site_with_db_no_dav_and_no_existing_account",
			"tre@investici.org",
			[]*as.Resource{
				&as.Resource{
					ID:   as.ResourceID("example6_site"),
					Type: as.ResourceTypeDomain,
					Name: "example6.com",
				},
				&as.Resource{
					ParentID: as.ResourceID("example6_site"),
					Type:     as.ResourceTypeDatabase,
					Name:     "example6",
				},
			},
			false,
		},

		{
			// Resource without an owner.
			"list",
			"",
			[]*as.Resource{
				&as.Resource{
					Type: as.ResourceTypeMailingList,
					Name: "list1@example.com",
					List: &as.MailingList{
						Admins: []string{"uno@investici.org"},
					},
				},
			},
			true,
		},
	}

	for _, td := range testdata {
		req := &as.CreateResourcesRequest{
			AdminRequestBase: as.AdminRequestBase{
				RequestBase: as.RequestBase{
					SSO: c.ssoTicket(testAdminUser),
				},
			},
			Username:  td.username,
			Resources: td.resources,
		}
		err := c.request("/api/resource/create", req, nil)
		if err == nil && !td.expectedOk {
			t.Errorf("'%s' should have failed but didn't", td.name)
		} else if err != nil && td.expectedOk {
			t.Errorf("'%s' failed: %v", td.name, err)
		}
	}
}
