package integrationtest

import (
	"testing"

	as "git.autistici.org/ai3/accountserver"
)

func TestIntegration_SearchIsAdminPrivileged(t *testing.T) {
	stop, _, c := startService(t)
	defer stop()

	var resp as.SearchUserResponse
	if err := c.request("/api/user/search", &as.SearchUserRequest{
		AdminRequestBase: as.AdminRequestBase{
			RequestBase: as.RequestBase{
				SSO: c.ssoTicket("uno@investici.org"),
			},
		},
		Pattern: "uno@investici.org",
	}, &resp); err == nil {
		t.Fatal("no error for user-privileged SearchUser!")
	}
}

func TestIntegration_SearchUser(t *testing.T) {
	stop, _, c := startService(t)
	defer stop()

	var resp as.SearchUserResponse
	if err := c.request("/api/user/search", &as.SearchUserRequest{
		AdminRequestBase: as.AdminRequestBase{
			RequestBase: as.RequestBase{
				SSO: c.ssoTicket(testAdminUser, testAdminGroup),
			},
		},
		Pattern: "uno@investici.org",
	}, &resp); err != nil {
		t.Fatal(err)
	}
	if len(resp.Usernames) != 1 {
		t.Fatalf("expected 1 result, got %d", len(resp.Usernames))
	}
	if resp.Usernames[0] != "uno@investici.org" {
		t.Fatalf("got bad username '%s', expected uno@investici.org", resp.Usernames[0])
	}
}

func TestIntegration_SearchResource(t *testing.T) {
	stop, _, c := startService(t)
	defer stop()

	var resp as.SearchResourceResponse
	if err := c.request("/api/resource/search", &as.SearchResourceRequest{
		AdminRequestBase: as.AdminRequestBase{
			RequestBase: as.RequestBase{
				SSO: c.ssoTicket(testAdminUser, testAdminGroup),
			},
		},
		Pattern: "due*",
	}, &resp); err != nil {
		t.Fatal(err)
	}
	if len(resp.Results) != 4 {
		t.Fatalf("expected 4 results, got %d", len(resp.Results))
	}
}
