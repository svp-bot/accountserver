A few notes on the data model
======

The data model used by the accountserver tries to be straightforward
and simple, but there are numerous aspects about the assumed semantics
that require further discussion.

At its core, it represents *users* and their associated
*resources*. Resources can represent accounts on services of different
type, and can be nested if there is a direct and relevant hierarchical
relation.

## Authentication

The major issue with the LDAP backend is that it's not easy to do
joins (you need to do two separate queries, and most open source
clients aren't even coded to do that). This makes it hard to go, for
example, from the email resource object to the main user account
object.

The issue of authentication is made complex by the choice of specific
services and desired UX that we have adopted. Specifically, the
decision of having the e-mail coincide with the primary user identity
(along with the availability of web-based single sign-on), and the UX
decision of presenting a single "authentication control surface" to
the user (so that, for instance, you don't have to separately set up
2FA for the main user account and your email) determine the need to
unify authentication details for the main user object and the email
resource(s).

In practice, this means that the API offers its authentication-related
methods on the user object rather than on the email resource. While
the implementation on a SQL backend would be straightforward
(credentials would simply be fields, or subtables, of the *user*
table), in the LDAP backend we are going to rely on denormalization
(i.e. copying the same attribute to multiple objects) and careful
splitting of some attributes to where they are used. For an example of
the latter, consider 2FA, where the split between interactive and
non-interactive clients allows us to:

* put the TOTP secret on the user object, because that's used by the
  interactive authentication service (the single sign-on login server
  application);
* put application-specific passwords (used by non-interactive clients)
  on the email resource LDAP objects, which is where the email service
  authenticates its users. Same for the storage encryption keys.
