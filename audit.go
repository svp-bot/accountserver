package accountserver

import (
	"bytes"
	"encoding/json"
	"io"
	"log"
	"log/syslog"
)

// A fixed log_type makes it possible to limit searches to this
// specific category of logs.
const auditLogType = "account_audit"

type auditLogger interface {
	Log(*RequestContext, *Resource, string)
}

type auditLogEntry struct {
	LogType      string `json:"log_type"`
	User         string `json:"user,omitempty"`
	By           string `json:"by"`
	Message      string `json:"message"`
	Comment      string `json:"comment,omitempty"`
	ResourceName string `json:"resource_name,omitempty"`
	ResourceType string `json:"resource_type,omitempty"`
}

func buildAuditMessage(rctx *RequestContext, rsrc *Resource, what string) []byte {
	e := auditLogEntry{
		LogType: auditLogType,
		Message: what,
		Comment: rctx.Comment,
		By:      rctx.Auth.Username,
	}
	if rctx.User != nil {
		e.User = rctx.User.Name
	}

	// Fall back to resource from context if unspecified.
	if rsrc == nil && rctx.Resource != nil {
		rsrc = rctx.Resource
	}
	if rsrc != nil {
		e.ResourceName = rsrc.Name
		e.ResourceType = rsrc.Type
	}

	var buf bytes.Buffer
	if _, err := io.WriteString(&buf, "@cee:"); err != nil {
		return nil
	}
	if err := json.NewEncoder(&buf).Encode(&e); err != nil {
		return nil
	}
	return buf.Bytes()
}

type stderrAuditLogger struct{}

func newStderrAuditLogger() auditLogger {
	return &stderrAuditLogger{}
}

func (l *stderrAuditLogger) Log(rctx *RequestContext, rsrc *Resource, what string) {
	b := buildAuditMessage(rctx, rsrc, what)
	if b == nil {
		return
	}
	log.Printf("%s", string(b))
}

type syslogAuditLogger struct {
	w *syslog.Writer
}

func newSyslogAuditLogger() auditLogger {
	w, err := syslog.New(syslog.LOG_INFO|syslog.LOG_DAEMON, "")
	if err != nil {
		log.Printf("warning: could not initialize syslog, logging to stderr instead")
		return newStderrAuditLogger()
	}
	return &syslogAuditLogger{w}
}

func (l *syslogAuditLogger) Log(rctx *RequestContext, rsrc *Resource, what string) {
	b := buildAuditMessage(rctx, rsrc, what)
	if b == nil {
		return
	}
	l.w.Write(b) // nolint: errcheck
}
