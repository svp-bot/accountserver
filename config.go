package accountserver

import (
	"errors"
	"io/ioutil"

	"git.autistici.org/ai3/go-common/clientutil"
	"git.autistici.org/id/go-sso"
)

// Config holds the configuration for the AccountService.
type Config struct {
	Validation ValidationConfig `yaml:",inline"`

	Shards struct {
		Available map[string][]string `yaml:"available"`
		Allowed   map[string][]string `yaml:"allowed"`
	} `yaml:"shards"`

	SSO struct {
		PublicKeyFile string   `yaml:"public_key"`
		Domain        string   `yaml:"domain"`
		Service       string   `yaml:"service"`
		Groups        []string `yaml:"groups"`
		AdminGroup    string   `yaml:"admin_group"`
	} `yaml:"sso"`

	UserMetaDB *clientutil.BackendConfig `yaml:"user_meta_server"`

	AuthSocket                 string `yaml:"auth_socket"`
	UserAuthService            string `yaml:"user_auth_service"`
	AccountRecoveryAuthService string `yaml:"account_recovery_auth_service"`

	EnableOpportunisticEncryption bool `yaml:"auto_enable_encryption"`
	AuditLogsToSyslog             bool `yaml:"audit_syslog"`
}

func (c *Config) compile() error {
	if len(c.Shards.Allowed) == 0 {
		return errors.New("no allowed shards")
	}
	return c.Validation.compile()
}

func (c *Config) domainBackend() domainBackend {
	b := &staticDomainBackend{sets: make(map[string]*stringSet)}
	for kind, list := range c.Validation.AvailableDomains {
		b.sets[kind] = newStringSetFromList(list)
	}
	return b
}

func (c *Config) shardBackend() shardBackend {
	b := &staticShardBackend{
		available: make(map[string]*stringSet),
		allowed:   make(map[string]*stringSet),
	}
	loadSet := func(target map[string]*stringSet, src map[string][]string) {
		for kind, list := range src {
			target[kind] = newStringSetFromList(list)
		}
	}
	loadSet(b.available, c.Shards.Available)
	loadSet(b.allowed, c.Shards.Allowed)
	return b
}

func (c *Config) validationContext(be Backend) (*validationContext, error) {
	return &validationContext{
		config:  &c.Validation,
		domains: c.domainBackend(),
		shards:  c.shardBackend(),
		backend: be,
	}, nil
}

func (c *Config) templateContext() *templateContext {
	return &templateContext{
		shards:  c.shardBackend(),
		webroot: c.Validation.WebsiteRootDir,
	}
}

func (c *Config) ssoValidator() (sso.Validator, error) {
	pkey, err := ioutil.ReadFile(c.SSO.PublicKeyFile)
	if err != nil {
		return nil, err
	}
	return sso.NewValidator(pkey, c.SSO.Domain)
}
