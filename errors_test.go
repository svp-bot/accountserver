package accountserver

import (
	"testing"
)

func TestErrors(t *testing.T) {
	verr := newValidationError(nil, "field", "bad")
	rerr := newRequestError(verr)

	if !IsRequestError(rerr) {
		t.Errorf("IsRequestError() returned false")
	}
	if !IsValidationError(rerr) {
		t.Errorf("IsValidationError() returned false")
	}
}
