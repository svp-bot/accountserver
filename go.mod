module git.autistici.org/ai3/accountserver

go 1.14

require (
	git.autistici.org/ai3/go-common v0.0.0-20210507084757-2cf72583ce0e
	git.autistici.org/ai3/tools/aux-db v0.0.0-20210127161623-c7f0177bcc33
	git.autistici.org/id/auth v0.0.0-20210507085352-c4f218c42591
	git.autistici.org/id/go-sso v0.0.0-20210117165919-e56e6579953d
	git.autistici.org/id/usermetadb v0.0.0-20210507085300-ad16aa223703
	github.com/PuerkitoBio/goquery v1.5.1 // indirect
	github.com/go-ldap/ldap/v3 v3.3.0
	github.com/go-test/deep v1.0.7
	github.com/mattes/migrate v0.0.0-20180508041624-4768a648fbd9 // indirect
	github.com/matttproud/golang_protobuf_extensions v1.0.2-0.20181231171920-c182affec369 // indirect
	github.com/patrickmn/go-cache v0.0.0-20180815053127-5633e0862627
	github.com/pquerna/otp v1.3.0
	github.com/prometheus/client_golang v1.11.0
	github.com/sethvargo/go-password v0.2.0
	golang.org/x/crypto v0.0.0-20210506145944-38f3c27a63bf
	golang.org/x/net v0.0.0-20210226172049-e18ecbb05110
	golang.org/x/sync v0.0.0-20210220032951-036812b2e83c
	gopkg.in/yaml.v2 v2.4.0
)
