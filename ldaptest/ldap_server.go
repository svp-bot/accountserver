package ldaptest

import (
	"errors"
	"fmt"
	"io/ioutil"
	"net"
	"os"
	"os/exec"
	"path/filepath"
	"testing"
	"time"
)

// Config for the test in-memory LDAP server.
type Config struct {
	Dir   string
	Base  string
	Port  int
	LDIFs []string
}

func waitForPort(port int, timeout time.Duration) error {
	addr := fmt.Sprintf("127.0.0.1:%d", port)
	deadline := time.Now().Add(timeout)
	for {
		conn, err := net.Dial("tcp", addr)
		if err == nil {
			conn.Close()
			return nil
		}
		time.Sleep(200 * time.Millisecond)
		if time.Now().After(deadline) {
			return errors.New("server did not come up within the deadline")
		}
	}
}

// StartServer starts a test LDAP server with the specified configuration.
func StartServer(t testing.TB, config *Config) func() {
	tmpf, err := ioutil.TempFile("", "")
	if err != nil {
		t.Fatal(err)
	}
	fmt.Fprintf(tmpf, `ldap.rootDn=%s
ldap.managerDn=cn=manager,%s
ldap.managerPassword=password
ldap.port=%d
`, config.Base, config.Base, config.Port)
	defer tmpf.Close()

	args := []string{
		filepath.Join(config.Dir, "unboundid-ldap-server/bin/unboundid-ldap-server"),
		tmpf.Name(),
	}
	args = append(args, config.LDIFs...)
	// Use /bin/sh so that the script doesn't have to be executable.
	proc := exec.Command("/bin/sh", args...)
	proc.Stdout = os.Stdout
	proc.Stderr = os.Stderr

	if err := proc.Start(); err != nil {
		t.Fatalf("error starting LDAP server: %v", err)
	}

	waitForPort(config.Port, 5*time.Second)

	return func() {
		proc.Process.Kill()
		proc.Wait()
		os.Remove(tmpf.Name())
	}
}
